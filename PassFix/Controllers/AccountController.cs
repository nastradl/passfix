﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using PassFix.Extensions;
using PassFix.Helpers;
using PassFix.Models.AccountViewModels;
using PassFix.Models.Central;
using PassFix.Services;
using PassFix.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;

namespace PassFix.Controllers
{
    [Authorize]
    [Route("[controller]/[action]")]
    public class AccountController : BaseController
    {
        private readonly BranchService _branch = new BranchService();
        private readonly AuthService _rep = new AuthService();

        public AccountController() { }

        public ApplicationSignInManager SignInManager => HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
        public ApplicationUserManager UserManager => HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

        private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;
        
        public string ErrorMessage { get; set; }

        [AllowAnonymous]
        public ActionResult Login()
        {
            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // This doesn't count login failures towards account lockout
                // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                var signedUser = await UserManager.FindByEmailAsync(model.Email);

                if (signedUser == null)
                {
                    // Check if user used username instead
                    signedUser = await UserManager.FindByNameAsync(model.Email);

                    if (signedUser == null)
                        return Json(new
                        {
                            success = 0,
                            error = "Invalid username or password"
                        });
                }

                if (signedUser.LockoutEnabled)
                {
                    return Json(new
                    {
                        success = 0,
                        error = "Your account has been locked or disabled. Please contact an admin to re-enable it."
                    });
                }

                var result = await SignInManager.PasswordSignInAsync(signedUser.UserName, model.Password, model.RememberMe, shouldLockout: false);

                if (result == SignInStatus.Success)
                {
                    // Get user claims
                    var claims = await UserManager.GetClaimsAsync(signedUser.Id);

                    // Get branches
                    var branches = new List<Branch>();

                    // Temporary. I work at home sometimes and no access to database
                    if (claims.Any(x => x.Type.ToLower() == "amu"))
                    {
                        if (!bool.Parse(ConfigurationManager.AppSettings["showFullBranchList"]))
                            branches = Helper.GetTempBranchList(); 
                        else
                            branches = await _branch.GetBranches();
                    }

                    // audit
                    await _rep.UserLoggedIn(signedUser.Id);

                    // reset test data
                    if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
                    {
                        new TestInformation().ResetTestData();
                    }

                    var subclasses = await _rep.GetSubClasses();

                    foreach (var subclass in subclasses)
                    {
                        subclass.Claims.RemoveAll(c => !claims.Select(x => x.Value).Contains(c.Action));
                    }

                    var menuModel = new MenuModel
                    {
                        Username = signedUser.UserName,
                        SubClasses = subclasses,
                        Claims = claims.OrderBy(x => x.Value).ToList(),
                        Branches = branches
                    };

                    return Json(new
                    {
                        success = 1,
                        homepage = Helper.JsonPartialView(this, "~/Views/Home/Tools.cshtml", menuModel),
                        menu = Helper.JsonPartialView(this, "~/Views/Shared/_SideMenu.cshtml", menuModel),
                        returnUrl
                    });
                }
               
                if (result == SignInStatus.LockedOut)
                {
                    return Json(new
                    {
                        success = 0,
                        error = "You account has been locked",
                        returnUrl
                    });
                }

                await _rep.UserLoginFailed(signedUser.Id);
                return Json(new
                {
                    success = 0,
                    error = "Invalid username or password"
                });
            }

            return Json(new
            {
                success = 0,
                errors = ModelState.ToDictionary(
                    kvp => kvp.Key,
                    kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
                )
            });
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(UserRequest request, string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            if (ModelState.IsValid)
            {
                // Check if this email is already registered/requested
                var exists = await _rep.CheckEmailRegistered(request.EmailAddress);

                if (exists)
                    return Json(new
                    {
                        success = 0,
                        error = "You are already registered on this platform. Please contact an admin if you encountered an issue while registering"
                    });

                var result = await _rep.AddUserRequest(request);

                return Json(new
                {
                    success = result
                }); 
            }

            return Json(new
            {
                success = 0,
                errors = ModelState.Where(x => x.Value.Errors.Count > 0).ToDictionary(
                    kvp => kvp.Key,
                    kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
                )
            });
        }

        public async Task<ActionResult> Logout()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            await _rep.UserLoggedOut(User.Identity.GetUserId());

            return RedirectToAction(nameof(HomeController.Index), "Home");
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByEmailAsync(model.Email);
                if (user == null)
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return Json(new
                    {
                        success = 0,
                        error = "Sorry, no registered user with that email address"
                    });
                }

                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.ResetPasswordCallbackLink(user.Id, code, Request.Url?.Scheme);

                var iris = ConfigurationManager.AppSettings["companyEmail"];
                var friendlyName = ConfigurationManager.AppSettings["friendlyName"];
                var emailcredentials = ConfigurationManager.AppSettings["email"];
                var emailpassword = Helper.DecryptConfig(ConfigurationManager.AppSettings["password"], false);
                var styledBtn = "style='font-size: 16px;background-color: #027bbb;color: white;text-decoration: none;padding: 7px 20px;margin: 7px 0px;display: " +
                                "inline-block;-webkit-box-shadow: 2px 2px 5px rgba(0,0,0,0.3);-moz-box-shadow: 2px 2px 5px rgba(0,0,0,0.3);" +
                                "box-shadow: 2px 2px 5px rgba(0,0,0,0.3);border-radius: 5px;'";
                string subject = "PassFix password reset";
                string message = $@"<div>Hello { user.FirstName.ToTitleCase() } { user.Surname.ToTitleCase() },</div>
                                    <div style='margin-top: 10px'>A request was made to reset your password on the PassFix application.</div>
                                    <div style='margin-top: 10px'>Please click on the button below to reset your password:</div>
                                    <div style='margin-top: 10px'>
                                        <a href='{ callbackUrl }' {styledBtn}>Reset My Password</a>
                                    <div style='margin-top: 10px'>If you did not make this request please contact an administrator.</div>                                                                
                                    <div style='margin-top: 10px'>Thank you</div>";

                var mailMessage = new MailMessage
                {
                    From = new MailAddress(iris, friendlyName),
                    IsBodyHtml = true
                };
                mailMessage.To.Add(user.Email);
                mailMessage.Body = message;
                mailMessage.Subject = subject;

                using (var client = new SmtpClient("smtp.gmail.com", 587)
                {
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(emailcredentials, emailpassword)
                })
                {
                    client.Send(mailMessage);

                    return Json(new
                    {
                        success = 1
                    });
                }
            }

            return Json(new
            {
                success = 0,
                error = "Invalid email address"
            });
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return RedirectToAction(nameof(HomeController.Index), "Home");
            }
            var user = await UserManager.FindByIdAsync(userId);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{userId}'.");
            }
            var result = await UserManager.ConfirmEmailAsync(user.Id, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        [HttpGet]
        [AllowAnonymous]
        public ActionResult ResetPassword(string code = null)
        {
            if (code == null)
            {
                throw new ApplicationException("A code must be supplied for password reset.");
            }
            var model = new ResetPasswordViewModel { Code = code };
            return View(model);
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new
                {
                    success = 0,
                    errors = ModelState.ToDictionary(
                        kvp => kvp.Key,
                        kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray()
                    )
                });
            }
            var user = await UserManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                return Json(new
                {
                    success = 0,
                    error = "Invalid credentials"
                });
            }

            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return Json(new
                {
                    success = 1
                });
            }

            return Json(new
            {
                success = 0
            });
        }
    }
}
