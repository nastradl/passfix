﻿using Microsoft.AspNet.Identity;
using PassFix.Enums;
using PassFix.Helpers;
using PassFix.Services;
using PassFix.ViewModels;
using System;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace PassFix.Controllers
{
    [Authorized]
    public class AmuController : BaseController
    {
        private readonly AuthService _auth = new AuthService();
        private readonly BranchService _rep = new BranchService();

        #region Enrolment

        [ClaimRequirement("Amu", "BackToApproval")]
        public ActionResult BackToApproval()
        {
            var model = new Record
            {
                SearchUrl = "SearchBackToApproval",
                UpdateUrl = "UpdateBackToApproval",
                Title = "Back To Approval",
                AllowEditLocation = true,
                ShowLocation = true
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("BackToApproval");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "BackToAfisCheck")]
        public ActionResult BackToAfisCheck()
        {
            var model = new Record
            {
                SearchUrl = "SearchBackToAfisCheck",
                Title = "Back To Afis Check"
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("BackToAfisCheck");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "BackToPaymentCheck")]
        public ActionResult BackToPaymentCheck()
        {
            var model = new Record
            {
                SearchUrl = "SearchBackToPaymentCheck",
                UpdateUrl = "UpdateBackToPayment",
                Title = "Back To Payment"
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("BackToPaymentCheck");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "EditEnrolmentRecord")]
        public async Task<ActionResult> EditEnrolmentRecord()
        {
            var states = await _auth.GetStates();
            var titles = await _auth.GetTitles();
            var model = new DetailedRecord
            {
                SearchUrl = "SearchEnrolmentRecord",
                PageTitle = "Edit Enrolment Record",
                States = states,
                Titles = titles
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("EditEnrolmentRecord");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_DetailedBaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "LocalQuery")]
        public ActionResult LocalQuery()
        {
            var model = new Record
            {
                SearchUrl = "SearchLocalQuery",
                Title = "Local Query"
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("LocalQuery");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseLocalQuery.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "EditBioRecord")]
        public ActionResult EditBioRecord()
        {
            var model = new Record
            {
                SearchUrl = "SearchEditBioRecord",
                Title = "Edit Bio Record"
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("EditBioRecord");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "ReverseEditBioRecord")]
        public ActionResult ReverseEditBioRecord()
        {
            var model = new Record
            {
                SearchUrl = "SearchReverseEditBioRecord",
                Title = "Reverse Edit Bio Record Change",
                ShowSecondFormNo = false
            };

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "ModifyEnrolmentFile")]
        public ActionResult ModifyEnrolmentFile()
        {
            var model = new Record
            {
                SearchUrl = "SearchModifyEnrolmentFile",
                Title = "Modify Enrolment File"
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("ModifyEnrolmentFile");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewXML.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "AnalyseRecord")]
        public async Task<ActionResult> AnalyseRecord()
        {
            var states = await _auth.GetStates();
            var titles = await _auth.GetTitles();
            var model = new DetailedRecord
            {
                SearchUrl = "SearchAnalyseRecord",
                PageTitle = "Analyse Enrolment Records",
                ShowMultiMessage = true,
                States = states,
                Titles = titles
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("EditEnrolmentRecord");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_DetailedBaseAnaylseView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "ResetStagecode")]
        public ActionResult ResetStagecode()
        {
            var model = new Record
            {
                SearchUrl = "SearchResetStagecode",
                Title = "Reset Stagecode"
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("BackToAfisCheck");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "ManuallyFail")]
        public ActionResult ManuallyFail()
        {
            var model = new Record
            {
                SearchUrl = "SearchManuallyFail",
                Title = "Manually Fail Record"
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("BackToAfisCheck");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        #endregion

        #region Booklet

        [ClaimRequirement("Amu", "BackToPerso")]
        public ActionResult BackToPerso()
        {
            var model = new Record
            {
                SearchUrl = "SearchBackToPerso",
                Title = "Back To Perso",
                ShowPassportNo = true,
                ShowSearchByDocNo = true,
                AllowEditLocation = true,
                ShowPersoStageCode = true,
                ShowDocStageCode = true,
                ShowStageCode = false,
                ShowPersoLocation = true
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("BackToPerso");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "QueryBookletInfo")]
        public ActionResult QueryBookletInfo()
        {
            var model = new Record
            {
                SearchUrl = "SearchQueryBookletInfo",
                Title = "Query Booklet Info",
                ShowStageCode = true,
                ShowBoxSn = true,
                ShowUpdateButton = false
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("QueryBookletInfo");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewDocNo.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "OverideBookletSequence")]
        public ActionResult OverideBookletSequence()
        {
            var model = new Record
            {
                SearchUrl = "SearchOverideBookletSequence",
                Title = "Overide Booklet Sequence"
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("OverideBookletSequence");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewDocNo.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "BookletToPersoQueue")]
        public ActionResult BookletToPersoQueue()
        {
            var model = new Record
            {
                SearchUrl = "SearchBookletToPersoQueue",
                Title = "Return Booklet To Perso Queue"
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("BookletToPersoQueue");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewDocNo.cshtml", model)
            });
        }

        #endregion

        #region Personalisation

        [ClaimRequirement("Amu", "ResendJobForPrinting")]
        public ActionResult ResendJobForPrinting()
        {
            var model = new Record
            {
                SearchUrl = "SearchResendJobForPrinting",
                Title = "Resend Job For Printing",
                ShowStageCode = false,
                ShowDocStageCode = true,
                ShowPersoStageCode = true,
                ShowPassportNo = true
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("ResendJobForPrinting");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "SendToIssuance")]
        public ActionResult SendToIssuance()
        {
            var model = new Record
            {
                SearchUrl = "SearchSendToIssuance",
                Title = "Search Send To Issuance",
                ShowStageCode = true,
                ShowDocStageCode = true,
                ShowPersoStageCode = true,
                ShowPassportNo = true
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("SendToIssuance");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        #endregion

        #region Issuance    

        [ClaimRequirement("Amu", "IssuePassport")]
        public ActionResult IssuePassport()
        {
            var model = new Record
            {
                SearchUrl = "SearchIssuePassport",
                Title = "Search Issue Passport",
                ShowStageCode = true,
                ShowSearchByDocNo = true,
                ShowDocStageCode = true,
                ShowPersoStageCode = true,
                ShowPassportNo = true,
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("IssuePassport");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "FixWrongPassportIssued")]
        public ActionResult FixWrongPassportIssued()
        {
            var model = new Record
            {
                SearchUrl = "SearchFixWrongPassportIssued",
                Title = "Fix Wrong Passport Issued",
                ShowStageCode = false
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("FixWrongPassportIssued");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateView.cshtml", model)
            });
        }

        #endregion

        #region Intervention

        [ClaimRequirement("Amu", "CreateReissueRecord")]
        public ActionResult CreateReissueRecord()
        {
            var model = new Record
            {
                SearchUrl = "SearchCreateReissueRecord",
                Title = "Create reissue record"
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("CreateReissueRecord");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseReissueView.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "BindReissueRecord")]
        public ActionResult BindReissueRecord()
        {
            var model = new Record
            {
                SearchUrl = "SearchBindReissueRecord",
                Title = "Bind reissue record"
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("BindReissueRecord");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseBindReissueView.cshtml", model)
            });
        }

        #endregion

        #region Administration

        [ClaimRequirement("Amu", "ManageUser")]
        public ActionResult ManageUser()
        {
            var model = new Record
            {
                SearchUrl = "SearchManageUser",
                Title = "Manage User",
            };

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                model.TestInfo = TestInformation.GetTestData("ManageUser");
            }

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewUser.cshtml", model)
            });
        }

        #endregion

        #region Payment Management

        [ClaimRequirement("Amu", "PaymentQuery")]
        public ActionResult PaymentQuery()
        {
            var model = new Payment
            {
                SearchUrl = "SearchPaymentQuery",
                Title = "Search Payment Query"
            };

            if (User is ClaimsPrincipal user && user.HasClaim("Amu", "PaymentQueryAdmin"))
                model.IsAdmin = true;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BasePaymentQuery.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "ResetPayment")]
        public ActionResult ResetPayment()
        {
            var model = new Payment
            {
                SearchUrl = "SearchPaymentQueryReset",
                Title = "Search Payment Query"
            };
            
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BasePaymentQuery.cshtml", model)
            });
        }

        [ClaimRequirement("Amu", "EditPaymentRecord")]
        public ActionResult EditPaymentRecord()
        {
            var model = new Payment
            {
                SearchUrl = "SearchPaymentQueryEdit",
                Title = "Search Payment Query"
            };

            if (User is ClaimsPrincipal user && user.HasClaim("Amu", "PaymentQueryAdmin"))
                model.IsAdmin = true;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BasePaymentQuery.cshtml", model)
            });
        }

        #endregion

        // Json
        [ClaimRequirement("Amu", "BackToAfisCheck")]
        public async Task<ActionResult> SearchBackToAfisCheck(string formno, string branchCode)
        {
            var record = await _rep.SearchEnrolementByFormNo(formno, branchCode, User.Identity.GetUserId(), (int)EnumAuditEvents.SearchBackToAfisCheck);

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;
            

            // add update url
            record.UpdateUrl = "UpdateBackToAfisCheck";

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("BackToAfisCheck");
            }


            if (record.StageCode != "EM2001")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error = $"Cannot alter this record. Status requirements not met"
                });

            record.UpdateMsg = "Send Back To AFis Check";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }
        
        [ClaimRequirement("Amu", "BackToAfisCheck")]
        public async Task<ActionResult> UpdateBackToAfisCheck(Record record)
        {
            var result = await _rep.UpdateAfisRecord(record, User.Identity.GetUserId());

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "BackToApproval")]
        public async Task<ActionResult> SearchBackToApproval(string formno, string branchCode)
        {
            var record = await _rep.SearchForApprovalRecord(formno, branchCode, User.Identity.GetUserId());

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateBackToApproval";
            record.AllowEditLocation = true;
            record.ShowLocation = true;
            string error = string.Empty;
            bool badstagecode = false;

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("BackToApproval");
            }

            if (!string.IsNullOrEmpty(record.Error))
                error = record.Error;

            if (record.StageCode != "EM4000")
            {
                badstagecode = true;
                error = $"Cannot alter this record. Status requirements not met";
            }

            if (string.IsNullOrEmpty(error))
            {
                record.UpdateMsg = "Send Back To Approval";
                return Json(new
                {
                    success = 1,
                    enable = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
                });
            }

            return Json(new
                {
                    success = 1,
                    error,
                    badstagecode,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
                });
        }

        [ClaimRequirement("Amu", "BackToApproval")]
        public async Task<ActionResult> UpdateBackToApproval(Record record)
        {
            var result = await _rep.UpdateBackToApproval(record, User.Identity.GetUserId());

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "BackToPerso")]
        public async Task<ActionResult> SearchBackToPerso(string docno, string formno, string branchCode)
        {
            var record = new Record();
            
            if (!string.IsNullOrEmpty(formno))
                record = await _rep.SearchEnrolementByFormNo(formno, branchCode, User.Identity.GetUserId(), (int)EnumAuditEvents.SearchBackToPerso);

            if (!string.IsNullOrEmpty(docno))
                record = await _rep.SearchEnrolementByDocNo(docno, branchCode, User.Identity.GetUserId());

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateBackToPerso";
            record.AllowEditLocation = true;
            record.ShowPersoLocation = true;
            record.ShowPersoStageCode = true;
            record.ShowDocStageCode = true;
            record.ShowStageCode = false;
            record.ShowPassportNo = true;

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("BackToPerso");
            }

            if (record.PersoProfileStageCode != "PM2000" || record.DocProfileStageCode != "PM2000")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error = "Cannot alter this record. Status requirements not met"
                });

            record.UpdateMsg = "Send Back To Perso";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "BackToPerso")]
        public async Task<ActionResult> UpdateBackToPerso(Record record)
        {
            var result = await _rep.UpdateBackToPerso(record, User.Identity.GetUserId());

            return Json(new
            {
                success = result,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "ResetStagecode")]
        public async Task<ActionResult> SearchResetStagecode(string formno, string branchCode)
        {
            var record = await _rep.SearchEnrolementByFormNo(formno, branchCode, User.Identity.GetUserId(), (int)EnumAuditEvents.SearchEnrolProfile);

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;
            
            // add update url
            record.UpdateUrl = "UpdateResetStagecode";

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("BackToAfisCheck");
            }
            
            if (record.StageCode != "EM0801")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error = "Cannot alter this record. Status requirements not met"
                });

            record.UpdateMsg = "Reset Stagecode";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "ResetStagecode")]
        public async Task<ActionResult> UpdateResetStagecode(Record record)
        {
            var result = await _rep.UpdateResetStagecode(record, User.Identity.GetUserId());

            return Json(new
            {
                success = result,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "ManuallyFail")]
        public async Task<ActionResult> SearchManuallyFail(string formno, string branchCode)
        {
            var record = await _rep.SearchEnrolementByFormNo(formno, branchCode, User.Identity.GetUserId(), (int)EnumAuditEvents.SearchEnrolProfile);

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateManuallyFail";

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("BackToAfisCheck");
            }

            if (record.StageCode != "EM4000" && record.StageCode != "EM2000")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error = "Cannot alter this record. Status requirements not met"
                });

            record.UpdateMsg = "Fail Record";
            record.ShowRemark = true;

            if (record.StageCode == "EM4000")
            {
                if (!string.IsNullOrEmpty(record.PassportNo))
                    return Json(new
                    {
                        success = 1,
                        enable = true,
                        page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                        error = "Status is EM4000: Passport number is " + record.PassportNo
                    });

                return Json(new
                {
                    success = 1,
                    enable = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error = "Status is EM4000 but no passport number found"
                });
            }
            
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "ManuallyFail")]
        public async Task<ActionResult> UpdateManuallyFail(Record record)
        {
            if (string.IsNullOrEmpty(record.Remark))
            {
                return Json(new
                {
                    success = 0,
                    error = "Please enter a remark for this change"
                });
            }

            var result = await _rep.UpdateManuallyFail(record, User.Identity.GetUserId());

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "FixWrongPassportIssued")]
        public async Task<ActionResult> SearchFixWrongPassportIssued(string formno, string branchCode)
        {
            var record = await _rep.SearchFixWrongPassportIssued(formno, branchCode, User.Identity.GetUserId());

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateFixWrongPassportIssued";
            record.ShowStageCode = false;

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("FixWrongPassportIssued");
            }

            if (record.DocStageCode2 != "EM5000" || record.DocTwoStageCode != "EM5000")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error = "Cannot alter this record. Status requirements not met"
                });

            record.UpdateMsg = "Fix Wrong Passport Issued";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "FixWrongPassportIssued")]
        public async Task<ActionResult> UpdateFixWrongPassportIssued(Record record)
        {
            var result = await _rep.UpdateFixWrongPassportIssued(record, User.Identity.GetUserId());

            if (result == 3)
                return Json(new
                {
                    success = 0,
                    error = "Only one passport linked to this form number"
                });

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "BackToPaymentCheck")]
        public async Task<ActionResult> SearchBackToPaymentCheck(string formno, string branchCode)
        {
            var record = await _rep.SearchEnrolementByFormNo(formno, branchCode, User.Identity.GetUserId(), (int)EnumAuditEvents.SearchBackToPaymentCheck);

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateBackToPaymentCheck";

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("BackToPaymentCheck");
            }

            if (record.StageCode != "EM0801")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error = "Cannot alter this record. Status requirements not met"
                });

            record.UpdateMsg = "Send Back To Payment Check";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "BackToPaymentCheck")]
        public async Task<ActionResult> UpdateBackToPaymentCheck(Record record)
        {
            var result = await _rep.UpdateBackToPaymentCheck(record, User.Identity.GetUserId());

            return Json(new
            {
                success = result,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "AnalyseRecord")]
        public async Task<ActionResult> SearchAnalyseRecord(string formno, string branchCode)
        {
            var formnos = formno.Split(',').Select(x => x.Trim()).ToList();

            if (formnos.Count > 4)
                return Json(new
                {
                    success = 0,
                    enable = false,
                    error = "Maximum of FOUR forms please"
                });

            var records = await _rep.SearchEnrolementForAnalysis(formnos, branchCode, User.Identity.GetUserId());

            if (records == null)
                return Json(new
                {
                    success = 0,
                    enable = false,
                    error = "Could not connect to branch"
                });

            if (records.Count == 0)
                return Json(new
                {
                    success = 0,
                    enable = false,
                    error = "No records found matching entered search"
                });

            var json = Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_DetailedBaseAnalyseViewResult.cshtml", records)
            });

            json.MaxJsonLength = int.MaxValue;

            return json;
        }

        [ClaimRequirement("Amu", "EditEnrolmentRecord")]
        public async Task<ActionResult> SearchEnrolmentRecord(string formno, string branchCode)
        {
            var record = await _rep.SearchEnrolementByFormNoDetailed(formno, branchCode, User.Identity.GetUserId());

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.States = await _auth.GetStates();
            record.Titles = await _auth.GetTitles();
            record.UpdateUrl = "UpdateEnrolmentRecord";

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("EditEnrolmentRecord");
            }

            if (record.StageCode == "EM5000")
                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    enable = false,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_DetailedBaseUpdateViewResult.cshtml", record),
                    error = $"Cannot alter this record. It has a status of { Helper.GetStageCodeDescription(record.StageCode)}"
                });

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_DetailedBaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "EditEnrolmentRecord")]
        public async Task<ActionResult> UpdateEnrolmentRecord(DetailedRecord record)
        {
            var result = await _rep.UpdateEnrolmentRecord(record, User.Identity.GetUserId());

            return Json(new
            {
                success = result,
                clearform = false
            });
        }


        [ClaimRequirement("Amu", "ResendJobForPrinting")]
        public async Task<ActionResult> SearchResendJobForPrinting(string formno, string branchCode)
        {
            var record = await _rep.SearchEnrolementByFormNo(formno, branchCode, User.Identity.GetUserId(), (int)EnumAuditEvents.SearchBackToPaymentCheck);

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateResendJobForPrinting";
            record.ShowStageCode = false;
            record.ShowDocStageCode = true;
            record.ShowPersoStageCode = true;
            record.ShowPassportNo = true;

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("ResendJobForPrinting");
            }

            if (record.PersoProfileStageCode != "PM1000" || record.DocProfileStageCode != "PM1000")
            {
                var error = "Cannot alter this record. Status requirements not met";

                if (string.IsNullOrEmpty(record.PersoProfileStageCode) &&
                    string.IsNullOrEmpty(record.DocProfileStageCode))
                    error = "Cannot change stage code: No document found";

                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error
                });
            }
                

            record.UpdateMsg = "Resend Job For Printing";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "ResendJobForPrinting")]
        public async Task<ActionResult> UpdateResendJobForPrinting(Record record)
        {
            var result = await _rep.UpdateResendJobForPrinting(record, User.Identity.GetUserId());

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "IssuePassport")]
        public async Task<ActionResult> SearchIssuePassport(string docno, string formno, string branchCode)
        {
            var record = new Record();

            if (!string.IsNullOrEmpty(formno))
                record = await _rep.SearchEnrolementByFormNo(formno, branchCode, User.Identity.GetUserId(), (int)EnumAuditEvents.SearchBackToPaymentCheck);

            if (!string.IsNullOrEmpty(docno))
                record = await _rep.SearchEnrolementByDocNo(docno, branchCode, User.Identity.GetUserId());

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateIssuePassport";
            record.ShowStageCode = true;
            record.ShowDocStageCode = true;
            record.ShowPersoStageCode = true;
            record.ShowPassportNo = true;

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("IssuePassport");
            }

            if (record.DocProfiles != null)
            {
                var docprofilefound = false;
                var docprofilefound5000 = 0;

                foreach (var docprofile in record.DocProfiles)
                {
                    // Perso stagecode should be PM2000
                    var docperso = await _rep.GetPersoProfile(docprofile.Docno);
                    if (docperso.Stagecode == "PM2000")
                    {
                        record.PassportNo = docprofile.Docno;
                        record.PassportType = docprofile.Doctype;
                        record.DocProfileStageCode = docprofile.Stagecode;
                        record.PersoProfileStageCode = docperso.Stagecode;
                        docprofilefound = true;
                        break;
                    }

                    if (docperso.Stagecode == "EM5000")
                        docprofilefound5000++;
                 
                }

                if (!docprofilefound)
                {
                    string error = "Booklet not ready for issuance";

                    if (docprofilefound5000 == 1)
                    {
                        error = "Booklet already issued issued";
                    }

                    if (docprofilefound5000 > 1)
                    {
                        error = "Multiple booklets issued";
                    }

                    return Json(new
                    {
                        success = 1,
                        badstagecode = true,
                        page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                        error
                    });
                }
            }

            if (record.PersoProfileStageCode != "PM2000" || record.DocProfileStageCode != "PM2000" || record.StageCode != "EM4000")
            {
                var error = "Cannot alter this record. Status requirements not met";

                if (string.IsNullOrEmpty(record.PersoProfileStageCode) &&
                    string.IsNullOrEmpty(record.DocProfileStageCode))
                    error = "Cannot change stage code: No document found";

                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error
                });
            }


            record.UpdateMsg = "Issue Passport";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "IssuePassport")]
        public async Task<ActionResult> UpdateIssuePassport(Record record)
        {
            var result = await _rep.UpdateIssuePassport(record, User.Identity.GetUserId());

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "SendToIssuance")]
        public async Task<ActionResult> SearchSendToIssuance(string formno, string branchCode)
        {
            var record = await _rep.SearchEnrolementByFormNo(formno, branchCode, User.Identity.GetUserId(), (int)EnumAuditEvents.SearchSendToIssuance);

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateSendToIssuance";
            record.ShowStageCode = true;
            record.ShowDocStageCode = true;
            record.ShowPersoStageCode = true;
            record.ShowPassportNo = true;

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("SendToIssuance");
            }

            if (record.PersoProfileStageCode != "PM2001" && record.PersoProfileStageCode != "PM1000" 
                || record.DocProfileStageCode != "PM2001" && record.DocProfileStageCode != "PM1000")
            {
                var error = "Cannot alter this record. Status requirements not met";

                if (string.IsNullOrEmpty(record.PersoProfileStageCode) &&
                    string.IsNullOrEmpty(record.DocProfileStageCode))
                    error = "Cannot change status: No document found";

                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error
                });
            }


            record.UpdateMsg = "Send To Issuance";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "SendToIssuance")]
        public async Task<ActionResult> UpdateSendToIssuance(Record record)
        {
            var result = await _rep.UpdateSendToIssuance(record, User.Identity.GetUserId());

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "ManageUser")]
        public async Task<ActionResult> SearchManageUser(string username, string branchCode)
        {
            var record = await _rep.SearchManageUser(username, branchCode, User.Identity.GetUserId());

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // add update url
            record.UpdateUrl = "UpdateManageUser";

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("ManageUser");
            }

            record.UpdateMsg = "Update User";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewUserResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "ManageUser")]
        public async Task<ActionResult> UpdateManageUser(Record record)
        {
            var result = await _rep.UpdateManageUser(record, User.Identity.GetUserId());

            return Json(new
            {
                success = result,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "ModifyEnrolmentFile")]
        public async Task<ActionResult> SearchModifyEnrolmentFile(string formno, string password, string branchCode)
        {
            try
            {
                var record = await _rep.SearchModfyEnrolment(formno, password, branchCode, User.Identity.GetUserId());

                var err = CheckNoResultFound(record);
                if (err != null)
                    return err;

                // test data
                if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
                {
                    record.TestInfo = TestInformation.GetTestData("ModifyEnrolmentFile");
                }

                return Json(new
                {
                    success = 1,
                    enable = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewXMLResult.cshtml", record)
                });
            }
            catch (Exception)
            {
                return Json(new
                {
                    success = 0,
                    error = "Invalid administrator password"
                });
            }
        }

        [ClaimRequirement("Amu", "ModifyEnrolmentFile")]
        public async Task<ActionResult> UpdateModifyEnrolmentFile(Record record)
        {
            try
            {
                var result = await _rep.UpdateModifyEnrolmentFile(record, User.Identity.GetUserId());

                return Json(new
                {
                    success = result,
                    clearform = true
                });
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    success = 0,
                    error = ex.Message
                });
            }
        }


        [ClaimRequirement("Amu", "QueryBookletInfo")]
        public async Task<ActionResult> SearchQueryBookletInfo(string docno, string branchCode)
        {
            var record = await _rep.SearchQueryBookletInfo(docno, branchCode, User.Identity.GetUserId());

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("QueryBookletInfo");
            }

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            record.ShowUpdateButton = false;
            record.ShowBoxSn = true;
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResultDocNo.cshtml", record)
            });
        }


        [ClaimRequirement("Amu", "OverideBookletSequence")]
        public async Task<ActionResult> SearchOverideBookletSequence(string docno, string branchCode)
        {
            var record = await _rep.SearchQueryBookletInfo(docno, branchCode, User.Identity.GetUserId());

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("OverideBookletSequence");
            }

            if (record.StageCode != "IM2000")
            {
                var error = "Cannot alter this record. Status requirements not met";

                if (record.StageCode == "PM0500")
                    error = "Cannot alter this record as Booklet already personalised";

                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResultDocNo.cshtml", record),
                    error
                });
            }

            record.UpdateUrl = "UpdateOverideBookletSequence";
            record.UpdateMsg = "Overide Booklet Sequence";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResultDocNo.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "OverideBookletSequence")]
        public async Task<ActionResult> UpdateOverideBookletSequence(Record record)
        {
            var result = await _rep.UpdateOverideBookletSequence(record, User.Identity.GetUserId());

            return Json(new
            {
                success = result,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "CreateReissueRecord")]
        public async Task<ActionResult> SearchCreateReissueRecord(string docno, string branchCode)
        {
            var record = await _rep.SearchCreateReissueRecord(docno, branchCode, false, User.Identity.GetUserId());

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {                              
                record.TestInfo = TestInformation.GetTestData("OverideBookletSequence");
            }

            if (record.Passport.LocEnrolProfile.Stagecode != "EM5000")
            {
                var error = "Cannot alter this record. Status requirements not met";

                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseReissueUpdateViewResult.cshtml", record),
                    error
                });
            }

            record.UpdateUrl = "UpdateCreateReissueRecord";
            record.UpdateMsg = "Create Reissue Record";

            // get appreason list
            record.AppReasons = await _rep.GetAppReasons();

            record.DocNo = docno;
            record.BranchCode = branchCode;

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseReissueUpdateViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "CreateReissueRecord")]
        public async Task<ActionResult> UpdateCreateReissueRecord(Record record)
        {
            var result = await _rep.UpdateCreateReissueRecord(record, User.Identity.GetUserId());

            if (result == null)
                return Json(new
                {
                    success = 0
                });

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseReissueViewResult.cshtml", result),
                enable = true
            });
        }

        [ClaimRequirement("Amu", "BindReissueRecord")]
        public async Task<ActionResult> SearchBindReissueRecord(string formno, string docno, string branchCode)
        {
            var record = await _rep.SearchBindReissueRecord(docno, formno, branchCode, false, User.Identity.GetUserId());

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("OverideBookletSequence");
            }

            if (record.Passport.LocEnrolProfile.Stagecode != "EM5000")
            {
                var error = "Cannot alter this record. Status requirements not met";

                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResult.cshtml", record),
                    error
                });
            }

            record.UpdateUrl = "UpdateBindReissueRecord";
            record.UpdateMsg = "Bind Reissue Record";

            // get appreason list
            record.AppReasons = await _rep.GetAppReasons();

            record.FormNo = formno;
            record.DocNo = docno;
            record.BranchCode = branchCode;

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBindReissueViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "CreateReissueRecord")]
        public async Task<ActionResult> UpdateBindReissueRecord(Record record)
        {
            var result = await _rep.UpdateBindReissueRecord(record, User.Identity.GetUserId());

            if (result == null)
                return Json(new
                {
                    success = 0
                });

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseReissueViewResult.cshtml", result),
                enable = true
            });
        }

        [ClaimRequirement("Amu", "BookletToPersoQueue")]
        public async Task<ActionResult> SearchBookletToPersoQueue(string docno, string branchCode)
        {
            var record = await _rep.SearchQueryBookletInfo(docno, branchCode, User.Identity.GetUserId());

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            // test data
            if (bool.Parse(ConfigurationManager.AppSettings["useTestData"]))
            {
                record.TestInfo = TestInformation.GetTestData("BookletToPersoQueue");
            }

            if (record.StageCode != "IM3000")
            {
                var error = "Cannot alter this record. Status requirements not met";

                return Json(new
                {
                    success = 1,
                    badstagecode = true,
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResultDocNo.cshtml", record),
                    error
                });
            }

            record.UpdateMsg = "Send Booklet To PersoQueue";
            record.UpdateUrl = "UpdateBookletToPersoQueue";
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateViewResultDocNo.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "BookletToPersoQueue")]
        public async Task<ActionResult> UpdateBookletToPersoQueue(Record record)
        {
            var result = await _rep.UpdateBookletToPersoQueue(record, User.Identity.GetUserId());

            return Json(new
            {
                success = result,
                clearform = true
            });
        }


        [ClaimRequirement("Amu", "LocalQuery")]
        public async Task<ActionResult> SearchLocalQuery(string docno, string branchCode, string gender, string firstname, string surname, string formno)
        {
            // must have at least 2 variables
            var count = 0;

            if (!string.IsNullOrEmpty(docno)) count += 2;
            if (!string.IsNullOrEmpty(gender)) count++;
            if (!string.IsNullOrEmpty(firstname)) count++;
            if (!string.IsNullOrEmpty(surname)) count++;
            if (!string.IsNullOrEmpty(formno)) count += 2;

            if (count < 2)
                return Json(new
                {
                    success = 0,
                    error = "Please enter at least TWO fields to search"
                });

            var record = await _rep.SearchLocalQuery(docno, branchCode, gender, firstname, surname, formno, User.Identity.GetUserId());

            if (record == null)
            {
                return Json(new
                {
                    success = 0,
                    error = "Could not connect to branch"
                });
            }

            if (record.Count == 0)
                return Json(new
                {
                    success = 0,
                    error = "No record found matching your request"
                });


            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseLocalQueryResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "LocalQuery")]
        public async Task<ActionResult> ViewLocalResult(string formno, string branch)
        {
            var record = await _rep.SearchLocalQuery(null, branch, null, null, null, formno, User.Identity.GetUserId());

            if (record == null)
                return Json(new
                {
                    success = 0,
                    error = "No record found matching your request"
                });

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseLocalQueryResultTable.cshtml", record.First())
            });
        }

        [ClaimRequirement("Amu", "PaymentQuery")]
        public async Task<ActionResult> SearchPaymentQuery(string aid, string refid)
        {
            if (string.IsNullOrEmpty(aid) || string.IsNullOrEmpty(refid))
                return Json(new
                {
                    success = 0,
                    error = "Please enter both Reference and Application Id"
                });

            var payment = await _rep.SearchPaymentQuery(aid, refid, User.Identity.GetUserId());

            if (payment == null)
            {
                return Json(new
                {
                    success = 0,
                    error = "No payment record found matching your search"
                });
            }

            SetOldValues(payment);

            payment.AllowEdit = false;
            if (User is ClaimsPrincipal user && user.HasClaim("Amu", "PaymentQueryAdmin"))
                payment.IsAdmin = true;

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BasePaymentQueryResult.cshtml", payment)
            });
        }
        
        [ClaimRequirement("Amu", "EditPaymentRecord")]
        public async Task<ActionResult> SearchPaymentQueryEdit(string aid, string refid)
        {
            if (string.IsNullOrEmpty(aid) || string.IsNullOrEmpty(refid))
                return Json(new
                {
                    success = 0,
                    error = "Please enter both Reference and Application Id"
                });

            var payment = await _rep.SearchPaymentQuery(aid, refid, User.Identity.GetUserId());

            if (payment == null)
            {
                return Json(new
                {
                    success = 0,
                    error = "No payment record found matching your search"
                });
            }

            SetOldValues(payment);

            // Get lists
            ViewBag.Offices = await _rep.GetPaymentOffices();
            ViewBag.States = await _rep.GetPaymentStates();

            payment.AllowEdit = true;
            payment.AllowUpdate = true;
            payment.UpdateMsg = "Save Changes";
            payment.UpdateUrl = "UpdatePaymentQuery";
            if (User is ClaimsPrincipal user && user.HasClaim("Amu", "PaymentQueryAdmin"))
                payment.IsAdmin = true;

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BasePaymentQueryResult.cshtml", payment)
            });
        }

        [ClaimRequirement("Amu", "ResetPayment")]
        public async Task<ActionResult> SearchPaymentQueryReset(string aid, string refid)
        {
            if (string.IsNullOrEmpty(aid) || string.IsNullOrEmpty(refid))
                return Json(new
                {
                    success = 0,
                    error = "Please enter both Reference and Application Id"
                });

            var payment = await _rep.SearchPaymentQuery(aid, refid, User.Identity.GetUserId());

            if (payment == null)
            {
                return Json(new
                {
                    success = 0,
                    error = "No payment record found matching your search"
                });
            }

            if (payment.Applicationstatus == "EM5000")
            {
                return Json(new
                {
                    success = 0,
                    error = "Cannot reset a record with status EM5000"
                });
            }

            payment.AllowUpdate = true;
            payment.UpdateMsg = "Reset Payment";
            payment.UpdateUrl = "UpdateResetPayment";

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BasePaymentQueryResult.cshtml", payment)
            });
        }

        [ClaimRequirement("Amu", "PaymentQuery")]
        public async Task<ActionResult> UpdatePaymentQuery(Payment payment)
        {
            if (User is ClaimsPrincipal user && user.HasClaim("Amu", "PaymentQueryAdmin"))
                payment.IsAdmin = true;

            var result = await _rep.UpdatePaymentQuery(payment, User.Identity.GetUserId());

            if (result == 0)
            {
                return Json(new
                {
                    success = 0,
                    error = "Update failed"
                });
            }

            return Json(new
            {
                success = 1,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "ResetPayment")]
        public async Task<ActionResult> UpdateResetPayment(Payment payment)
        {
            var result = await _rep.ResetPayment(payment.Aid, payment.RefId, User.Identity.GetUserId());

            if (result == 0)
            {
                return Json(new
                {
                    success = 0,
                    error = "Reset failed"
                });
            }

            if (result == 2)
            {
                return Json(new
                {
                    success = 0,
                    error = "Cannot reset a record with status EM5000"
                });
            }

            return Json(new
            {
                success = 1,
                clearform = true
            });
        }

        [ClaimRequirement("Amu", "EditBioRecord")]
        public async Task<ActionResult> SearchEditBioRecord(string formno, string secondformno, string branchCode)
        {
           if (string.IsNullOrEmpty(formno) || string.IsNullOrEmpty(secondformno))
                return Json(new
                {
                    success = 0,
                    hideform = true,
                    error = "Please fill both fields"
                });

            if (formno == secondformno)
                return Json(new
                {
                    success = 0,
                    hideform = true,
                    error = "Both form numbers should not be the same"
                });

            var record = await _rep.SearchEditBioRecord(formno, secondformno, branchCode, User.Identity.GetUserId());

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            if (record.FaceReplace == null || record.SignatureReplace == null)
            {
                record.ShowUpdateButton = false;
                return Json(new
                {
                    success = 1,
                    error = "Replacement record must have Face and Signature",
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioViewResult.cshtml", record)
                });
            }

            record.UpdateMsg = "Replace LEFT side with RIGHT";
            record.UpdateUrl = "UpdateEditBioRecord";
            record.BranchCode = branchCode;
            record.FormNo = formno;
            record.SecondFormNo = secondformno;

            if (record.FaceReplace.SequenceEqual(record.FaceCurrent) &&
                record.SignatureReplace.SequenceEqual(record.SignatureReplace))
            {
                record.ShowUpdateButton = false;
                return Json(new
                {
                    success = 1,
                    error = "Both records are the same",
                    page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioViewResult.cshtml", record)
                });
            }
                
            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "ReverseEditBioRecord")]
        public async Task<ActionResult> SearchReverseEditBioRecord(string formno, string branchCode)
        {
            if (string.IsNullOrEmpty(formno))
                return Json(new
                {
                    success = 0,
                    error = "Please enter a form number"
                });

            var record = await _rep.SearchReverseEditBioRecord(formno, branchCode, User.Identity.GetUserId());

            var err = CheckNoResultFound(record);
            if (err != null)
                return err;

            record.UpdateMsg = "Reverse edit bio transaction";
            record.UpdateUrl = "UpdateReverseEditBioRecord";
            record.BranchCode = branchCode;
            record.FormNo = formno;
            record.ShowUpdateButton = false;

            return Json(new
            {
                success = 1,
                enable = true,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioViewResult.cshtml", record)
            });
        }

        [ClaimRequirement("Amu", "EditBioRecord")]
        public async Task<ActionResult> UpdateEditBioRecord(Record record)
        {
            if (record.Revert == 1)
                return await UpdateReverseEditBioRecord(record);

            var result = await _rep.UpdateEditBioRecord(record, User.Identity.GetUserId());

            if (result == 0)
            {
                return Json(new
                {
                    success = 0,
                    error = "Update failed"
                });
            }

            // get refresh
            var newrecord = await _rep.SearchEditBioRecord(record.FormNo, record.SecondFormNo, record.BranchCode,
                User.Identity.GetUserId());

            newrecord.UpdateMsg = "Replace LEFT side with RIGHT";
            newrecord.UpdateUrl = "UpdateEditBioRecord";
            newrecord.BranchCode = record.BranchCode;
            newrecord.FormNo = record.FormNo;
            newrecord.SecondFormNo = record.SecondFormNo;
            newrecord.ShowUpdateButton = false;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioViewResult.cshtml", newrecord)
            });
        }

        [ClaimRequirement("Amu", "EditBioRecord")]
        public async Task<ActionResult> UpdateReverseEditBioRecord(Record record)
        {
            var result = await _rep.UpdateReverseEditBioRecord(record, User.Identity.GetUserId());

            if (result == 0)
            {
                return Json(new
                {
                    success = 0,
                    error = "Revert failed"
                });
            }

            // get refresh
            var newrecord = await _rep.SearchEditBioRecord(record.FormNo, record.SecondFormNo, record.BranchCode,
                User.Identity.GetUserId());

            newrecord.ShowUpdateButton = false;

            return Json(new
            {
                success = 1,
                hideonupdate = 1,
                page = Helper.JsonPartialView(this, "~/Views/Amu/_BaseUpdateBioViewResult.cshtml", newrecord)
            });
        }

        private ActionResult CheckNoResultFound(Record record)
        {
            if (record == null)
                return Json(new
                {
                    success = 0,
                    enable = false,
                    error = "No record found matching your request"
                });

            if (!string.IsNullOrEmpty(record.Error))
                return Json(new
                {
                    success = 0,
                    enable = false,
                    error = record.Error
                });

            return null;
        }

        private ActionResult CheckNoResultFound(DetailedRecord record)
        {
            if (record == null)
                return Json(new
                {
                    success = 0,
                    enable = false,
                    error = "No record found matching your request"
                });

            if (!string.IsNullOrEmpty(record.Error))
                return Json(new
                {
                    success = 0,
                    enable = false,
                    error = record.Error
                });

            return null;
        }

        private void SetOldValues(Payment payment)
        {
            payment.OldSurname = payment.Lastname;
            payment.OldDob = payment.Dob;
            payment.OldExpiryDate = payment.Expirydate;
            payment.OldSex = payment.Sex;
            payment.OldFirstname = payment.Firstname;
            payment.OldProcessingcountry = payment.Processingcountry;
            payment.OldProcessingoffice = payment.Processingoffice;
            payment.OldProcessingstate = payment.Processingstate;
        }
    }
}
