﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using PassFix.Services;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Mvc;

namespace PassFix.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            // for testing purposes
            var message = filterContext.Exception.Message;
            var innerex = filterContext.Exception.InnerException;

            var exception = filterContext.Exception;

            // if its antiforgery token message, just log the person out


            var code = HttpStatusCode.InternalServerError; // 500 if unexpected
            var result = JsonConvert.SerializeObject(new { error = exception.Message });

            if (exception is FileNotFoundException) code = HttpStatusCode.NotFound;
            else if (exception is UnauthorizedAccessException) code = HttpStatusCode.Unauthorized;
            else if (exception is HttpRequestException) code = HttpStatusCode.BadRequest;

            var context = filterContext.HttpContext;

            try
            {
                // save to database
                string url = context.Request.Url?.ToString();
                string user = filterContext.HttpContext.User?.Identity.GetUserId();
                new AuthService().SaveError(exception, url, user);
            }
            catch
            {
                // do nothing
            }

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            context.Response.Write(result);
        }
    }
}