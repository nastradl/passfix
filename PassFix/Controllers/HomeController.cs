﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PassFix.Helpers;
using PassFix.Models.AccountViewModels;
using PassFix.Services;
using PassFix.ViewModels;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace PassFix.Controllers
{
    [Authorized]
    public class HomeController : Controller
    {
        private readonly BranchService _branch = new BranchService();
        private readonly AuthService _rep = new AuthService();
        public ApplicationUserManager UserManager => HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

        [AllowAnonymous]
        public ActionResult Index()
        {
            var model = new RegistrationModel();

            if (Request.Url.Host.StartsWith("localhost"))
            {
                model.LoginViewModel = new LoginViewModel
                {
                    Email = "admin@irissmart.com",
                    Password = "password1"
                };
            }

            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Tools");

            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> Error()
        {
            if (User.Identity.IsAuthenticated && !Request.IsAjaxRequest())
            {
                HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                await new AuthService().UserLoggedOut(User.Identity.GetUserId());
            }

            return View();
        }

        public async Task<ActionResult> Tools(string section, string option)
        {
            var signedUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (signedUser == null)
            {
                HttpContext.GetOwinContext().Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return RedirectToAction(nameof(Index), "Home");
            }

            // Get user claims
            var claims = await UserManager.GetClaimsAsync(signedUser.Id);

            // Get branches
            var branches = new List<Branch>();

            // Temporary. I work at home sometimes and no access to database
            if (claims.Any(x => x.Type.ToLower() == "amu"))
            {
                if (bool.Parse(ConfigurationManager.AppSettings["showFullBranchList"]))
                    branches = await _branch.GetBranches();
                else
                    branches = Helper.GetTempBranchList();
            }

            var subclasses = await _rep.GetSubClasses();

            foreach (var subclass in subclasses)
            {
                subclass.Claims.RemoveAll(c => !claims.Select(x => x.Value).Contains(c.Action));
            }

            var model = new MenuModel
            {
                Username = signedUser.UserName,
                SubClasses = subclasses,
                Claims = claims.OrderBy(x => x.Value).ToList(),
                Branches = branches
            };

            return View(model);
        }
    }
}
