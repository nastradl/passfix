﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using PassFix.Enums;
using PassFix.Extensions;
using PassFix.Helpers;
using PassFix.Models;
using PassFix.Models.Central;
using PassFix.Services;
using PassFix.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using X.PagedList;

namespace PassFix.Controllers
{
    [Authorized]
    public class UserManagementController : BaseController
    {
        private readonly AuthService _rep = new AuthService();
        private readonly BranchService _branch = new BranchService();

        public ApplicationUserManager UserManager => HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();

        [ClaimRequirement("UserManagement", "ManageMyAccount")]
        public async Task<ActionResult> ManageMyAccount()
        {
            var user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ManageMyAccount.cshtml", user)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public async Task<ActionResult> ManageOtherUsers()
        {
            var users = (await _rep.GetUsers()).Where(x => x.Id != User.Identity.GetUserId()).ToList();

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ManageOtherUsersWrap.cshtml", users)
            });
        }

        [ClaimRequirement("UserManagement", "ManageUserRequests")]
        public async Task<ActionResult> ManageUserRequests()
        {
            var requests = await _rep.GetUserRequests();

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ManageUserRequests.cshtml", requests)
            });
        }

        [ClaimRequirement("UserManagement", "ManageSubclasses")]
        public async Task<ActionResult> ManageSubclasses()
        {
            var subclass = await _rep.GetSubClasses();

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditSubclassesWrap.cshtml", subclass.OrderBy(x => x.SubclassName).ToList())
            });
        }

        [ClaimRequirement("UserManagement", "ViewErrorLogs")]
        public async Task<ActionResult> ViewErrorLogs()
        {
            var perpage = int.Parse(ConfigurationManager.AppSettings["errorPerPage"]);
            var errors = await _rep.GetErrorLogs(perpage, 0, "TimeStamp", 1);

            errors.Page = new StaticPagedList<ErrorLog>(errors.ErrorLogs, 1, perpage, errors.Total);
            errors.OrderBy = "TimeStamp";
            errors.CurrentPage = 1;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewErrorLogs.cshtml", errors)
            });
        }

        [ClaimRequirement("UserManagement", "ViewAuditTrails")]
        public async Task<ActionResult> ViewAuditTrails()
        {
            var perpage = int.Parse(ConfigurationManager.AppSettings["auditPerPage"]);
            var audits = await _rep.GetAuditTrails(perpage, 0, "TimeStamp", 1);

            audits.Page = new StaticPagedList<Audit>(audits.Audits, 1, perpage, audits.Total);
            audits.OrderBy = "TimeStamp";
            audits.CurrentPage = 1;

            // Search values
            audits.UserList = await _rep.GetUsers();
            audits.ActionsList = Enum.GetValues(typeof(EnumAuditEvents))
                .Cast<EnumAuditEvents>()
                .Select(v => v.ToString())
                .ToList();

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewAuditTrails.cshtml", audits)
            });
        }


        // Json
        [ClaimRequirement("UserManagement", "ManageUserRequests")]
        public async Task<ActionResult> ApproveUserRequest(string selected, string reject)
        {
            bool rejection = reject == "1";

            if (string.IsNullOrEmpty(selected))
                return Json(new
                {
                    success = 0,
                    error = "No users found matching request"
                });

            // convert selected to list of ids
            var ids = selected.Split(',').Select(int.Parse).ToList();
            var emailcredentials = ConfigurationManager.AppSettings["email"];
            var emailpassword = Helper.DecryptConfig(ConfigurationManager.AppSettings["password"], false);

            using (var client = new SmtpClient("smtp.gmail.com", 587)
            {
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(emailcredentials, emailpassword)
            })
            {
                if (rejection)
                {
                    foreach (var req in ids)
                    {
                        var request = await _rep.GetUserRequest(req);

                        await _rep.RejectUserRequest(User.Identity.GetUserId(), ids);

                        // send an email to user with registration information
                        var iris = ConfigurationManager.AppSettings["companyEmail"];
                        var friendlyName = ConfigurationManager.AppSettings["friendlyName"];
                        string subject = "PassFix registration request";
                        string message = $@"<div>Hello {request.FirstName.ToTitleCase()} {request.Surname.ToTitleCase()},</div>
                                    <div style='margin-top: 10px'>Unfortunately, your request to register with the Iris PassFix tool has been denied. 
                                    Please contact an admin staff if you need to discuss this decision.</div>
                                    <div style='margin-top: 10px'>Thank you</div>";

                        var mailMessage = new MailMessage
                        {
                            From = new MailAddress(iris, friendlyName),
                            IsBodyHtml = true
                        };
                        mailMessage.To.Add(request.EmailAddress);
                        mailMessage.Body = message;
                        mailMessage.Subject = subject;
                        client.Send(mailMessage);
                    }
                }
                else
                {
                    // send an email to user with registration information
                    foreach (var req in ids)
                    {
                        var request = await _rep.GetUserRequest(req);
                        string tempPassword = Helper.GenerateRandomString(6);
                        var result = await _rep.ApproveUserRequest(User.Identity.GetUserId(), ids, tempPassword);

                        if (result == -1)
                            return Json(new
                            {
                                success = 0,
                                error = "This user already exists in the database"
                            });
                        
                        var iris = ConfigurationManager.AppSettings["companyEmail"];
                        var friendlyName = ConfigurationManager.AppSettings["friendlyName"];
                        string subject = "PassFix registration request";
                        string message = $@"<div>Hello {request.FirstName.ToTitleCase()} {request.Surname.ToTitleCase()},</div>
                                    <div style='margin-top: 10px'>Your request to register with the Iris PassFix tool has been approved. 
                                    Please log in with your email and this temporary password:</div>
                                    <div style='font-size:18px; font-weight: bold; margin: 10px 0px'>{tempPassword}</div>
                                    <div>You will be required to create your own password the first time you log in.</div>
                                    <div style='margin-top: 10px'>Thank you</div>";

                        var mailMessage = new MailMessage
                        {
                            From = new MailAddress(iris, friendlyName),
                            IsBodyHtml = true
                        };
                        mailMessage.To.Add(request.EmailAddress);
                        mailMessage.Body = message;
                        mailMessage.Subject = subject;
                        client.Send(mailMessage);
                    }
                }
            }
                
            // get updated list of requests
            var requests = await _rep.GetUserRequests();
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ManageUserRequests.cshtml", requests)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public async Task<ActionResult> GetUserPermissions(string id)
        {
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditUserClaims.cshtml", await GetUserClaims(id))
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public async Task<ActionResult> AddClaims(string id, string claims)
        {
            await _rep.AddClaimsToUser(id, claims, User.Identity.GetUserId());

            var model = await GetUserClaims(id);

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditUserClaimsModal.cshtml", model)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public async Task<ActionResult> RemoveClaims(string id, string claims)
        {
            await _rep.RemoveClaimsFromUser(id, claims, User.Identity.GetUserId());

            var model = await GetUserClaims(id);

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditUserClaimsModal.cshtml", model)
            });
        }

        [ClaimRequirement("UserManagement", "ManageOtherUsers")]
        public async Task<ActionResult> SetUserLockout(string id, int lockout)
        {
            await _rep.SetUserLockout(id, User.Identity.GetUserId(), lockout == 1);

            var users = (await _rep.GetUsers()).Where(x => x.Id != User.Identity.GetUserId()).ToList();
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ManageOtherUsers.cshtml", users)
            });
        }


        [ClaimRequirement("UserManagement", "ManageMyAccount")]
        public async Task<ActionResult> UpdatePersonalDetails(ApplicationUser user)
        {
            // Confirm the correct user is making this change
            if (user.Id != User.Identity.GetUserId())
                return Json(new
                {
                    success = 0
                });

            var result = await _rep.UpdateUserDetails(user);

            if (result == -1)
                return Json(new
                {
                    success = result,
                    error = "That username is already in user. Please enter a different one"
                });

            return Json(new
            {
                success = result
            });
        }

        [ClaimRequirement("UserManagement", "ManageMyAccount")]
        public async Task<ActionResult> ChangePassword(ApplicationUser user)
        {
            // Confirm the correct user is making this change
            if (user.Id != User.Identity.GetUserId())
                return Json(new
                {
                    success = 0
                });

            var result = await _rep.UpdateUserPassword(user);
            return Json(new
            {
                success = result
            });
        }


        [ClaimRequirement("UserManagement", "ViewAuditTrails")]
        public async Task<ActionResult> SearchAudit(int perpage, string userid, string action, string start, string end, string formno, string docno)
        {
            var audits = await _rep.SearchAuditTrails(perpage, userid, action, start, end, formno, docno);
            audits.Page = new StaticPagedList<Audit>(audits.Audits, 1, perpage, audits.Total);
            audits.OrderBy = "TimeStamp";
            audits.CurrentPage = 1;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewAuditTrailsResult.cshtml", audits)
            });
        }

        [ClaimRequirement("UserManagement", "ViewErrorLogs")]
        public async Task<ActionResult> GetInnerException(int id)
        {
            var error = await _rep.GetErrorLog(id);

            return Json(new
            {
                success = 1,
                msg = error.InnerException
            });
        }

        [ClaimRequirement("UserManagement", "ManageSubclasses")]
        public async Task<ActionResult> GetSubclass(int id, string refreshmenu)
        {
            var sub = await _rep.GetSubClass(id);
            var claims = await _rep.GetClaimsList();

            var model = new EditSubclass
            {
                Id = sub.Id,
                SubclassName = sub.SubclassName,
                SelectedClaims = sub.Claims.OrderBy(x => x.Action).ToList(),
                UnSelectedClaims = claims.Where(x => x.Controller == "AMU" && sub.Claims.All(c => c.Id != x.Id)).OrderBy(x => x.Action).ToList()
            };

            if (refreshmenu == "1")
            {
                // Get user claims
                var signedUser = await UserManager.FindByIdAsync(User.Identity.GetUserId());
                var userclaims = await UserManager.GetClaimsAsync(signedUser.Id);

                // Remove any claims which end in "Admin" cos these are how I differentiate admin claims. There's obviously a better way to do this..but like... yeah. *sips coffee*.
                userclaims.Remove(userclaims.FirstOrDefault(x => x.Value.ToLower().EndsWith("admin")));

                // Get branches
                var branches = new List<Branch>();
                // Temporary. I work at home sometimes and no access to database
                if (userclaims.Any(x => x.Type.ToLower() == "amu"))
                {
                    if (!bool.Parse(ConfigurationManager.AppSettings["showFullBranchList"]))
                        branches = Helper.GetTempBranchList();
                    else
                        branches = await _branch.GetBranches();
                }

                var subclasses = await _rep.GetSubClasses();

                foreach (var subclass in subclasses)
                {
                    subclass.Claims.RemoveAll(c => !userclaims.Select(x => x.Value).Contains(c.Action));
                }

                var menuModel = new MenuModel
                {
                    Username = signedUser.UserName,
                    SubClasses = subclasses.OrderBy(x => x.SubclassName).ToList(),
                    Claims = userclaims.OrderBy(x => x.Value).ToList(),
                    Branches = branches
                };

                return Json(new
                {
                    success = 1,
                    page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditSubclasses.cshtml", model),
                    menu = Helper.JsonPartialView(this, "~/Views/Shared/_SideMenu.cshtml", menuModel)
                });
            }
           
            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_EditSubclasses.cshtml", model)
            });
        }

        [ClaimRequirement("UserManagement", "ManageSubclasses")]
        public async Task<ActionResult> AddClaimsToSubclass(int id, string claims)
        {
            await _rep.AddClaimsToSubclass(id, claims, User.Identity.GetUserId());

            return await GetSubclass(id, "1");
        }

        [ClaimRequirement("UserManagement", "ManageSubclasses")]
        public async Task<ActionResult> RemoveClaimsFromSubclass(int id, string claims)
        {
            await _rep.RemoveClaimsFromSubclass(id, claims, User.Identity.GetUserId());

            return await GetSubclass(id, "1");
        }

        [ClaimRequirement("UserManagement", "ViewErrorLogs")]
        public async Task<ActionResult> GetErrorLogsPage(int page, string orderby, int perpage, int reverse)
        {
            var errors = await _rep.GetErrorLogs(perpage, page - 1, orderby, reverse);
            errors.Page = new StaticPagedList<ErrorLog>(errors.ErrorLogs, page, perpage, errors.Total);
            errors.OrderBy = orderby;
            errors.CurrentPage = page;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewErrorLogsResult.cshtml", errors)
            });
        }

        [ClaimRequirement("UserManagement", "ViewAuditTrails")]
        public async Task<ActionResult> GetAuditTrailPage(int page, string orderby, int perpage, int reverse)
        {
            var audits = await _rep.GetAuditTrails(perpage, page - 1, orderby, reverse);
            audits.Page = new StaticPagedList<Audit>(audits.Audits, page, perpage, audits.Total);
            audits.OrderBy = orderby;
            audits.CurrentPage = page;

            return Json(new
            {
                success = 1,
                page = Helper.JsonPartialView(this, "~/Views/UserManagement/_ViewAuditTrailsResult.cshtml", audits)
            });
        }

        // Operations
        private async Task<EditUserClaimsWrap> GetUserClaims(string id)
        {
            var claims = new List<EditUserClaims>();
            var user = await UserManager.FindByIdAsync(id);
            var userclaims = await UserManager.GetClaimsAsync(user.Id);
            var allclaims = await _rep.GetClaimsList();

            // selected
            foreach (var claim in userclaims)
            {
                var userclaim = claims.FirstOrDefault(x => x.Controller == claim.Type);

                if (userclaim == null)
                {
                    userclaim = new EditUserClaims
                    {
                        Controller = claim.Type
                    };

                    claims.Add(userclaim);
                }

                userclaim.SelectedClaims.Add(claim.Value);
            }

            // unselected
            foreach (var claim in allclaims)
            {
                var userclaim = claims.FirstOrDefault(x => x.Controller == claim.Controller);
                if (userclaim == null)
                {
                    userclaim = new EditUserClaims
                    {
                        Controller = claim.Controller
                    };

                    claims.Add(userclaim);
                }

                if (userclaim.Controller == claim.Controller && !userclaim.SelectedClaims.Contains(claim.Action))
                    userclaim.UnSelectedClaims.Add(claim.Action);
            }

            // order them
            foreach (var c in claims)
            {
                c.SelectedClaims = c.SelectedClaims.OrderBy(x => x).ToList();
                c.UnSelectedClaims = c.UnSelectedClaims.OrderBy(x => x).ToList();
            }

            return new EditUserClaimsWrap
            {
                UserId = id,
                Claims = claims
            };
        }
    }
}