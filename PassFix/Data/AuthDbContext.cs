﻿using Microsoft.AspNet.Identity.EntityFramework;
using PassFix.Models;
using PassFix.Models.Central;
using System.Data.Entity;

namespace PassFix.Data
{
    public class AuthDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<AuditEvent> ListAuditEvents { get; set; }
        public DbSet<UserClaim> ListUserClaims { get; set; }
        public DbSet<ClaimsSubclass> ListUserClaimsSubclasses { get; set; }
        public DbSet<State> ListStates { get; set; }
        public DbSet<Title> ListTitles { get; set; }
        public DbSet<StageCode> ListStageCodes { get; set; }
        public DbSet<AppReason> ListAppReasons { get; set; }


        public DbSet<Audit> Audits { get; set; }
        public DbSet<ErrorLog> ErrorLogs { get; set; }
        public DbSet<UserRequest> UserRequests { get; set; }
        public DbSet<EditBioRecordStore> EditBioRecordStorage { get; set; }

        public AuthDbContext(string connString) : base(connString)
        {
            Database.CommandTimeout = 20;
            //Database.SetInitializer(new DropCreateDatabaseIfModelChanges<AppDbContext>());
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ApplicationUser>()
                .Property(p => p.IsFirstTimeLogin);
            //.HasDefaultValue(1);
        }
    }
}
