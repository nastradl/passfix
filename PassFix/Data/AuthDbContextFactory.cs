﻿using PassFix.Data;
using System.Configuration;
using System.Data.Entity.Infrastructure;

namespace PassFixData.Data
{
    public class AuthDbContextFactory : IDbContextFactory<AuthDbContext>
    {
        public AuthDbContext Create()
        {
            var conn = bool.Parse(ConfigurationManager.AppSettings["useLaptopAsCentral"])
                ? ConfigurationManager.ConnectionStrings["Laptop"].ConnectionString
                : bool.Parse(ConfigurationManager.AppSettings["useLiveCentral"])
                    ? ConfigurationManager.ConnectionStrings["LiveServer"].ConnectionString
                    : ConfigurationManager.ConnectionStrings["TestServer"].ConnectionString;
            
            return new AuthDbContext(conn);
        }
    }
}
