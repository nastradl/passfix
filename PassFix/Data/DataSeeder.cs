﻿using PassFixData.Data;
using System;
using System.IO;

namespace PassFix.Data
{
    public class DataSeeder
    {
        private static readonly AuthDbContext Context = new AuthDbContextFactory().Create();
        public static void SeedData()
        {
            // NOTE: Please make sure the tables which depend on others (FK) are put in last
            var info = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent;
            if (info == null)
                return;

            var filePaths = Directory.GetFiles(
                Path.Combine(info.FullName, "Data",
                    "SeedScripts"), "*.sql",
                SearchOption.TopDirectoryOnly);

            foreach (var path in filePaths)
            {
                Context.Database.ExecuteSqlCommand(File.ReadAllText(path));
            }
        }
    }
}