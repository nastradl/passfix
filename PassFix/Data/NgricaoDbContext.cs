using Microsoft.AspNet.Identity.EntityFramework;
using PassFix.Models;
using PassFix.Models.NGRICAO;
using System.Data.Entity;

namespace PassFix.Data
{
    public class NgricaoDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<LocEnrolProfile> LocEnrolProfile { get; set; }

        public DbSet<LocDocProfile> LocDocProfile { get; set; }

        public DbSet<LocDocHolderBioProfile> LocDocHolderBioProfile { get; set; }

        public DbSet<LocDocHolderCustomProfile> LocDocHolderCustomProfile { get; set; }

        public DbSet<LocBranch> LocBranch { get; set; }

        public DbSet<LocDocInventory> LocDocInventory { get; set; }

        public DbSet<LocPaymentHistory> LocPaymentHistory { get; set; }

        public DbSet<LocPersoProfile> LocPersoProfile { get; set; }

        public DbSet<FormCache> FormCache { get; set; }

        public DbSet<LocDocHolderMainProfile> LocDocHolderMainProfile { get; set; }

        public DbSet<User> User { get; set; }

        public NgricaoDbContext(string connString) : base(connString)
        {
            Database.CommandTimeout = 20;
        }

        protected override void OnModelCreating(DbModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //builder.Entity<LocDocInventory>()
            //    .HasOne(x => x.BranchcodeNavigation)
            //    .WithMany(x => x.LocDocInventory)
            //    .HasPrincipalKey(x => x.BranchCode);

            //builder.Entity<LocDocInventory>()
            //    .HasRequired(x => x.BranchcodeNavigation)
            //    .WithMany(x => x.LocDocInventory)
            //    .HasForeignKey(x => x.Branchcode);
        }
    }
}
