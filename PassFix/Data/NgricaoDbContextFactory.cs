﻿using System.Configuration;
using System.Data.Entity.Infrastructure;


namespace PassFix.Data
{
    public class NgricaoDbContextFactory : IDbContextFactory<NgricaoDbContext>
    {
        public string Conn { get; set; }

        public NgricaoDbContext Create()
        {
            if (string.IsNullOrEmpty(Conn))
            {
                Conn = ConfigurationManager.AppSettings["useBranchDownstairs"].ToLower() == "true" ? ConfigurationManager.ConnectionStrings["TestServerIcao"].ConnectionString
                    : ConfigurationManager.ConnectionStrings["LaptopNGRICAO"].ConnectionString;
            }

            return new NgricaoDbContext(Conn);
        }
    }
}
