﻿using System.Configuration;
using System.Data.Entity.Infrastructure;


namespace PassFix.Data
{
    public class NgricaoDbContextOldFactory : IDbContextFactory<NgricaoDbContextOld>
    {
        public string Conn { get; set; }

        public NgricaoDbContextOld Create()
        {
            if (string.IsNullOrEmpty(Conn))
            {
                Conn = ConfigurationManager.AppSettings["useBranchDownstairs"].ToLower() == "true" ? ConfigurationManager.ConnectionStrings["TestServerIcao"].ConnectionString
                    : ConfigurationManager.ConnectionStrings["LaptopNGRICAO"].ConnectionString;
            }

            return new NgricaoDbContextOld(Conn);
        }
    }
}
