﻿using PassFix.Enums;
using PassFix.Extensions;
using PassFix.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using PassFix.Models.Central;

namespace PassFix.Data
{
    public class SeedData
    {
        public static async Task Initialize(IServiceProvider serviceProvider, string defaultUserPw)
        {

            using (var context = new AuthDbContext(
                serviceProvider.GetRequiredService<DbContextOptions<AuthDbContext>>()))
            {
                // Perform database delete and create
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var uid = await CreateDefaultUser(serviceProvider, defaultUserPw);
                SeedDb(serviceProvider, context, uid);
            }
        }

        private static async Task<string> CreateDefaultUser(IServiceProvider serviceProvider, string defaultUserPw)
        {
            if (string.IsNullOrEmpty(defaultUserPw))
                return "";

            const string seedUserName = "admin@irissmart.com";

            var userManager = serviceProvider.GetService<UserManager<ApplicationUser>>();

            var user = await userManager.FindByNameAsync(seedUserName);
            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = seedUserName,
                    FirstName = "Default",
                    Surname =  "Administrator"
                };

                await userManager.CreateAsync(user, defaultUserPw);

                // Add user administrator claim
                await userManager.AddClaimAsync(user, new System.Security.Claims.Claim("privilege", "admin"));
            }

            return user.Id;
        }

        public static void SeedDb(IServiceProvider serviceProvider, AuthDbContext context, string uid)
        {
            // Convert enums to table data
            context.ListAuditEvents.SeedEnumValues<AuditEvent, EnumAuditEvents>(@enum => @enum);
            context.ListStageCodes.SeedEnumValues<StageCode, EnumStageCodes>(@enum => @enum);
            context.ListAppReasons.SeedEnumValues<AppReason, EnumAppReasons>(@enum => @enum);

            var info = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent?.Parent;
            if (info != null)
            {
                string[] filePaths = Directory.GetFiles(
                    Path.Combine(info.FullName,
                        "Data"), "*.sql",
                    SearchOption.TopDirectoryOnly);

                foreach (var path in filePaths)
                {
                    context.Database.ExecuteSqlCommand(File.ReadAllText(path));
                }
            }
        }
    }
}
