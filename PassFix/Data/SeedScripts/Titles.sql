IF (EXISTS (SELECT * 
				 FROM INFORMATION_SCHEMA.TABLES 
				 WHERE TABLE_NAME = 'ListTitles'))
				 BEGIN
DELETE FROM [ListTitles]
DBCC CHECKIDENT ('ListTitles', RESEED, 0)
END
ELSE
CREATE TABLE [ListTitles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Gender] [varchar](1) NOT NULL
 CONSTRAINT [PK_ListTitles] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

SET ANSI_PADDING OFF
SET IDENTITY_INSERT [ListTitles] ON 

INSERT[ListTitles] ([id], [name], [gender]) VALUES (1, N'Mr', N'M')
INSERT[ListTitles] ([id], [name], [gender]) VALUES (2, N'Mrs', N'F')
INSERT[ListTitles] ([id], [name], [gender]) VALUES (3, N'Miss', N'F')
INSERT[ListTitles] ([id], [name], [gender]) VALUES (4, N'Dr', N'U')
SET IDENTITY_INSERT[ListTitles] OFF

