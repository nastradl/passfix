﻿using System.ComponentModel;

namespace PassFix.Enums
{
    public enum EnumAppReasons
    {
        [Description("Fresh Booklet")]
        Fresh = 1,
        [Description("Document Renewed")]
        Reissue,
        [Description("Document Lost")]
        Lost,
        [Description("Document Damaged")]
        Damaged,
        [Description("Changed Details")]
        ChangedDetail,
        [Description("Document Stolen")]
        Stolen,
    }
}
