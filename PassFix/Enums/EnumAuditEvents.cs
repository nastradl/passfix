﻿using System.ComponentModel;

namespace PassFix.Enums
{
    public enum EnumAuditEvents
    {
        [Description("User logged in successfully")]
        LoginSuccess = 1,
        [Description("User logged out")]
        LogOut,
        [Description("User failed to login with username and password")]
        LoginFailed,
        [Description("User requested to be registered")]
        RequestAdded,
        [Description("User request approved and new user created")]
        RequestApproved,
        [Description("User request denied")]
        RequestDenied,
        [Description("Assign privilege to user")]
        PrivilegeAssigned,
        [Description("Revoke privilege from user")]
        PrivilegeRevoked,
        [Description("Disable a user")]
        DisableUser,
        [Description("User updated personal details")]
        PersonalDetailsUpdated,
        [Description("User changed password")]
        PasswordChanged,
        [Description("Search enrolprofile table")]
        SearchEnrolProfile,
        [Description("Update enrolprofile stagecode from EM4000 to EM2000 and set enrollocationid to 9115001")]
        UpdateBackToApproval,
        [Description("Update enrolprofile stagecode from EM0801 to EM0800")]
        UpdateBackToPaymentCheck,
        [Description("Update persoprofile and docprofile stagecode from PM2000 to PM2001")]
        UpdateBackToPerso,
        [Description("Update enrolprofile stagecode from EM2001 to EM1500")]
        UpdateAfisRecord,
        [Description("Update record details")]
        UpdateRecordDetails,
        [Description("Update persoprofile and docprofile stagecode from PM1000 to PM0500")]
        UpdateResendJobForPrinting,
        [Description("Update docinventory stagecode from IM2000 to IM3000")]
        UpdateOverideBookletSequence,
        [Description("Update docinventory stagecode from IM3000 to IM2000")]
        UpdateBookletToPersoQueue,
        [Description("Update persoprofile and docprofile stagecode from PM2001/PM1000 to PM2000")]
        UpdateSendToIssuance,
        [Description("Search local database")]
        LocalQuery,
        [Description("Fix wrong passport issued")]
        UpdateFixWrongPassportIssued,
        [Description("Search bioprofile table")]
        SearchEditBioRecord,
        [Description("Update bioprofile Signature/Face")]
        UpdateEditBioRecord,
        [Description("Revert back to previous bioprofile Signature/Face")]
        UpdateEditBioRecordRevert,
        [Description("Search for a reversable bioprofile transaction")]
        SearchReverseEditBioRecord,
        [Description("Search for user at branch")]
        SearchBranchUser,
        [Description("Update branch user")]
        UpdateBranchUser,
        [Description("Retreive appliction temp XML file")]
        SearchXmlRecord,
        [Description("Edit XML file for application")]
        UpdateXmlFile,
        [Description("Search for record for Reissue from central")]
        SearchCreateReissueRecord,
        [Description("Create reissue record")]
        UpdateCreateReissueRecord,
        [Description("Search for record for Bind Reissue from central")]
        SearchBindReissueRecord,
        [Description("Create bind reissue record")]
        UpdateBindReissueRecord,
        [Description("Search for record")]
        SearchBackToAfisCheck,
        [Description("Search for record")]
        SearchBackToPerso,
        [Description("Search for record")]
        SearchFixWrongPassportIssued,
        [Description("Search for record")]
        SearchBackToPaymentCheck,
        [Description("Search for record")]
        SearchSendToIssuance,
        [Description("Search for payment")]
        SearchPaymentQuery,
        [Description("Edit payment")]
        UpdatePayment,
        [Description("Reset payment")]
        ResetPayment,
        [Description("Reset stagecode")]
        UpdateResetStagecode,
        [Description("Update Manually Fail")]
        UpdateManuallyFail

    }
}
