﻿using System.ComponentModel;

namespace PassFix.Enums
{
    public enum EnumStageCodes
    {
        [Description("Pending Payment Verification")]
        EM0800 = 1,
        [Description("Failed Payment Verification")]
        EM0801,
        [Description("Enrollment Completed")]
        EM1000,
        [Description("Sent to AFIS")]
        EM1500,
        [Description("AFIS Succeeded")]
        EM2000,
        [Description("AFIS Rejected")]
        EM2001,
        [Description("Approval Succeed")]
        EM3000,
        [Description("Approval Rejected")]
        EM3001,
        [Description("Approval Succeeded")]
        EM4000,
        [Description("Approval Rejected")]
        EM4001,
        [Description("Enrolment Suspended")]
        EM4009,
        [Description("Issuance: Passport Issued")]
        EM4200,
        [Description("Issuance: Rejected")]
        EM4201,
        [Description("Issuance: Pending Central Update")]
        EM4500,
        [Description("Issuance Rejected")]
        EM4501,
        [Description("Issuance: Completed")]
        EM5000,
        [Description("Issuance Failed")]
        EM5001,
        [Description("Issuance Document Lost")]
        EM5002,
        [Description("Document Stolen")]
        EM5003,
        [Description("Document Damaged")]
        EM5004,
        [Description("Document Blacklisted")]
        EM5005,
        [Description("Document Revoked")]
        EM5006,
        [Description("Null fields found")]
        FM2001,
        [Description("Document Arrived at Mainware house")]
        IM1000,
        [Description("Booklet Ready for Personalisation")]
        IM2000,
        [Description("Booklet Previously Overridden")]
        IM3000,
        [Description("Booklet Personalization Succeeded")]
        PM0500,
        [Description("Personalization failed")]
        PM0501,
        [Description("Personalization: Unable to Detect Passport")]
        PM0502,
        [Description("Personalization: Unable to Read ChipSN")]
        PM0503,
        [Description("Personalization: Unable to Write Passport Headers")]
        PM0504,
        [Description("Personalization: Unable to Write Passport Surface Data")]
        PM0505,
        [Description("Personalization: Unable to Write Facial Image")]
        PM0506,
        [Description("Personalization: Unable to Write Fingerprint Data")]
        PM0507,
        [Description("Personalization: Unable to Write Security Document")]
        PM0508,
        [Description("Personalization: Unable to Finalize Chip")]
        PM0509,
        [Description("Personalization Succeeded")]
        PM1000,
        [Description("QC Succeeded")]
        PM2000,
        [Description("QC Failed")]
        PM2001,
        [Description("New")]
        QM1000,
        [Description("Sent to AFIS")]
        QM1500,
        [Description("AFIS succeeded")]
        QM2000,
        [Description("AFIS Rejected")]
        QM2001
    }
}
