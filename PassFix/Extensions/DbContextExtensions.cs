﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PassFix.Data;
using PassFix.Enums;
using PassFix.Models.Central;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using PassFix.Models;

namespace PassFix.Extensions
{
    public class DbContextExtensions
    {
        private static AuthDbContext _context;
        private static UserManager<ApplicationUser> _userManager;
        private static string _adminPw;

        public DbContextExtensions(AuthDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public void Seed(IServiceProvider serviceProvider, string adminPassword)
        {
            _adminPw = adminPassword;

            // Perform database delete and create
            //_context.Database.EnsureDeleted();
            //_context.Database.EnsureCreated();

            // Perform seed operations
            AddEnumsToDatabase(_context);
            
            AddClaims(_context);

            AddSubClaims(_context);
            AssignSubClaims(_context);

            AddDefaultAdmin(_context);

            //Run scripts if you need to repopulate the database with lists and such
            SeedData(_context);

            // Save changes and release resources
            _context.SaveChanges();
            _context.Dispose();
        }
        
        private static void AddEnumsToDatabase(AuthDbContext context)
        {
            // Convert enums to table data
            context.ListAuditEvents.SeedEnumValues<AuditEvent, EnumAuditEvents>(@enum => @enum);
            context.ListStageCodes.SeedEnumValues<StageCode, EnumStageCodes>(@enum => @enum);
            context.ListAppReasons.SeedEnumValues<AppReason, EnumAppReasons>(@enum => @enum);
        }

        private static void AddClaims(AuthDbContext context)
        {
            // Add default claims
            var claims = new List<UserClaim>
            {
                new UserClaim
                {
                    Controller = "UserManagement",
                    Action = "ManageMyAccount"
                },
                new UserClaim
                {
                    Controller = "UserManagement",
                    Action = "ManageOtherUsers"
                },
                new UserClaim
                {
                    Controller = "UserManagement",
                    Action = "ManageUserRequests"
                },
                new UserClaim
                {
                    Controller = "UserManagement",
                    Action = "ViewAuditTrails"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "EditEnrolmentRecord"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "BackToApproval"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "BackToPerso"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "IssuePassport"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "QueryBookletInfo"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "OverideBookletSequence"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "BackToAfisCheck"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "BackToPaymentCheck"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "ResendJobForPrinting"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "BookletToPersoQueue"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "SendToIssuance"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "LocalQuery"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "FixWrongPassportIssued"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "EditBioRecord"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "ReverseEditBioRecord"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "ManageUser"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "ModifyEnrolmentFile"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "CreateReissueRecord"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "BindReissueRecord"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "Analyse Enrolment Record"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "Payment Query"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "Edit Payment Record"
                },
                new UserClaim
                {
                    Controller = "AMU",
                    Action = "Reset Payment"
                }
            };

            // Add any claims not already in the database
            var missingClaims = claims.Where(x => !context.ListUserClaims.Any(z => z.Controller == x.Controller && z.Action == x.Action)).ToList();
            context.ListUserClaims.AddRange(missingClaims);
        }

        private static void AddSubClaims(AuthDbContext context)
        {
            // Get default claim allocation. 
            // Perhaps create a module in application in future to assign this if it turns out it will be used frequently enough to warrant it
            var enrolments = new List<int> { 12, 19, 10, 11, 5, 22, 17, 25 };
            var persos = new List<int> { 6, 16, 13 };
            var booklets = new List<int> { 8, 6, 9 };
            var issuance = new List<int> { 7, 18 };
            var payment = new List<int> { 26, 27, 28 };
            var intervention = new List<int> { 23, 24 };
            var admin = new List<int> { 21, 29 };


            var enrolmentclaims = context.ListUserClaims.Where(x => enrolments.Contains(x.Id)).ToList();
            var persoclaims = context.ListUserClaims.Where(x => persos.Contains(x.Id)).ToList();
            var bookletsclaims = context.ListUserClaims.Where(x => booklets.Contains(x.Id)).ToList();
            var issuanceclaims = context.ListUserClaims.Where(x => issuance.Contains(x.Id)).ToList();
            var paymentclaims = context.ListUserClaims.Where(x => payment.Contains(x.Id)).ToList();
            var interventionclaims = context.ListUserClaims.Where(x => intervention.Contains(x.Id)).ToList();
            var adminclaims = context.ListUserClaims.Where(x => admin.Contains(x.Id)).ToList();
            
            var claims = new List<ClaimsSubclass>
            {
                new ClaimsSubclass
                {
                    SubclassName = "Enrolment Management",
                    Claims = enrolmentclaims
                },
                new ClaimsSubclass
                {
                    SubclassName = "Personalisation Management",
                    Claims = persoclaims
                },
                new ClaimsSubclass
                {
                    SubclassName = "Booklet Management",
                    Claims = bookletsclaims
                },
                new ClaimsSubclass
                {
                    SubclassName = "Issuance Management",
                    Claims = issuanceclaims
                },
                new ClaimsSubclass
                {
                    SubclassName = "Payment Management",
                    Claims = paymentclaims
                },
                new ClaimsSubclass
                {
                    SubclassName = "Intervention",
                    Claims = interventionclaims
                },
                new ClaimsSubclass
                {
                    SubclassName = "Administration",
                    Claims = adminclaims
                },
            };

            // Add any subclaims not already in the database
            var missingClaims = claims.Where(x => !context.ListUserClaimsSubclasses.Any(z => z.SubclassName == x.SubclassName)).ToList();
            context.ListUserClaimsSubclasses.AddRange(missingClaims);
        }

        private static void AssignSubClaims(AuthDbContext context)
        {

        }

        private void AddDefaultAdmin(AuthDbContext context)
        {
            var user = new ApplicationUser
            {
                FirstName = "Default",
                Surname = "Administrator",
                Email = "admin@irissmart.com",
                UserName = "Administrator",
                EmailConfirmed = true,
                DateCreated = DateTime.Now,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D")
            };

            if (!context.Users.Any(u => u.UserName == user.UserName))
            {
                var password = new PasswordHasher();
                var hashed = password.HashPassword(_adminPw);
                user.PasswordHash = hashed;

                var userStore = new UserStore<ApplicationUser>(context);
                userStore.CreateAsync(user).Wait();

                // Add user administrator claims
                var claims = context.ListUserClaims.ToList();
                var claimslist = claims.Select(claim => new Claim(claim.Controller, claim.Action)).ToList();

                foreach (var claim in claimslist)
                    _userManager.AddClaim(user.Id, claim);

                context.SaveChanges();
            }
            else
            {
                // Add all the claims in the system to admin
                var admin = context.Users.FirstOrDefault(x => x.UserName == user.UserName);
                var claims = context.ListUserClaims.ToList();
                var claimslist = claims.Select(claim => new Claim(claim.Controller, claim.Action)).ToList();

                if (admin != null)
                {
                    var adminclaims = _userManager.GetClaimsAsync(admin.Id).Result;

                    foreach (var claim in claimslist)
                    {
                        if (adminclaims.FirstOrDefault(x => x.Value == claim.Value && x.Type == claim.Type) == null)
                            _userManager.AddClaimAsync(admin.Id, claim).Wait();
                    }
                }
            }
        }

        public void SeedData(AuthDbContext context)
        {
            // NOTE: Please make sure the tables which depend on others (FK) are put in last
            var info = Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent?.Parent?.Parent;
            if (info != null)
            {
                var filePaths = Directory.GetFiles(Path.Combine(info.FullName, "Data", "SeedScripts"), "*.sql", SearchOption.TopDirectoryOnly);

                foreach (var path in filePaths)
                {
                    context.Database.ExecuteSqlCommand(File.ReadAllText(path));
                }
            }
        }
    }
}
