using PassFix.Controllers;
using System.Web.Mvc;

namespace PassFix.Extensions
{
    public static class UrlHelperExtensions
    {
        public static string EmailConfirmationLink(this UrlHelper urlHelper, string userId, string code, string scheme)
        {
            return urlHelper.Action(
                actionName: nameof(AccountController.ConfirmEmail),
                controllerName: "Account",
                routeValues: new { userId, code },
                protocol: scheme);
        }

        public static string ResetPasswordCallbackLink(this UrlHelper urlHelper, string userId, string code, string scheme)
        {
            return urlHelper.Action(
                actionName: nameof(AccountController.ResetPassword),
                controllerName: "Account",
                routeValues: new { userId, code },
                protocol: scheme);
        }
    }
}
