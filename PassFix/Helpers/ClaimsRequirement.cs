﻿using System.Security.Claims;
using System.Web.Mvc;

namespace PassFix.Helpers
{
    public class ClaimRequirement : AuthorizeAttribute
    {
        private readonly string _claimType;
        private readonly string _claimValue;
        public ClaimRequirement(string type, string value)
        {
            _claimType = type;
            _claimValue = value;
        }
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            if (filterContext.HttpContext.User is ClaimsPrincipal user && user.HasClaim(_claimType, _claimValue))
            {
                base.OnAuthorization(filterContext);
            }
            else
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
        }
    }

}

