﻿using PassFix.Enums;
using PassFix.Extensions;
using PassFix.ViewModels;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace PassFix.Helpers
{
    public static class Helper
    {
        private static readonly Random Random = new Random();

        public static string GenerateRandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        public static string SplitCamelCase(this string str)
        {
            return Regex.Replace(
                Regex.Replace(
                    str,
                    @"(\P{Ll})(\P{Ll}\p{Ll})",
                    "$1 $2"
                ),
                @"(\p{Ll})(\P{Ll})",
                "$1 $2"
            );
        }

        public static List<Branch> GetTempBranchList()
        {
            return new List<Branch>
            {
                new Branch
                {
                    BranchCode = "001",
                    BranchId = 1,
                    BranchName = "Downstairs",
                    Ip = "172.31.10.105"
                }
            };
        }

        public static string GetStageCodeDescription(string stageCode)
        {
            if (string.IsNullOrEmpty(stageCode))
                return string.Empty;

            var code = (EnumStageCodes)Enum.Parse(typeof(EnumStageCodes), stageCode);
            return code.GetEnumDescription();
        }

        public static T DeepCopy<T>(T other)
        {
            using (var ms = new MemoryStream())
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(ms, other);
                ms.Position = 0;
                return (T)formatter.Deserialize(ms);
            }
        }

        public static string JsonPartialView(Controller controller, string viewName, object model)
        {
            controller.ViewData.Model = model;
            using (var stringWriter = new StringWriter())
            {
                var partialView = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
                var viewContext = new ViewContext(controller.ControllerContext, partialView.View, controller.ViewData, controller.TempData, stringWriter);
                partialView.View.Render(viewContext, stringWriter);
                partialView.ViewEngine.ReleaseView(controller.ControllerContext, partialView.View);
                return stringWriter.ToString();
            }
        }

        public static string DecryptConfig(string encrypted, bool isConnString = true)
        {
            var fluff = ConfigurationManager.AppSettings["salt"];

            if (!isConnString)
                return encrypted.Replace(fluff.ToArray(), "");

            var builder = new SqlConnectionStringBuilder(encrypted);
            string password = builder.Password;

            var plain = password.Replace(fluff.ToArray(), "");
            builder.Password = plain;

            return builder.ConnectionString;
        }


    }
}
