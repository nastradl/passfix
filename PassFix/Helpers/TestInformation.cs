﻿using PassFix.Data;
using PassFix.ViewModels;
using System.Collections.Generic;
using System.Linq;

namespace PassFix.Helpers
{
    public class TestInformation 
    {
        private readonly NgricaoDbContext _context = new NgricaoDbContextFactory().Create();

        public static List<TestInfo> GetTestData(string location)
        {
            var result = new List<TestInfo>();
            switch (location)
            {
                case "EditEnrolmentRecord":
                {
                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000889416"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000936633"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = false,
                        Value = "369000870680"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = false,
                        Value = "369000874165"
                    });

                    break;
                }
                case "ManageUser":
                {
                    result.Add(new TestInfo
                    {
                        Name = "Login Name",
                        Pass = true,
                        Value = "Yahaya"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "Login Name",
                        Pass = true,
                        Value = "Lamidi"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "Login Name",
                        Pass = true,
                        Value = "Nana"
                    });

                    break;
                }
                case "BackToApproval":
                {
                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000906789"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = false,
                        Value = "369000889416"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = false,
                        Value = "369000903131"
                    });

                    break;
                }

                case "FixWrongPassportIssued":
                {
                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000874165"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = false,
                        Value = "369000903131"
                    });

                    break;
                }
                case "ResendJobForPrinting":
                {
                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000876334"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000936633"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = false,
                        Value = "369000931187"
                    });

                    break;
                }
                case "BackToPerso":
                {
                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000833099"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "DocNo",
                        Pass = true,
                        Value = "A06937532"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = false,
                        Value = "369000931187"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = false,
                        Value = "369000931098"
                    });

                    break;
                }
                case "IssuePassport":
                {
                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000931388"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "DocNo",
                        Pass = true,
                        Value = "A06870173"
                    });
                        
                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = false,
                        Value = "369000900476"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = false,
                        Value = "369000934955"
                    });

                    break;
                }
                case "QueryBookletInfo":
                {
                    result.Add(new TestInfo
                    {
                        Name = "DocNo",
                        Pass = true,
                        Value = "A06867554"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "DocNo",
                        Pass = true,
                        Value = "A06869017"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "DocNo",
                        Pass = true,
                        Value = "A06869185"
                    });

                    break;
                }
                case "OverideBookletSequence":
                {
                    result.Add(new TestInfo
                    {
                        Name = "DocNo",
                        Pass = true,
                        Value = "A06937940"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "DocNo",
                        Pass = true,
                        Value = "A06937964"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "DocNo",
                        Pass = false,
                        Value = "A06870990"
                    });
                        
                    break;
                }

                case "BackToAfisCheck":
                {
                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000904559"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000912562"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = false,
                        Value = "369000920833"
                    });

                    break;
                }

                case "BackToPaymentCheck":
                {
                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000914914"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000919183"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = false,
                        Value = "369000900476"
                    });

                    break;
                }

                case "BookletToPersoQueue":
                {
                    result.Add(new TestInfo
                    {
                        Name = "DocNo",
                        Pass = true,
                        Value = "A06870990"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "DocNo",
                        Pass = false,
                        Value = "A06937964"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "DocNo",
                        Pass = false,
                        Value = "A06938124"
                    });

                    break;
                }


                case "SendToIssuance":
                {
                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000931098"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000939386"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = false,
                        Value = "369000900476"
                    });

                    break;
                }

                case "CreateReissueRecord":
                    {
                        result.Add(new TestInfo
                        {
                            Name = "DocNo",
                            Pass = true,
                            Value = "A03599000"
                        });

                        result.Add(new TestInfo
                        {
                            Name = "DocNo",
                            Pass = true,
                            Value = "A05404540"
                        });

                        break;
                    }

                case "BindReissueRecord":
                    {
                        result.Add(new TestInfo
                        {
                            Name = "FormNo",
                            Pass = true,
                            Value = "369000904559"
                        });

                        result.Add(new TestInfo
                        {
                            Name = "DocNo",
                            Pass = true,
                            Value = "A03599000"
                        });

                        break;
                    }

                case "LocalQuery":
                {
                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000900476"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "First Name",
                        Pass = true,
                        Value = "Bala"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "Surname",
                        Pass = true,
                        Value = "Judith"
                    });

                    break;
                }

                case "EditBioRecord":
                {
                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000931187"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "369000920833"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = false,
                        Value = "369000849145"
                    });

                    break;
                }

                case "ModifyEnrolmentFile":
                {
                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "1010000020691"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "1010000016601"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "FormNo",
                        Pass = true,
                        Value = "1010000000332"
                    });

                    result.Add(new TestInfo
                    {
                        Name = "Password",
                        Pass = true,
                        Value = "SecretNovascale2016"
                    });

                    break;
                }
            }
          
            return result;
        }

        public void ResetTestData()
        {
            // BackToApproval
            var forms1 = new List<string> { "369000906789" };
            var changes1 = _context.LocEnrolProfile.Where(x => forms1.Contains(x.Formno)).ToList();
            foreach (var change in changes1)
            {
                change.Stagecode = "EM4000";
            }
            _context.SaveChanges();

            // ResendJobForPrinting
            var forms2 = new List<string> { "369000876334", "369000936633" };
            var changes2 = _context.LocPersoProfile.Where(x => forms2.Contains(x.Formno)).ToList();
            foreach (var change in changes2)
            {
                change.Stagecode = "PM1000";
            }
            _context.SaveChanges();

            var changes3 = _context.LocDocProfile.Where(x => forms2.Contains(x.Formno)).ToList();
            foreach (var change in changes3)
            {
                change.Stagecode = "PM1000";
            }
            _context.SaveChanges();

            // BackToPerso
            var forms3 = new List<string> { "369000833099", "369000849145" };
            var changes4 = _context.LocPersoProfile.Where(x => forms3.Contains(x.Formno)).ToList();
            foreach (var change in changes4)
            {
                change.Stagecode = "PM2000";
            }
            _context.SaveChanges();

            var changes5 = _context.LocDocProfile.Where(x => forms3.Contains(x.Formno)).ToList();
            foreach (var change in changes5)
            {
                change.Stagecode = "PM2000";
            }
            _context.SaveChanges();

            // IssuePassport
            var forms4 = new List<string> { "369000931388" };
            var changes6 = _context.LocEnrolProfile.Where(x => forms4.Contains(x.Formno)).ToList();
            foreach (var change in changes6)
            {
                change.Stagecode = "EM4000";
            }
            _context.SaveChanges();

            var changes7 = _context.LocDocProfile.Where(x => forms4.Contains(x.Formno)).ToList();
            foreach (var change in changes7)
            {
                change.Stagecode = "PM2000";
            }
            _context.SaveChanges();

            // OverideBookletSequence
            var forms5 = new List<string> { "A06937940", "A06937964" };
            var changes8 = _context.LocDocInventory.Where(x => forms5.Contains(x.Docno)).ToList();
            foreach (var change in changes8)
            {
                change.Stagecode = "IM2000";
            }
            _context.SaveChanges();

            // BackToAfisCheck
            var forms6 = new List<string> { "369000904559", "369000912562" };
            var changes9 = _context.LocEnrolProfile.Where(x => forms6.Contains(x.Formno)).ToList();
            foreach (var change in changes9)
            {
                change.Stagecode = "EM2001";
            }
            _context.SaveChanges();

            // BackToPaymentCheck
            var forms7 = new List<string> { "369000914914", "369000919183" };
            var changes10 = _context.LocEnrolProfile.Where(x => forms7.Contains(x.Formno)).ToList();
            foreach (var change in changes10)
            {
                change.Stagecode = "EM0801";
            }
            _context.SaveChanges();

            // SendToIssuance 
            var forms8 = new List<string> { "369000931098" };
            var changes11 = _context.LocPersoProfile.Where(x => forms8.Contains(x.Formno)).ToList();
            foreach (var change in changes11)
            {
                change.Stagecode = "PM1000";
            }
            _context.SaveChanges();

            var changes12 = _context.LocDocProfile.Where(x => forms8.Contains(x.Formno)).ToList();
            foreach (var change in changes12)
            {
                change.Stagecode = "PM1000";
            }
            _context.SaveChanges();

            // BookletToPersoQueue
            var forms9 = new List<string> { "A06870990", "A06937964", "A06938124" };
            var changes13 = _context.LocDocInventory.Where(x => forms9.Contains(x.Docno)).ToList();
            foreach (var change in changes13)
            {
                change.Stagecode = "IM3000";
            }
            _context.SaveChanges();

            // FixWrongPassportIssued
            var forms10 = new List<string> { "369000874165" };
            var changes14 = _context.LocDocProfile.Where(x => forms10.Contains(x.Formno)).ToList();
            var changes15 = _context.LocEnrolProfile.Where(x => forms10.Contains(x.Formno)).ToList();

            foreach (var change in changes14)
            {
                change.Stagecode = "EM5000";
            }

            foreach (var change in changes15)
            {
                change.Stagecode = "EM5000";
            }
            _context.SaveChanges();
        }
    }
}
