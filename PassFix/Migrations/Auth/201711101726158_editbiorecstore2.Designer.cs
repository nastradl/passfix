// <auto-generated />
namespace PassFix.Migrations.Auth
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class editbiorecstore2 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(editbiorecstore2));
        
        string IMigrationMetadata.Id
        {
            get { return "201711101726158_editbiorecstore2"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
