namespace PassFix.Migrations.Auth
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class audit2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Audits", "FormNo", c => c.String(maxLength: 12));
            AlterColumn("dbo.Audits", "DocNo", c => c.String(maxLength: 12));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Audits", "DocNo", c => c.String());
            AlterColumn("dbo.Audits", "FormNo", c => c.String());
        }
    }
}
