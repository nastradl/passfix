namespace PassFix.Migrations.Auth
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subclasses : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AspNetUserClaimsSubclass",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SubclassName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.ListUserClaims", "ClaimsSubclass_Id", c => c.Int());
            CreateIndex("dbo.ListUserClaims", "ClaimsSubclass_Id");
            AddForeignKey("dbo.ListUserClaims", "ClaimsSubclass_Id", "dbo.AspNetUserClaimsSubclass", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ListUserClaims", "ClaimsSubclass_Id", "dbo.AspNetUserClaimsSubclass");
            DropIndex("dbo.ListUserClaims", new[] { "ClaimsSubclass_Id" });
            DropColumn("dbo.ListUserClaims", "ClaimsSubclass_Id");
            DropTable("dbo.AspNetUserClaimsSubclass");
        }
    }
}
