namespace PassFix.Migrations.Auth
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class subclasses2 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.AspNetUserClaimsSubclass", newName: "UserClaimsSubclass");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.UserClaimsSubclass", newName: "AspNetUserClaimsSubclass");
        }
    }
}
