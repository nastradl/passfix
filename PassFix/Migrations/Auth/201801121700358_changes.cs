namespace PassFix.Migrations.Auth
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class changes : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.ListUserClaims", name: "ClaimsSubclass_Id", newName: "Subclass_Id");
            RenameIndex(table: "dbo.ListUserClaims", name: "IX_ClaimsSubclass_Id", newName: "IX_Subclass_Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.ListUserClaims", name: "IX_Subclass_Id", newName: "IX_ClaimsSubclass_Id");
            RenameColumn(table: "dbo.ListUserClaims", name: "Subclass_Id", newName: "ClaimsSubclass_Id");
        }
    }
}
