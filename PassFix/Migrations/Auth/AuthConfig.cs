using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PassFix.Data;
using PassFix.Enums;
using PassFix.Extensions;
using PassFix.Models;
using PassFix.Models.Central;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Security.Claims;

namespace PassFix.Migrations.Auth
{
    using System.Data.Entity.Migrations;

    internal sealed class AuthConfig : DbMigrationsConfiguration<AuthDbContext>
    {
        public AuthConfig()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations\Auth";
        }

        protected override void Seed(AuthDbContext context)
        {
            // Run scripts if you need to repopulate the database with lists and such
            DataSeeder.SeedData();

            // Convert enums to table data
            context.ListAuditEvents.SeedEnumValues<AuditEvent, EnumAuditEvents>(@enum => @enum);
            context.ListStageCodes.SeedEnumValues<StageCode, EnumStageCodes>(@enum => @enum);
            context.ListAppReasons.SeedEnumValues<AppReason, EnumAppReasons>(@enum => @enum);

            AddClaims(context);

            AddSubClaims(context);
           
            AddDefaultAdmin(context);

            // Save changes and release resources
            context.SaveChanges();
            //context.Dispose();
        }

        private static void AddClaims(AuthDbContext context)
        {
            var usermanagement = new List<string>
            {
                "ManageMyAccount", "ManageOtherUsers", "ManageUserRequests", "ViewAuditTrails", "ViewErrorLogs", "ManageSubclasses"
            };

            var amu = new List<string>
            {
                "EditEnrolmentRecord", "BackToApproval", "BackToPerso", "IssuePassport", "QueryBookletInfo", "OverideBookletSequence", "BackToAfisCheck", "BackToPaymentCheck",
                "ResendJobForPrinting", "BookletToPersoQueue", "SendToIssuance", "LocalQuery", "FixWrongPassportIssued", "EditBioRecord", "ReverseEditBioRecord", "ManageUser",
                "ModifyEnrolmentFile", "CreateReissueRecord", "BindReissueRecord", "PaymentQuery", "EditPaymentRecord", "ResetPayment",  "AnalyseRecord", "PaymentQueryAdmin",
                "ResetStagecode", "ManuallyFail"
            };

            var claims = new List<UserClaim>();

            // Add usermanagement claims
            foreach (var action in usermanagement)
            {
                claims.Add(new UserClaim
                {
                    Controller = "UserManagement",
                    Action = action
                });
            }

            // Add AMU claims
            foreach (var action in amu)
            {
                claims.Add(new UserClaim
                {
                    Controller = "AMU",
                    Action = action
                });
            }

            // Add any claims not already in the database
            var missingClaims = claims.Where(x => !context.ListUserClaims.Any(z => z.Controller == x.Controller && z.Action == x.Action)).ToList();
            context.ListUserClaims.AddRange(missingClaims);
        }

        private void AddDefaultAdmin(AuthDbContext context)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            string defaultpw = ConfigurationManager.AppSettings["defaultAdminPw"];
            var user = new ApplicationUser
            {
                FirstName = "Default",
                Surname = "Administrator",
                Email = "admin@irissmart.com",
                UserName = "Administrator",
                EmailConfirmed = true,
                DateCreated = DateTime.Now,
                PhoneNumberConfirmed = true,
                SecurityStamp = Guid.NewGuid().ToString("D")
            };

            if (!context.Users.Any(u => u.UserName == user.UserName))
            {
                var password = new PasswordHasher();
                var hashed = password.HashPassword(defaultpw);
                user.PasswordHash = hashed;

                var userStore = new UserStore<ApplicationUser>(context);
                userStore.CreateAsync(user).Wait();

                // Add user administrator claims
                var claims = context.ListUserClaims.ToList();
                var claimslist = claims.Select(claim => new Claim(claim.Controller, claim.Action)).ToList();

                foreach (var claim in claimslist)
                    userManager.AddClaimAsync(user.Id, claim).Wait();

                context.SaveChanges();
            }
            else
            {
                // Add all the claims in the system to admin
                var admin = context.Users.FirstOrDefault(x => x.UserName == user.UserName);
                var claims = context.ListUserClaims.ToList();
                var claimslist = claims.Select(claim => new Claim(claim.Controller, claim.Action)).ToList();

                var adminclaims = userManager.GetClaimsAsync(admin?.Id).Result;

                foreach (var claim in claimslist)
                {
                    if (adminclaims.FirstOrDefault(x => x.Value == claim.Value && x.Type == claim.Type) == null)
                        userManager.AddClaimAsync(admin?.Id, claim).Wait();
                }
            }
        }

        private static void AddSubClaims(AuthDbContext context)
        {
            // Get default claims allocation. 
            var enrolments = new List<string>
            {
                "BackToApproval",
                "BackToAfisCheck",
                "BackToPaymentCheck",
                "EditEnrolmentRecord",
                "LocalQuery",
                "EditBioRecord",
                "ReverseEditBioRecord",
                "ModifyEnrolmentFile",
                "AnalyseRecord",
                "ResetStagecode",
                "ManuallyFail"
            };
                
            var persos = new List<string> {
                "ResendJobForPrinting",
                "SendToIssuance"
            };
            
            var booklets = new List<string>  {
                "BackToPerso",
                "QueryBookletInfo",
                "OverideBookletSequence"
            };

            var issuance = new List<string>  {
                "IssuePassport",
                "FixWrongPassportIssued"
            };

            var payment = new List<string>  {
                "PaymentQuery",
                "PaymentQueryAdmin",
                "EditPaymentRecord",
                "ResetPayment"
            };

            var intervention = new List<string> {
                "CreateReissueRecord",
                "BindReissueRecord"
            };

            var admin = new List<string> {
                "ManageUser"
            };

            // Please make sure action names are unique
            var enrolmentclaims = context.ListUserClaims.Where(x => enrolments.Contains(x.Action)).ToList();
            var persoclaims = context.ListUserClaims.Where(x => persos.Contains(x.Action)).ToList();
            var bookletsclaims = context.ListUserClaims.Where(x => booklets.Contains(x.Action)).ToList();
            var issuanceclaims = context.ListUserClaims.Where(x => issuance.Contains(x.Action)).ToList();
            var paymentclaims = context.ListUserClaims.Where(x => payment.Contains(x.Action)).ToList();
            var interventionclaims = context.ListUserClaims.Where(x => intervention.Contains(x.Action)).ToList();
            var adminclaims = context.ListUserClaims.Where(x => admin.Contains(x.Action)).ToList();

            var claims = new List<ClaimsSubclass>
            {
                new ClaimsSubclass
                {
                    SubclassName = "Enrolment",
                    Claims = enrolmentclaims
                },
                new ClaimsSubclass
                {
                    SubclassName = "Personalisation",
                    Claims = persoclaims
                },
                new ClaimsSubclass
                {
                    SubclassName = "Booklet",
                    Claims = bookletsclaims
                },
                new ClaimsSubclass
                {
                    SubclassName = "Issuance",
                    Claims = issuanceclaims
                },
                new ClaimsSubclass
                {
                    SubclassName = "Payment",
                    Claims = paymentclaims
                },
                new ClaimsSubclass
                {
                    SubclassName = "Intervention",
                    Claims = interventionclaims
                },
                new ClaimsSubclass
                {
                    SubclassName = "Administration",
                    Claims = adminclaims
                },
            };

            //LaunchDiagnostics();

            // Add any subclaims not already in the database
            var missingClaims = claims.Where(x => !context.ListUserClaimsSubclasses.Any(z => z.SubclassName == x.SubclassName)).ToList();
            context.ListUserClaimsSubclasses.AddRange(missingClaims);

            // Also add any claims related not added
            var curr = context.ListUserClaimsSubclasses.ToList();
            for (int i = 0; i < curr.Count; i++)
            {
                var related = claims.FirstOrDefault(x => x.SubclassName == curr[i].SubclassName);

                if (related != null)
                {
                    // remove all ending with "admin"
                    for (int z = 0; z < related.Claims.Count; z++)
                        if (related.Claims[z].Action.ToLower().EndsWith("admin"))
                            related.Claims.Remove(related.Claims[z]);

                    if (curr[i].Claims == null)
                        curr[i].Claims = related.Claims;
                    else
                    {
                        var missing = related.Claims.Where(x => curr[i].Claims.All(z => z.Action != x.Action)).ToList();

                        if (missing.Count != 0)
                            curr[i].Claims.AddRange(missing);
                    }
                }
            }


        }

        private static void LaunchDiagnostics()
        {
            if (System.Diagnostics.Debugger.IsAttached == false)
                System.Diagnostics.Debugger.Launch();
        }
    }
}
