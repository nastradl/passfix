namespace PassFix.Migrations.Ngricao
{
    using System.Data.Entity.Migrations;

    public partial class inventory : DbMigration
    {
        public override void Up()
        {
            //DropForeignKey("dbo.Loc_DocInventory", "BranchcodeNavigation_Id", "dbo.Loc_Branch");
            //DropIndex("dbo.Loc_DocInventory", new[] { "BranchcodeNavigation_Id" });
            //RenameColumn(table: "dbo.Loc_DocInventory", name: "BranchcodeNavigation_Id", newName: "LocBranch_Id");
            //AlterColumn("dbo.Loc_DocInventory", "LocBranch_Id", c => c.Long());
            //CreateIndex("dbo.Loc_DocInventory", "LocBranch_Id");
            //AddForeignKey("dbo.Loc_DocInventory", "LocBranch_Id", "dbo.Loc_Branch", "Id");
        }
        
        public override void Down()
        {
            //DropForeignKey("dbo.Loc_DocInventory", "LocBranch_Id", "dbo.Loc_Branch");
            //DropIndex("dbo.Loc_DocInventory", new[] { "LocBranch_Id" });
            //AlterColumn("dbo.Loc_DocInventory", "LocBranch_Id", c => c.Long(nullable: false));
            //RenameColumn(table: "dbo.Loc_DocInventory", name: "LocBranch_Id", newName: "BranchcodeNavigation_Id");
            //CreateIndex("dbo.Loc_DocInventory", "BranchcodeNavigation_Id");
            //AddForeignKey("dbo.Loc_DocInventory", "BranchcodeNavigation_Id", "dbo.Loc_Branch", "Id", cascadeDelete: true);
        }
    }
}
