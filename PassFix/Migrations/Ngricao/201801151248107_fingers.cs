namespace PassFix.Migrations.Ngricao
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fingers : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger5Code", c => c.String(maxLength: 12));
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger5Reason", c => c.Int());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger5Image", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger5Template1", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger5Template2", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger6Code", c => c.String(maxLength: 12));
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger6Reason", c => c.Int());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger6Image", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger6Template1", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger6Template2", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger7Code", c => c.String(maxLength: 12));
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger7Reason", c => c.Int());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger7Image", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger7Template1", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger7Template2", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger8Code", c => c.String(maxLength: 12));
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger8Reason", c => c.Int());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger8Image", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger8Template1", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger8Template2", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger9Code", c => c.String(maxLength: 12));
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger9Reason", c => c.Int());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger9Image", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger9Template1", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger9Template2", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger10Code", c => c.String(maxLength: 12));
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger10Reason", c => c.Int());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger10Image", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger10Template1", c => c.Binary());
            //AddColumn("dbo.Loc_DocHolderBioProfile", "Finger10Template2", c => c.Binary());
        }
        
        public override void Down()
        {
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger10Template2");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger10Template1");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger10Image");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger10Reason");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger10Code");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger9Template2");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger9Template1");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger9Image");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger9Reason");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger9Code");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger8Template2");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger8Template1");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger8Image");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger8Reason");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger8Code");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger7Template2");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger7Template1");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger7Image");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger7Reason");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger7Code");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger6Template2");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger6Template1");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger6Image");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger6Reason");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger6Code");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger5Template2");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger5Template1");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger5Image");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger5Reason");
            //DropColumn("dbo.Loc_DocHolderBioProfile", "Finger5Code");
        }
    }
}
