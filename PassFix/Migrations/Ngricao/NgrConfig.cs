namespace PassFix.Migrations.Ngricao
{
    using System.Data.Entity.Migrations;

    internal sealed class NgrConfig : DbMigrationsConfiguration<PassFix.Data.NgricaoDbContext>
    {
        public NgrConfig()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations\Ngricao";
        }

        protected override void Seed(PassFix.Data.NgricaoDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
