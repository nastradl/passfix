namespace PassFix.Migrations.OldNgricao
{
    using System.Data.Entity.Migrations;

    public partial class initial : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.FormCache",
            //    c => new
            //        {
            //            FormNo = c.Long(nullable: false, identity: true),
            //        })
            //    .PrimaryKey(t => t.FormNo);
            
            //CreateTable(
            //    "dbo.Loc_Branch",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            BranchName = c.String(nullable: false, maxLength: 30),
            //            BranchCode = c.String(nullable: false, maxLength: 5),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.Loc_DocHolderBioProfile",
            //    c => new
            //        {
            //            Formno = c.String(nullable: false, maxLength: 12),
            //            Faceimage = c.Binary(),
            //            Faceimagej2K = c.Binary(),
            //            Faceentrytime = c.DateTime(),
            //            Finger1Image = c.Binary(),
            //            Finger1Template1 = c.Binary(),
            //            Finger1Template2 = c.Binary(),
            //            Finger1Code = c.String(maxLength: 2),
            //            Finger1Reason = c.Int(),
            //            Finger2Image = c.Binary(),
            //            Finger2Template1 = c.Binary(),
            //            Finger2Template2 = c.Binary(),
            //            Finger2Code = c.String(maxLength: 12),
            //            Finger2Reason = c.Int(),
            //            Finger3Image = c.Binary(),
            //            Finger3Template1 = c.Binary(),
            //            Finger3Template2 = c.Binary(),
            //            Finger3Code = c.String(maxLength: 12),
            //            Finger3Reason = c.Int(),
            //            Finger4Image = c.Binary(),
            //            Finger4Template1 = c.Binary(),
            //            Finger4Template2 = c.Binary(),
            //            Finger4Code = c.String(maxLength: 12),
            //            Finger4Reason = c.Int(),
            //            Fingerentrytime = c.DateTime(),
            //            Signimage = c.Binary(),
            //            Signentrytime = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.Formno);
            
            //CreateTable(
            //    "dbo.Loc_DocHolderCustomProfile",
            //    c => new
            //        {
            //            Formno = c.String(nullable: false, maxLength: 12),
            //            Othername1 = c.String(maxLength: 30),
            //            Othername2 = c.String(maxLength: 30),
            //            Othername3 = c.String(maxLength: 30),
            //            Othername4 = c.String(maxLength: 30),
            //            Dobflag = c.Int(),
            //            Originstate = c.String(maxLength: 25),
            //            Addstreet = c.String(maxLength: 25),
            //            Addtown = c.String(maxLength: 25),
            //            Addstate = c.String(maxLength: 25),
            //            Profession = c.String(maxLength: 25),
            //            Occupation = c.String(maxLength: 25),
            //            Height = c.Int(),
            //            Eyecolor = c.Int(),
            //            Haircolor = c.Int(),
            //            Maritalstatus = c.Int(),
            //            Birthmark = c.String(maxLength: 50),
            //            Maidenname = c.String(maxLength: 25),
            //            Nokname = c.String(maxLength: 25),
            //            Nokaddress = c.String(maxLength: 50),
            //            Child1Sname = c.String(maxLength: 30),
            //            Child1Fname = c.String(maxLength: 30),
            //            Child1Dob = c.DateTime(),
            //            Child1Pobt = c.String(maxLength: 25),
            //            Child1Pobs = c.String(maxLength: 25),
            //            Child1Sex = c.String(maxLength: 1),
            //            Child2Sname = c.String(maxLength: 30),
            //            Child2Fname = c.String(maxLength: 30),
            //            Child2Dob = c.DateTime(),
            //            Child2Pobt = c.String(maxLength: 25),
            //            Child2Pobs = c.String(maxLength: 25),
            //            Child2Sex = c.String(maxLength: 1),
            //            Child3Sname = c.String(maxLength: 30),
            //            Child3Fname = c.String(maxLength: 30),
            //            Child3Dob = c.DateTime(),
            //            Child3Pobt = c.String(maxLength: 25),
            //            Child3Pobs = c.String(maxLength: 25),
            //            Child3Sex = c.String(maxLength: 1),
            //            Child4Sname = c.String(maxLength: 30),
            //            Child4Fname = c.String(maxLength: 30),
            //            Child4Dob = c.DateTime(),
            //            Child4Pobt = c.String(maxLength: 25),
            //            Child4Pobs = c.String(maxLength: 25),
            //            Child4Sex = c.String(maxLength: 1),
            //            Title = c.String(maxLength: 20),
            //            Eyecolorother = c.String(maxLength: 20),
            //            Haircolorother = c.String(maxLength: 20),
            //            Addpermanent = c.String(maxLength: 50),
            //            Email = c.String(maxLength: 50),
            //            Contactphone = c.String(maxLength: 50),
            //            Mobilephone = c.String(maxLength: 50),
            //            Gtoname = c.String(maxLength: 60),
            //            Gtoaddress = c.String(maxLength: 50),
            //        })
            //    .PrimaryKey(t => t.Formno);
            
            //CreateTable(
            //    "dbo.Loc_DocHolderMainProfile",
            //    c => new
            //        {
            //            Formno = c.String(nullable: false, maxLength: 12),
            //            Surname = c.String(maxLength: 12),
            //            Firstname = c.String(maxLength: 12),
            //            Birthdate = c.DateTime(),
            //            Birthtown = c.String(maxLength: 25),
            //            Birthstate = c.String(maxLength: 25),
            //            Sex = c.String(maxLength: 1),
            //            Nationality = c.String(maxLength: 3),
            //            Personalno = c.String(maxLength: 16),
            //        })
            //    .PrimaryKey(t => t.Formno);
            
            //CreateTable(
            //    "dbo.Loc_DocProfile",
            //    c => new
            //        {
            //            Docno = c.String(nullable: false, maxLength: 12),
            //            Formno = c.String(maxLength: 12),
            //            Doctype = c.String(maxLength: 2),
            //            Expirydate = c.DateTime(),
            //            Issuedate = c.DateTime(),
            //            Issueplace = c.String(maxLength: 25),
            //            Printtime = c.DateTime(),
            //            Printby = c.String(maxLength: 20),
            //            Encodetime = c.DateTime(),
            //            Encodeby = c.String(maxLength: 20),
            //            Qctime = c.DateTime(),
            //            Qcby = c.String(maxLength: 20),
            //            Issuetime = c.DateTime(),
            //            Issueby = c.String(maxLength: 20),
            //            Authoritycode = c.String(maxLength: 12),
            //            Acqbatch = c.String(maxLength: 50),
            //            Branchcode = c.String(maxLength: 5),
            //            Appreason = c.Int(),
            //            Stagecode = c.String(maxLength: 6),
            //        })
            //    .PrimaryKey(t => t.Docno)
            //    .ForeignKey("dbo.Loc_DocHolderMainProfile", t => t.Formno)
            //    .Index(t => t.Formno);
            
            //CreateTable(
            //    "dbo.Loc_DocInventory",
            //    c => new
            //        {
            //            Chipsn = c.String(nullable: false, maxLength: 18),
            //            Doctype = c.String(maxLength: 2),
            //            Docno = c.String(nullable: false, maxLength: 12),
            //            Transfertime = c.DateTime(),
            //            Branchcode = c.String(maxLength: 5),
            //            Stagecode = c.String(maxLength: 6),
            //            Boxsn = c.String(maxLength: 50),
            //            Entrytime = c.DateTime(),
            //        })
            //    .PrimaryKey(t => t.Chipsn);
            
            //CreateTable(
            //    "dbo.Loc_EnrolProfile",
            //    c => new
            //        {
            //            Formno = c.String(nullable: false, maxLength: 12),
            //            Appreason = c.Int(nullable: false),
            //            Branchcode = c.String(nullable: false, maxLength: 5),
            //            Enrollocationid = c.Long(nullable: false),
            //            Layoutid = c.Long(nullable: false),
            //            Olddocno = c.String(maxLength: 12),
            //            Doctype = c.String(maxLength: 2),
            //            Fileno = c.String(maxLength: 15),
            //            Nofinger = c.Int(nullable: false),
            //            Enrolby = c.String(nullable: false, maxLength: 20),
            //            Enroltime = c.DateTime(nullable: false),
            //            Appby = c.String(maxLength: 20),
            //            Apptime = c.DateTime(),
            //            Remarks = c.String(maxLength: 1024),
            //            Stagecode = c.String(nullable: false, maxLength: 6),
            //            Priority = c.Int(nullable: false),
            //            Overwriteafisby = c.String(maxLength: 12),
            //            Overwriteafistime = c.DateTime(),
            //            Overwritepriorityby = c.String(maxLength: 20),
            //            Overwriteprioritytime = c.DateTime(),
            //            Overwriteadminrejectby = c.String(maxLength: 20),
            //            Overwriteadminrejecttime = c.DateTime(),
            //            Overwriteotherby = c.String(maxLength: 20),
            //            Overwriteothertime = c.DateTime(),
            //            Thirdpartyissueby = c.String(maxLength: 20),
            //            Thirdpartyissuetime = c.DateTime(),
            //            Issueby = c.String(maxLength: 20),
            //            Issuetime = c.DateTime(),
            //            Docpage = c.String(maxLength: 2),
            //        })
            //    .PrimaryKey(t => t.Formno);
            
            //CreateTable(
            //    "dbo.Loc_PaymentHistory",
            //    c => new
            //        {
            //            Formno = c.String(nullable: false, maxLength: 12),
            //            Bankdraftno = c.String(maxLength: 20),
            //            Pymttime = c.DateTime(),
            //            Pymtrecvby = c.String(maxLength: 20),
            //            Pymtamt = c.Int(),
            //            Bankname = c.String(maxLength: 30),
            //            Receiptno = c.String(maxLength: 15),
            //            Refno = c.String(maxLength: 50),
            //            Appid = c.String(maxLength: 50),
            //        })
            //    .PrimaryKey(t => t.Formno);
            
            //CreateTable(
            //    "dbo.Loc_PersoProfile",
            //    c => new
            //        {
            //            Formno = c.String(nullable: false, maxLength: 12),
            //            Docno = c.String(maxLength: 12),
            //            Persotime = c.DateTime(),
            //            Branchcode = c.String(nullable: false, maxLength: 5),
            //            Stagecode = c.String(nullable: false, maxLength: 6),
            //            Persolocationid = c.Long(),
            //            Layoutid = c.Long(),
            //            Doctype = c.String(nullable: false, maxLength: 2),
            //            Issuedate = c.DateTime(),
            //            Expirydate = c.DateTime(),
            //            Priority = c.Int(),
            //            Remarks = c.String(maxLength: 50),
            //            Chipsn = c.String(maxLength: 18),
            //        })
            //    .PrimaryKey(t => t.Formno);
            
            //CreateTable(
            //    "dbo.AspNetRoles",
            //    c => new
            //        {
            //            Id = c.String(nullable: false, maxLength: 128),
            //            Name = c.String(nullable: false, maxLength: 256),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            //CreateTable(
            //    "dbo.AspNetUserRoles",
            //    c => new
            //        {
            //            UserId = c.String(nullable: false, maxLength: 128),
            //            RoleId = c.String(nullable: false, maxLength: 128),
            //        })
            //    .PrimaryKey(t => new { t.UserId, t.RoleId })
            //    .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
            //    .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
            //    .Index(t => t.UserId)
            //    .Index(t => t.RoleId);
            
            //CreateTable(
            //    "dbo.User",
            //    c => new
            //        {
            //            Id = c.Long(nullable: false, identity: true),
            //            LoginName = c.String(maxLength: 16),
            //            FullName = c.String(maxLength: 128),
            //            PasswordHash = c.String(maxLength: 512),
            //            PasswordExpires = c.Long(nullable: false),
            //            SessionKey = c.String(maxLength: 32),
            //            BiometricTemplate = c.Binary(),
            //            Challenge = c.Binary(),
            //            LoginTime = c.Long(),
            //            SessionEndTime = c.Long(),
            //            Designation = c.String(maxLength: 64),
            //            OfficeTelephoneNumber1 = c.String(maxLength: 32),
            //            OfficeTelephoneNumber2 = c.String(maxLength: 32),
            //            OfficeTelephoneNumber3 = c.String(maxLength: 32),
            //            OfficeFaxNumber = c.String(maxLength: 32),
            //            HouseTelephoneNumber = c.String(maxLength: 32),
            //            MobileTelephoneNumber = c.String(maxLength: 32),
            //            EMailAddress1 = c.String(maxLength: 32),
            //            EMailAddress2 = c.String(maxLength: 32),
            //            EmailAddress3 = c.String(maxLength: 32),
            //            OrganizationalUnitId = c.Long(),
            //            Disabled = c.Boolean(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.AspNetUsers",
            //    c => new
            //        {
            //            Id = c.String(nullable: false, maxLength: 128),
            //            FirstName = c.String(nullable: false, maxLength: 20),
            //            Surname = c.String(nullable: false, maxLength: 20),
            //            Gender = c.String(maxLength: 1),
            //            DateCreated = c.DateTime(nullable: false),
            //            IsFirstTimeLogin = c.Boolean(nullable: false),
            //            Email = c.String(maxLength: 256),
            //            EmailConfirmed = c.Boolean(nullable: false),
            //            PasswordHash = c.String(),
            //            SecurityStamp = c.String(),
            //            PhoneNumber = c.String(),
            //            PhoneNumberConfirmed = c.Boolean(nullable: false),
            //            TwoFactorEnabled = c.Boolean(nullable: false),
            //            LockoutEndDateUtc = c.DateTime(),
            //            LockoutEnabled = c.Boolean(nullable: false),
            //            AccessFailedCount = c.Int(nullable: false),
            //            UserName = c.String(nullable: false, maxLength: 256),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            //CreateTable(
            //    "dbo.AspNetUserClaims",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            UserId = c.String(nullable: false, maxLength: 128),
            //            ClaimType = c.String(),
            //            ClaimValue = c.String(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
            //    .Index(t => t.UserId);
            
            //CreateTable(
            //    "dbo.AspNetUserLogins",
            //    c => new
            //        {
            //            LoginProvider = c.String(nullable: false, maxLength: 128),
            //            ProviderKey = c.String(nullable: false, maxLength: 128),
            //            UserId = c.String(nullable: false, maxLength: 128),
            //        })
            //    .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
            //    .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
            //    .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            //DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            //DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            //DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            //DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            //DropForeignKey("dbo.Loc_DocProfile", "Formno", "dbo.Loc_DocHolderMainProfile");
            //DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            //DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            //DropIndex("dbo.AspNetUsers", "UserNameIndex");
            //DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            //DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            //DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            //DropIndex("dbo.Loc_DocProfile", new[] { "Formno" });
            //DropTable("dbo.AspNetUserLogins");
            //DropTable("dbo.AspNetUserClaims");
            //DropTable("dbo.AspNetUsers");
            //DropTable("dbo.User");
            //DropTable("dbo.AspNetUserRoles");
            //DropTable("dbo.AspNetRoles");
            //DropTable("dbo.Loc_PersoProfile");
            //DropTable("dbo.Loc_PaymentHistory");
            //DropTable("dbo.Loc_EnrolProfile");
            //DropTable("dbo.Loc_DocInventory");
            //DropTable("dbo.Loc_DocProfile");
            //DropTable("dbo.Loc_DocHolderMainProfile");
            //DropTable("dbo.Loc_DocHolderCustomProfile");
            //DropTable("dbo.Loc_DocHolderBioProfile");
            //DropTable("dbo.Loc_Branch");
            //DropTable("dbo.FormCache");
        }
    }
}
