namespace PassFix.Migrations.OldNgricao
{
    using System.Data.Entity.Migrations;

    internal sealed class NgrOldConfig : DbMigrationsConfiguration<PassFix.Data.NgricaoDbContextOld>
    {
        public NgrOldConfig()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Migrations\OldNgricao";
        }

        protected override void Seed(PassFix.Data.NgricaoDbContextOld context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
        }
    }
}
