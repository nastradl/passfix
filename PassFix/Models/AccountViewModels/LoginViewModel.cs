﻿using System;
using System.ComponentModel.DataAnnotations;

namespace PassFix.Models.AccountViewModels
{
    [Serializable]
    public class LoginViewModel
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [StringLength(15)]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
}
