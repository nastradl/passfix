﻿using PassFix.Models.Central;

namespace PassFix.Models.AccountViewModels
{
    public class RegistrationModel
    {
        public RegistrationModel()
        {
            LoginViewModel = new LoginViewModel();
            ForgotPasswordViewModel = new ForgotPasswordViewModel();
            Request = new UserRequest();
        }

        public LoginViewModel LoginViewModel { get; set; }

        public ForgotPasswordViewModel ForgotPasswordViewModel { get; set; }

        public UserRequest Request { get; set; }
    }
}
