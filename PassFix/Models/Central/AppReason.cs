﻿using PassFix.Enums;
using PassFix.Extensions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.Central
{
    [Table("ListAppReasons")]
    public class AppReason
    {
        protected AppReason() { } //For EF

        public AppReason(EnumAppReasons @enum)
        {
            Id = (int)@enum;
            Name = @enum.ToString();
            Description = @enum.GetEnumDescription();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public static implicit operator AppReason(EnumAppReasons @enum) => new AppReason(@enum);

        public static implicit operator EnumAppReasons(AppReason listAppReason) => (EnumAppReasons)listAppReason.Id;
    }
}
