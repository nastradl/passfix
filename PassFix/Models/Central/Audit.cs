﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.Central
{
    public class Audit
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [ForeignKey("Event")]
        public int EventId { get; set; }

        public AuditEvent Event { get; set; }

        [StringLength(150)]
        public string Description { get; set; }

        [DefaultValue("getdate()")]
        public DateTime TimeStamp { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }

        [StringLength(12)]
        public string FormNo { get; set; }

        [StringLength(12)]
        public string DocNo { get; set; }

        public ApplicationUser User { get; set; }
    }
}
