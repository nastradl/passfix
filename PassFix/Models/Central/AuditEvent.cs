﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using PassFix.Enums;
using PassFix.Extensions;

namespace PassFix.Models.Central
{
    [Table("ListAuditEvents")]
    public class AuditEvent
    {
        protected AuditEvent() { } //For EF

        public AuditEvent(EnumAuditEvents @enum)
        {
            Id = (int)@enum;
            Name = @enum.ToString();
            Description = @enum.GetEnumDescription();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public static implicit operator AuditEvent(EnumAuditEvents @enum) => new AuditEvent(@enum);

        public static implicit operator EnumAuditEvents(AuditEvent listAuditEvents) => (EnumAuditEvents)listAuditEvents.Id;
    }
}
