﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.Central
{
    [Table("UserClaimsSubclass")]
    public class ClaimsSubclass
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string SubclassName { get; set; }

        public List<UserClaim> Claims { get; set; }
    }
}
