﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.Central
{
    [Table("EditBioRecordStorage")]
    public class EditBioRecordStore
    {
        [Key]
        public int Id { get; set; }
       
        public string FormNo { get; set; }

        public string UpdateWithFormNo { get; set; }

        public byte[] Signature { get; set; }

        public byte[] Face { get; set; }

        public DateTime DateChanged { get; set; }

        [ForeignKey("User")]
        public string UserId { get; set; }

        public ApplicationUser User { get; set; }
    }
}
