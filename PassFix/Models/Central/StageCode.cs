﻿using PassFix.Enums;
using PassFix.Extensions;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.Central
{
    [Table("ListStageCodes")]
    public class StageCode
    {
        protected StageCode() { } //For EF

        public StageCode(EnumStageCodes @enum)
        {
            Id = (int)@enum;
            Name = @enum.ToString();
            Description = @enum.GetEnumDescription();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        [StringLength(100)]
        public string Description { get; set; }

        public static implicit operator StageCode(EnumStageCodes @enum) => new StageCode(@enum);

        public static implicit operator EnumStageCodes(StageCode listStageCode) => (EnumStageCodes)listStageCode.Id;
    }
}
