﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.Central
{
    [Table("ListUserClaims")]
    public class UserClaim
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Controller { get; set; }

        [Required]
        [StringLength(50)]
        public string Action { get; set; }

        public ClaimsSubclass Subclass { get; set; }
    }
}