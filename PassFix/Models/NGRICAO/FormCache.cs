﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.NGRICAO
{
    [Table("FormCache")]
    public class FormCache
    {
        [Key]
        public long FormNo { get; set; }
    }
}
