﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.NGRICAO
{
    [Table("Loc_Branch")]
    public class LocBranch
    {
        //public LocBranch()
        //{
        //    LocDocInventory = new HashSet<LocDocInventory>();
        //}

        [Key]
        public long Id { get; set; }

        [Required]
        [StringLength(30)]
        public string BranchName { get; set; }

        [Required]
        [StringLength(5)]
        public string BranchCode { get; set; }

        //public ICollection<LocDocInventory> LocDocInventory { get; set; }
    }
}
