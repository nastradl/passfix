﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.NGRICAO
{
    [Table("Loc_DocHolderBioProfile")]
    [Serializable]
    public class LocDocHolderBioProfileOld
    {
        [Key]
        [StringLength(12)]
        public string Formno { get; set; }

        public byte[] Faceimage { get; set; }

        public byte[] Faceimagej2K { get; set; }

        public DateTime? Faceentrytime { get; set; }


        [StringLength(2)]
        public string Finger1Code { get; set; }

        public int? Finger1Reason { get; set; }

        public byte[] Finger1Image { get; set; }

        public byte[] Finger1Template1 { get; set; }

        public byte[] Finger1Template2 { get; set; }


        [StringLength(12)]
        public string Finger2Code { get; set; }

        public int? Finger2Reason { get; set; }

        public byte[] Finger2Image { get; set; }

        public byte[] Finger2Template1 { get; set; }

        public byte[] Finger2Template2 { get; set; }


        [StringLength(12)]
        public string Finger3Code { get; set; }

        public int? Finger3Reason { get; set; }

        public byte[] Finger3Image { get; set; }

        public byte[] Finger3Template1 { get; set; }

        public byte[] Finger3Template2 { get; set; }


        [StringLength(12)]
        public string Finger4Code { get; set; }

        public int? Finger4Reason { get; set; }

        public byte[] Finger4Image { get; set; }

        public byte[] Finger4Template1 { get; set; }

        public byte[] Finger4Template2 { get; set; }


        public DateTime? Fingerentrytime { get; set; }

        public byte[] Signimage { get; set; }

        public DateTime? Signentrytime { get; set; }
    }
}
