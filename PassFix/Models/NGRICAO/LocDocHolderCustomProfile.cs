﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.NGRICAO
{
    [Table("Loc_DocHolderCustomProfile")]
    [Serializable]
    public class LocDocHolderCustomProfile
    {
        [Key]
        [StringLength(12)]
        public string Formno { get; set; }

        [StringLength(30)]
        public string Othername1 { get; set; }

        [StringLength(30)]
        public string Othername2 { get; set; }

        [StringLength(30)]
        public string Othername3 { get; set; }

        [StringLength(30)]
        public string Othername4 { get; set; }

        public int? Dobflag { get; set; }

        [StringLength(25)]
        public string Originstate { get; set; }

        [StringLength(25)]
        public string Addstreet { get; set; }

        [StringLength(25)]
        public string Addtown { get; set; }

        [StringLength(25)]
        public string Addstate { get; set; }

        [StringLength(25)]
        public string Profession { get; set; }

        [StringLength(25)]
        public string Occupation { get; set; }

        public int? Height { get; set; }

        public int? Eyecolor { get; set; }

        public int? Haircolor { get; set; }

        public int? Maritalstatus { get; set; }

        [StringLength(50)]
        public string Birthmark { get; set; }

        [StringLength(25)]
        public string Maidenname { get; set; }

        [StringLength(25)]
        public string Nokname { get; set; }

        [StringLength(50)]
        public string Nokaddress { get; set; }

        [StringLength(30)]
        public string Child1Sname { get; set; }

        [StringLength(30)]
        public string Child1Fname { get; set; }

        public DateTime? Child1Dob { get; set; }

        [StringLength(25)]
        public string Child1Pobt { get; set; }

        [StringLength(25)]
        public string Child1Pobs { get; set; }

        [StringLength(1)]
        public string Child1Sex { get; set; }

        [StringLength(30)]
        public string Child2Sname { get; set; }

        [StringLength(30)]
        public string Child2Fname { get; set; }

        public DateTime? Child2Dob { get; set; }

        [StringLength(25)]
        public string Child2Pobt { get; set; }

        [StringLength(25)]
        public string Child2Pobs { get; set; }

        [StringLength(1)]
        public string Child2Sex { get; set; }

        [StringLength(30)]
        public string Child3Sname { get; set; }

        [StringLength(30)]
        public string Child3Fname { get; set; }

        public DateTime? Child3Dob { get; set; }

        [StringLength(25)]
        public string Child3Pobt { get; set; }

        [StringLength(25)]
        public string Child3Pobs { get; set; }

        [StringLength(1)]
        public string Child3Sex { get; set; }

        [StringLength(30)]
        public string Child4Sname { get; set; }

        [StringLength(30)]
        public string Child4Fname { get; set; }

        public DateTime? Child4Dob { get; set; }

        [StringLength(25)]
        public string Child4Pobt { get; set; }

        [StringLength(25)]
        public string Child4Pobs { get; set; }

        [StringLength(1)]
        public string Child4Sex { get; set; }

        [StringLength(20)]
        public string Title { get; set; }

        [StringLength(20)]
        public string Eyecolorother { get; set; }

        [StringLength(20)]
        public string Haircolorother { get; set; }

        [StringLength(50)]
        public string Addpermanent { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(50)]
        public string Contactphone { get; set; }

        [StringLength(50)]
        public string Mobilephone { get; set; }

        [StringLength(60)]
        public string Gtoname { get; set; }

        [StringLength(50)]
        public string Gtoaddress { get; set; }
    }
}
