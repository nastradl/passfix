﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.NGRICAO
{
    [Table("Loc_DocHolderMainProfile")]
    [Serializable]
    public class LocDocHolderMainProfile
    {
        public LocDocHolderMainProfile()
        {
            LocDocProfile = new HashSet<LocDocProfile>();
        }

        [Key]
        [StringLength(12)]
        public string Formno { get; set; }

        [StringLength(30)]
        public string Surname { get; set; }

        [StringLength(30)]
        public string Firstname { get; set; }

        public DateTime? Birthdate { get; set; }

        [StringLength(25)]
        public string Birthtown { get; set; }

        [StringLength(25)]
        public string Birthstate { get; set; }

        [StringLength(1)]
        public string Sex { get; set; }

        [StringLength(3)]
        public string Nationality { get; set; }

        [StringLength(16)]
        public string Personalno { get; set; }

        public ICollection<LocDocProfile> LocDocProfile { get; set; }
    }
}
