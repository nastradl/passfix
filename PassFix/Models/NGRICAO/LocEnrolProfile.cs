﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.NGRICAO
{
    [Table("Loc_EnrolProfile")]
    [Serializable]
    public class LocEnrolProfile
    {
        [Key]
        [StringLength(12)]
        public string Formno { get; set; }

        public int Appreason { get; set; }

        [Required]
        [StringLength(5)]
        public string Branchcode { get; set; }

        [Required]
        public long Enrollocationid { get; set; }

        [Required]
        public long Layoutid { get; set; }

        [StringLength(12)]
        public string Olddocno { get; set; }

        [StringLength(2)]
        public string Doctype { get; set; }

        [StringLength(15)]
        public string Fileno { get; set; }

        [Required]
        public int Nofinger { get; set; }

        [Required]
        [StringLength(20)]
        public string Enrolby { get; set; }

        [Required]
        public DateTime Enroltime { get; set; }

        [StringLength(20)]
        public string Appby { get; set; }

        public DateTime? Apptime { get; set; }

        [StringLength(1024)]
        public string Remarks { get; set; }

        [Required]
        [StringLength(6)]
        public string Stagecode { get; set; }

        [Required]
        public int Priority { get; set; }

        [StringLength(12)]
        public string Overwriteafisby { get; set; }

        public DateTime? Overwriteafistime { get; set; }

        [StringLength(20)]
        public string Overwritepriorityby { get; set; }

        public DateTime? Overwriteprioritytime { get; set; }

        [StringLength(20)]
        public string Overwriteadminrejectby { get; set; }

        public DateTime? Overwriteadminrejecttime { get; set; }

        [StringLength(20)]
        public string Overwriteotherby { get; set; }

        public DateTime? Overwriteothertime { get; set; }

        [StringLength(20)]
        public string Thirdpartyissueby { get; set; }

        public DateTime? Thirdpartyissuetime { get; set; }

        [StringLength(20)]
        public string Issueby { get; set; }

        public DateTime? Issuetime { get; set; }

        [StringLength(2)]
        public string Docpage { get; set; }
    }
}
