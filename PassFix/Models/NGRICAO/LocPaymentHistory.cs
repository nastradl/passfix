﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.NGRICAO
{
    [Table("Loc_PaymentHistory")]
    [Serializable]
    public class LocPaymentHistory
    {
        [Key]
        [StringLength(12)]
        public string Formno { get; set; }

        [StringLength(20)]
        public string Bankdraftno { get; set; }

        public DateTime? Pymttime { get; set; }

        [StringLength(20)]
        public string Pymtrecvby { get; set; }

        public int? Pymtamt { get; set; }

        [StringLength(30)]
        public string Bankname { get; set; }

        [StringLength(15)]
        public string Receiptno { get; set; }

        [StringLength(50)]
        public string Refno { get; set; }

        [StringLength(50)]
        public string Appid { get; set; }

    }
}
