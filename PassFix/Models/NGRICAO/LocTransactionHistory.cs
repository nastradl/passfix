﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.NGRICAO
{
    [Table("Loc_TransactionHistory")]
    public class LocTransactionHistory
    {
        [Key]
        [StringLength(12)]
        public string Formno { get; set; }

        public DateTime Txntime { get; set; }

        [StringLength(20)]
        public string Userid { get; set; }

        [StringLength(12)]
        public string Docno { get; set; }

        [StringLength(20)]
        public string Appid { get; set; }

        [StringLength(5)]
        public string Branchcode { get; set; }

        [StringLength(16)]
        public string Txncode { get; set; }

        [StringLength(6)]
        public string Stagecode { get; set; }

        [StringLength(12)]
        public string Errorcode { get; set; }

        public long? Locationid { get; set; }
    }
}
