﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace PassFix.Models.NGRICAO
{
    [Table("User")]
    public class User
    {
        [Key]
        public long Id { get; set; }

        [StringLength(16)]
        public string LoginName { get; set; }

        [StringLength(128)]
        public string FullName { get; set; }

        [StringLength(512)]
        public string PasswordHash { get; set; }

        public long PasswordExpires { get; set; }

        [StringLength(32)]
        public string SessionKey { get; set; }

        public byte[] BiometricTemplate { get; set; }

        public byte[] Challenge { get; set; }

        public long? LoginTime { get; set; }

        public long? SessionEndTime { get; set; }

        [StringLength(64)]
        public string Designation { get; set; }

        [StringLength(32)]
        public string OfficeTelephoneNumber1 { get; set; }

        [StringLength(32)]
        public string OfficeTelephoneNumber2 { get; set; }

        [StringLength(32)]
        public string OfficeTelephoneNumber3 { get; set; }

        [StringLength(32)]
        public string OfficeFaxNumber { get; set; }

        [StringLength(32)]
        public string HouseTelephoneNumber { get; set; }

        [StringLength(32)]
        public string MobileTelephoneNumber { get; set; }

        [StringLength(32)]
        public string EMailAddress1 { get; set; }

        [StringLength(32)]
        public string EMailAddress2 { get; set; }

        [StringLength(32)]
        public string EmailAddress3 { get; set; }

        public long? OrganizationalUnitId { get; set; }

        public bool Disabled { get; set; }
    }
}
