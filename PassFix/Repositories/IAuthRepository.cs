﻿using PassFix.Models;
using PassFix.Models.Central;
using PassFix.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PassFix.Repositories
{
    public interface IAuthRepository
    {
        Task<int> GetNumberOfUsers();

        Task<List<State>> GetStates();

        Task<List<Title>> GetTitles();

        Task<List<ApplicationUser>> GetUsers();

        Task<List<UserClaim>> GetClaimsList();

        Task<ViewAudits> GetAuditTrails(int perpage, int page, string orderby, int reverse);

        Task<ViewAudits> SearchAuditTrails(int perpage, string userid, string action, string start, string end, string formno, string docno);

        Task<int> AddClaimsToUser(string userId, string claims, string operatorId);

        Task<int> UpdateUserDetails(ApplicationUser user);

        Task<int> UpdateUserPassword(ApplicationUser user);

        Task<int> RemoveClaimsFromUser(string userId, string claims, string operatorId);

        Task<int> SetUserLockout(string userId, string operatorId, bool lockOut);

        Task<int> AddUserRequest(UserRequest request);

        Task<int> UserLoggedOut(string userId);

        Task<int> UserLoggedIn(string userId);

        Task<int> UserLoginFailed(string userId);

        Task<bool> CheckEmailRegistered(string email);

        Task<List<UserRequest>> GetUserRequests();

        Task<int> ApproveUserRequest(string userId, List<int> requestId, string tempPassword);

        Task<int> RejectUserRequest(string userId, List<int> requestId);

        Task<int> SaveError(Exception ex, string url, string userId);

        Task<UserRequest> GetUserRequest(int req);
    }
}
