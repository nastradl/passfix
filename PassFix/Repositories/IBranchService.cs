﻿using PassFix.Models.NGRICAO;
using PassFix.ViewModels;
using System.Collections.Generic;
using System.Threading.Tasks;
using PassFix.Models.Central;

namespace PassFix.Repositories
{
    public interface IBranchRepository
    {
        Task<List<Branch>> GetBranches();

        Task<List<AppReason>> GetAppReasons();

        Task<Record> SearchEnrolementByFormNo(string formno, string branch, string userId, int eventId);

        Task<Record> SearchEnrolementByDocNo(string docno, string branch, string userId);

        Task<Record> SearchQueryBookletInfo(string docno, string branch, string userId);

        Task<DetailedRecord> SearchEnrolementByFormNoDetailed(string formno, string branch, string userId);

        Task<Record> SearchForApprovalRecord(string formno, string branch, string userId);

        Task<Record> SearchEditBioRecord(string formno, string secondformno, string branch, string userId);

        Task<Record> SearchReverseEditBioRecord(string formno, string branch, string userId);

        Task<Record> SearchFixWrongPassportIssued(string formno, string branch, string userId);

        Task<Record> SearchCreateReissueRecord(string docno, string searchbranchcode, bool getbio, string userId);

        Task<Record> SearchBindReissueRecord(string docno, string formno, string searchbranchcode, bool getbio, string userId);

        Task<Record> SearchManageUser(string username, string branch, string userId);

        Task<Record> SearchModfyEnrolment(string formno, string password, string searchbranchcode, string userId);

        Task<int> UpdateAfisRecord(Record record, string userId);

        Task<Record> UpdateCreateReissueRecord(Record record, string userId);

        Task<Record> UpdateBindReissueRecord(Record record, string userId);

        Task<int> UpdateBackToApproval(Record record, string userId);

        Task<int> UpdateBackToPaymentCheck(Record record, string userId);

        Task<int> UpdateBackToPerso(Record record, string userId);

        Task<int> UpdateFixWrongPassportIssued(Record record, string userId);

        Task<int> UpdateResendJobForPrinting(Record record, string userId);

        Task<int> UpdateIssuePassport(Record record, string userId);

        Task<int> UpdateSendToIssuance(Record record, string userId);

        Task<int> UpdateEnrolmentRecord(DetailedRecord record, string userId);

        Task<int> UpdateOverideBookletSequence(Record record, string userId);

        Task<int> UpdateBookletToPersoQueue(Record record, string userId);

        Task<int> UpdateEditBioRecord(Record record, string userId);

        Task<int> UpdateManageUser(Record record, string userId);

        Task<int> UpdateReverseEditBioRecord(Record record, string userId);

        Task<int> UpdateModifyEnrolmentFile(Record record, string userId);

        Task<List<LocalQuery>> SearchLocalQuery(string docno, string searchbranchcode, string gender, string firstname, string surname, string formno, string getUserId);

        // Mini Queries
        Task<LocPersoProfile> GetPersoProfile(string docNo);
    }
}
