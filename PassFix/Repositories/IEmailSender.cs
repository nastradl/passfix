﻿using System.Threading.Tasks;

namespace PassFix.Repositories
{
    public interface IEmailSender
    {
        Task SendEmailAsync(string email, string subject, string message);
    }
}
