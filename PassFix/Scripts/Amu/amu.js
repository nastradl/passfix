﻿function Amu() {
    SetSemanticUiInputs();

    $(".searchform").keypress(function (e) {
        if (e.keyCode === 13) {
            e.preventDefault();
            return false;
        }
    });

    $(".searchform")
        .form({
            className: { success: "" },
            fields: {
                formno: {
                    identifier: "formno",
                    rules: [
                        {
                            type: "empty",
                            prompt: "Please enter search details"
                        }
                    ]
                },
                docno: {
                    identifier: "docno",
                    rules: [
                        {
                            type: "empty",
                            prompt: "Please enter search details"
                        }
                    ]
                },
                username: {
                    identifier: "username",
                    rules: [
                        {
                            type: "empty",
                            prompt: "Please enter a username"
                        }
                    ]
                },
                password: {
                    identifier: "password",
                    rules: [
                        {
                            type: "empty",
                            prompt: "Please enter a password"
                        }
                    ]
                },
                searchbranchcode: {
                    identifier: "searchbranchcode",
                    rules: [
                        {
                            type: "empty",
                            prompt: "Please select a branch"
                        }
                    ]
                }
            },
            onSuccess: function (event) {
                $(this).find(".submit").addClass("loading");
            }
        })
        .api({
            serializeForm: true,
            method: "POST", 
            onSuccess: function (response) {
                // clear any previous error
                $(this).find(".error .header").text();

                // remove loading
                $(this).find(".submit").removeClass("loading");

                // update page
                $("#searchresults").html(response.page);

                if (response.error) {
                    if (response.badstagecode) {
                        $(".stagecodediv").addClass("redbg");
                    }

                    var errors = [response.error];
                    $(this).form("add errors", errors);

                    // enable anything that requires it
                    if (response.enable) {
                        $(".ui.disabled").removeClass("disabled");
                        OnUpdate();
                    }

                }
                else {
                    $(".stagecodediv").removeClass("redbg");

                    // enable anything that requires it
                    if (response.enable) {
                        $(".ui.disabled").removeClass("disabled");
                        $(".ui.perma").addClass("disabled");
                    }

                    // allow update now
                    OnUpdate();

                    // enable update button
                    $("#searchresults").find(".submit").removeClass("disabled");
                }

                // refresh semantic ui
                SetSemanticUiInputs();
            },
            onFailure: function (response) {
                // remove loading
                $(this).find(".submit").removeClass("loading");

                if (response.success != 0) {
                    showError(null, null, msgType.Error);
                } else {
                    if (response.error) {
                        var errors = [response.error];
                        $(this).form("add errors", errors);
                        ClearForm();
                    }
                }
            }
        });

    $(".searchlocal")
        .form({
            className: { success: "" },
            fields: {
                searchbranchcode: {
                    identifier: "searchbranchcode",
                    rules: [
                        {
                            type: "empty",
                            prompt: "Please select a branch"
                        }
                    ]
                }
            },
            onSuccess: function (event) {
                $(this).find(".submit").addClass("loading");
            }
        })
        .api({
            serializeForm: true,
            method: "POST",
            onSuccess: function (response) {
                // clear any previous error
                $(this).find(".error .header").text();

                // remove loading
                $(this).find(".submit").removeClass("loading");

                var searchResults = $("#searchresults").html(response.page).animate({ scrollTop: $(this).position().top }, 1000);

                // update page
                searchResults.html(response.page);

                // enable anything that requires it
                if (response.enable) {
                    $(".ui.disabled").removeClass("disabled");
                    $(".ui.perma").addClass("disabled");
                }

                // allow update now
                OnUpdate();

                // view details
                $(".viewitem").click(function () {
                    $(".viewtables").removeClass("selected");
                    $(this).addClass("loading").closest(".viewtables").addClass("selected");
                    var formno = $(this).data("formno");

                    $.ajax({
                        type: "POST",
                        url: "/amu/viewLocalResult",
                        data: { formno: formno, branch: $("#searchbranchcode").val() },
                        dataType: "json",
                        statusCode: {
                            403: function() {
                                showError(null, null, msgType.LoggedOut);
                                window.location.href = "/";
                            }
                        },
                        error: function() {
                            showError(null, null, msgType.Error);
                            $(".loading").removeClass("loading");
                        },
                        success: function (result) {
                            $(".loading").removeClass("loading");
                            $("#viewTableDetails").fadeOut(function() {
                                $(this).html(result.page).fadeIn(function() {
                                     $("html, body").animate({ scrollTop: $("footer").position().top }, 1000);
                                });
                                
                                // Show tables
                                $(".tablehead").click(function () {
                                    var chevron = $(this).find(".chevron");
                                    if (chevron.hasClass("down")) {
                                        chevron.toggleClass("up down");
                                        $(this).next("table").fadeIn(function() {
                                            $(this).animate({ scrollTop: $(this).closest(". column").offset().top }, 1000);
                                        });
                                    } else {
                                        chevron.toggleClass("up down");
                                        $(this).next("table").fadeOut();
                                    }
                                });
                            });
                        }
                    });

                });

                if (response.error) {
                    var errors = [response.error];
                    $(this).form("add errors", errors);
                    $("#searchresults").fadeOut();
                }
            },
            onFailure: function (response) {
                // remove loading
                $(this).find(".submit").removeClass("loading");

                if (response.success != 0) {
                    showError(null, null, msgType.Error);
                } else {
                    if (response.error) {
                        var errors = [response.error];
                        $(this).form("add errors", errors);
                        ClearForm();
                    }
                }
            }
        });
}

function OnUpdate() {
    $(".update.form")
        .form({
            className: { success: "" }
        })
        .api({
            serializeForm: true,
            method: "POST",
            onSuccess: function (response) {
                var form = $(this);

                // show success message
                form.addClass("success");

                // stop user from posting again
                $("#updateform").find(".submit").addClass("disabled");

                // clear any previous error
                form.find(".field.error").removeClass("error");
                form.find(".ui.error.message ul").remove();

                // after a few seconds, clear form
                setTimeout(
                    function () {
                        if (response.clearform) {
                            ClearForm();
                        }
                        
                        if (response.page) {
                            $("#searchresults").fadeOut(function() {
                                $(this).html(response.page);
                                if (response.hideonupdate) {
                                    $(".hideonupdate").hide();
                                }
                                $(this).fadeIn();
                            });
                        }

                        form.removeClass("success");
                    },
                    2000);
            },
            onFailure: function (response) {
                if (response.success != 0) {
                    showError(null, null, msgType.Error);
                } else {
                    if (response.error) {
                        if (response.hideform)
                            $("#updateform").fadeOut();

                        var errors = [response.error];
                        $(this).form("add errors", errors);
                    }
                }
            }
        });

    // replace bio revert
    $("#revertchange").click(function() {
        $("#Revert").val("1");
        $(this).addClass("active").closest("form").submit();
        return false;
    });

    // restart any checkbox
    $(".update .ui.checkbox")
        .checkbox().checkbox({
            onChecked: function () {
                $("#Disabled").val(1);
            },
            onUnchecked: function () {
                $("#Disabled").val(0);
            }
        });

    // dropdowns
    $(".ui.dropdown")
        .dropdown();

    // start xml viewer if required
   var xml = $("#Xml").val();

   if (xml) {
       // xml edit
       var docSpec = {
           onchange: function () {
               $("#Xml").val(Xonomy.harvest());
           },
           elements: {
               "PERMISSIONCODE": {
                   collapsed: function () {
                       return true;
                   }
               },
               "BIO": {
                   collapsed: function () {
                       return true;
                   }
               }
           }
       };

        var editor = document.getElementById("xmldocument");
        Xonomy.setMode("laic");
        Xonomy.render(xml, editor, docSpec);
    }
  
}

function SetSemanticUiInputs() {
    // switch between passport search and formno
    $(".ui.searchdoc")
        .checkbox().checkbox({
            onChecked: function () {
                // clear any previous error
                $(".searchform").removeClass("error");
                ClearForm();

                $("#searchByString").text("Search By Passport No")

                $("#searchbyform").transition({
                    animation: "fade right",
                    onComplete: function () {
                        $("#searchbydoc").transition("fade right");
                        $("#FormNo").val("");
                    }
                });
            },
            onUnchecked: function () {
                // clear any previous error
                $(".searchform").removeClass("error");
                ClearForm();

                $("#searchByString").text("Search By Form No");

                $("#searchbydoc").transition({
                    animation: "fade right",
                    onComplete: function () {
                        $("#searchbyform").transition("fade right");
                        $("#DocNo").val("");
                    }
                });
            }
        });

    $(".nofinger").checkbox();

    $("#updateform .dropdown").dropdown({
        onChange: function (value) {
            var target = $(this);
            if (value) {
                target.find(".dropdown.icon").removeClass("dropdown").addClass("delete").on("click", function () {
                    target.dropdown("clear");
                    $(this).removeClass("delete").addClass("dropdown");
                });
            }
        }
    });

    $(".genderdropdown").dropdown({
        onChange: function (value) {
            var target = $(this);
            if (value) {
                target.find(".dropdown.icon").removeClass("dropdown").addClass("delete").on("click", function () {
                    target.dropdown("clear");
                    $(this).removeClass("delete").addClass("dropdown");
                });
            }

            if (value == "M") {
                $('*[data-gender="F"]').addClass("disabled");
                $('*[data-gender="M"]').removeClass("disabled");
            } else {
                $('*[data-gender="M"]').addClass("disabled");
                $('*[data-gender="F"]').removeClass("disabled");
            }
        }
    });

    var calendarOpts = {
        type: "date",
        onHide: function() {
            $("input").blur();
        },
        monthFirst: false
    };

    $("#dobcalendar").calendar(calendarOpts);
    $("#dobcalendar2").calendar(calendarOpts);
}

function ClearForm() {
    // replace branch selected after clear
    var branch = $("#searchbranchcode").val();
    $("#updateform").form("clear").find(".input").addClass("disabled");
    $("#updateform").find(".dropdown").addClass("disabled");
    $(".clearable").text("");
    $(".searchbranchcode").each(function() {
        $(this).val(branch);
    });

    // disable button
    $("#updateform").find(".submit").text("Update");
}