﻿var requestlist = [];

$(document)
    .ready(function () {
        // Set what happens when you press the approval buttons
        $(document).on("click", ".approvebtn", function () {
            $("#reject").val(0);

            // submit as usual
            $(this).closest("form").addClass("loading").submit();
        });

        $(document).on("click", ".rejectbtn", function () {
            $("#reject").val(1);

            // submit as usual
            $(this).closest("form").addClass("loading").submit();
        });

        $(document).on("click", ".approveallbtn", function () {
            $("#reject").val(0);
            var form = $(this).closest("form");

            // check all the boxes
            form.find(".checkbox").each(function () {
                $(this).checkbox("check");
            });

            // then submit
            form.addClass("loading").submit();
        });
    });

function ManageUserRequests() {
    // If slider changed, allow approve
    $(".slider.checkbox").checkbox({
            onChecked: function () {
                $(".approvebtn, .rejectbtn").removeClass("disabled");

                // update list and hidden value
                var checkbox = $(this).closest(".checkbox");
                var id = checkbox.data("id");

                requestlist.push(id);
                $(this).closest(".form").find("#selected").val(requestlist.join(", "));
            },
            onUnchecked: function () {
                // disable button if all are turned off
                if ($(this).closest("table").find(".slider.checkbox.checked").length == 0)
                    $(".approvebtn, .rejectbtn").addClass("disabled");

                // update list and hidden value
                var id = $(this).closest(".checkbox").data("id");

                var index = requestlist.indexOf(id);
                requestlist.splice(index, 1);
                $(this).closest(".form").find("#selected").val(requestlist.join(", "));
            }
        })
        ;
    
    // Submit the approval form
    $("#approveUserForm")
        .form({
            className: { success: "" }
        })
        .api({
            serializeForm: true,
            method: "POST",
            onSuccess: function (response) {
                $(this).addClass("success");
                $(".submit.button").addClass("disabled");

                // Show message for a few seconds then update ui
                setTimeout(
                    function () {
                        $("#workspace").fadeOut(function () {
                            $(this).html(response.page);
                            ManageUserRequests();
                            $(this).fadeIn();
                        });
                    },
                    1000);
            },
            onFailure: function (response) {
                if (response.success != 0)
                    showError(null, null, msgType.Error);
            }
        });
}