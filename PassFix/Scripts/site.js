﻿var msgType = {
    Error: 1,
    LoggedOut: 2
};

$(document)
        .ready(function () {

            // prevent ajax caching
            $.ajaxSetup({ cache: false });

            $.fn.api.settings.successTest = function (response) {
                return response.success == 1;
            }

            // fix menu when passed
            $(".masthead")
                    .visibility({
                        once: false,
                        onBottomPassed: function () {
                            $(".fixed.menu").transition("fade in");
                        },
                        onBottomPassedReverse: function () {
                            $(".fixed.menu").transition("fade out");
                        }
                    })
                ;

            // create sidebar and attach to menu open
            $(".ui.sidebar")
                    .sidebar()
                ;

            // show login
            $(".showlogin").click(function () {
                $(".regdiv").each(function () {
                    $(this).fadeOut();
                });

                $(".regdiv").promise().done(function () {
                    $("#logindiv").fadeIn();
                });
            });

            // show registration
            $(".showreg").click(function () {
                $(".regdiv").each(function () {
                    $(this).fadeOut();
                });

                $(".regdiv").promise().done(function () {
                    $("#requestdiv").fadeIn();
                });
            });

            // show homepage
            $("#bigtitle").click(function () {
                if ($("#defaultdiv").length) {
                    $("#defaultdiv").siblings().each(function() {
                        $(this).fadeOut();
                    });

                    $("#defaultdiv").siblings().promise().done(function() {
                        $("#defaultdiv").fadeIn();
                    });
                } else {
                    window.location.href = "/";
                }
            });

            // reset password
            $(".forgotpw").click(function () {
                $(".regdiv").each(function () {
                    $(this).fadeOut();
                });

                $(".regdiv").promise().done(function () {
                    $("#forgotdiv").fadeIn();
                });
            });

            // initialize accordions and dropdowns
            $(".ui.accordion")
                .accordion();

            $(".ui.dropdown")
                    .dropdown();

            // NOTE: This validation really is not practical for MVC seeing as I can't use my normal model validation.
            // i think it would work great with a javascript framework though.
            // tbh I probably could get this to work if I actually bothered to think about it. Maybe come back to it later? TODO!!

           // login form
           $("#loginfrm")
                .form({
                    className: { success: "" },
                    fields: {
                        email: {
                            identifier: "email",
                            rules: [
                                {
                                    type: "empty",
                                    prompt: "Please enter your e-mail"
                                }
                            ]
                        },
                        password: {
                            identifier: "password",
                            rules: [
                                {
                                    type: "empty",
                                    prompt: "Please enter your password"
                                }
                            ]
                        }
                    }
                })
                .api({
                    serializeForm: true,
                    method: "POST",
                    onSuccess: function (response) {
                        $(this).addClass("success");
                        $(".submit.button").addClass("disabled");

                        setTimeout(
                            function () {
                                // Hide header and show menu
                                $("#logindiv").fadeOut(function () {
                                    $("#mainheader").slideUp(function () {
                                        $("#sideMenu").hide().html(response.menu).fadeIn();
                                        $("#renderBody").html(response.homepage);
                                        $("#sideMenuSlide")
                                            .transition("slide right").css("height", "100%");

                                        // initialize accordions
                                        $(".ui.accordion")
                                                .accordion()
                                            ;
                                    });
                                });
                            }, 2000);
                    },
                    onFailure: function (response) {
                        if (response.success != 0)
                            showError(null, null, msgType.Error);
                    }
                })
                ;

            // forgot password form
           $("#forgotfrm")
               .form({
                   className: { success: "" },
                   fields: {
                       email: {
                           identifier: "email",
                           rules: [
                               {
                                   type: "empty",
                                   prompt: "Please enter your e-mail"
                               }
                           ]
                       }
                   }
               })
               .api({
                   serializeForm: true,
                   method: "POST",
                   onSuccess: function (response) {
                       $(this).addClass("success");
                       $(".submit.button").addClass("disabled");

                       setTimeout(
                           function () {
                               // Hide header and show menu
                               $("#forgotdiv").fadeOut(function () {
                                   $("#logindiv").fadeIn();
                               });
                           }, 2000);
                   },
                   onFailure: function (response) {
                       if (response.success != 0)
                           showError(null, null, msgType.Error);

                       if (response.error)
                           $("#forgotdiv .error.message .header").text(response.error);
                   }
               })
               ;

            // reset password form
           $("#resetpasswordfrm")
               .form({
                   className: { success: "" },
                   fields: {
                       email: {
                           identifier: "email",
                           rules: [
                               {
                                   type: "empty",
                                   prompt: "Please enter your e-mail"
                               }
                           ]
                       },
                       password: {
                           identifier: "password",
                           rules: [
                               {
                                   type: "empty",
                                   prompt: "Enter your new password"
                               },
                               {
                                   type: "minLength[4]",
                                   prompt: "Your password must be at minimum of 4 characters"
                               }
                           ]
                       },
                       confirmpassword: {
                           identifier: "confirmpassword",
                           rules: [
                               {
                                   type: "empty",
                                   prompt: "Your new password and confirmation password must match"
                               }
                           ]
                       }
                   }
               })
               .api({
                   serializeForm: true,
                   method: "POST",
                   onSuccess: function () {
                       $(this).find(".success.message").fadeIn().delay(2000).fadeOut(function() {
                           window.location.href = "/";
                       });
                   },
                   onFailure: function (response) {
                       if (response.success != 0)
                           showError(null, null, msgType.Error);
                   }
               });

            // registration form
            $("#requestfrm")
                .form({
                    className: { success: "" },
                    fields: {
                        emailaddress: {
                            identifier: "emailaddress",
                            rules: [
                                {
                                    type: "empty",
                                    prompt: "Please enter your e-mail"
                                },
                                {
                                    type: "regExp[^[a-zA-Z0-9._]+@irissmart\.com$]",
                                    prompt: "Only Iris email accounts are allowed on this platform"
                                }
                            ]
                        },
                        firstname: {
                            identifier: "firstname",
                            rules: [
                                {
                                    type: "empty",
                                    prompt: "Please enter your firstname"
                                },
                                {
                                    type: "maxLength[20]",
                                    prompt: "Your firstname must be at most 20 characters"
                                }
                            ]
                        },
                        surname: {
                            identifier: "surname",
                            rules: [
                                {
                                    type: "empty",
                                    prompt: "Please enter your surname"
                                },
                                {
                                    type: "maxLength[20]",
                                    prompt: "Your surname must be at most 20 characters"
                                }
                            ]
                        },
                        message: {
                            identifier: "message",
                            rules: [
                                {
                                    type: "maxLength[100]",
                                    prompt: "Your message cannot be more than {ruleValue} characters"
                                }
                            ]
                        }
                    }
                })
                .api({
                    serializeForm: true,
                    method: "POST",
                    onSuccess: function () {
                        $(this).addClass("success");
                        $(".teal.submit.button").addClass("disabled");

                        setTimeout(
                            function () {
                                $("#requestfrm").form("clear");
                                $(".showlogin").click();
                            }, 4000);

                    },
                    onFailure: function (response) {
                        if (response.success == 0) {
                            if (response.error)
                                $("#requestfrm .errormessage").text(response.error);
                            else
                                $("#requestfrm .errormessage").text(JSON.stringify(response.errors));
                        }
                        else
                            showError(null, null, msgType.Error);
                    }
                })
               ;

            // catch all 401 errors
            $(document).ajaxError(function (event, jqxhr) {
                event.preventDefault();
                if (jqxhr.status == 401 || jqxhr.status == 403) {
                    showError(null, null, msgType.LoggedOut);

                    setTimeout(
                        function () {
                        window.location.href = "/";
                    }, 2000);
                }
            });
        })
    ;

function showError(header, message, type) {
    if (header) {
        $(".ui.modal.message .header").text(header);
    }

    if (message) {
        $(".ui.modal.message .content p").text(message);
    }

    if (type == msgType.Error) {
        $(".ui.modal.message .header").text("Error");
        $(".ui.modal.message .content p").text("An error has occurred. It has been logged. If it persists please contact an admin.");
    }

    if (type == msgType.LoggedOut) {
        $(".ui.modal.message .header").text("Authorisation Denied");
        $(".ui.modal.message .content p").text("You have been logged out of the system. Please log in again to continue");
    }

    $("#popupMsg")
            .modal("show")
        ;
}