﻿var selectedBranch;

$(document)
    .ready(function () {
        // on menu item clicked
        $(document).on("click", "a[data-option]", function () {
            $("#sideMenuSlide .item.active").removeClass("active");
            $(this).addClass("active");

            var controller = $(this).data("section");
            var action = $(this).data("option");
            var url = "/" + controller + "/" + action;
            $("#menuloader").addClass("active");

            $.ajax({
                type: "POST",
                url: url,
                dataType: "json",
                error: function () {
                    $(".loader").removeClass("active");
                },
                success: function (result) {
                    $(".loader").removeClass("active");
                    $("#workspace").fadeOut(function() {
                        $(this).html(result.page);

                        // show branch selection if its AMU
                        if (controller.toLowerCase() == "amu")
                            $("#selectBranch").show();
                        else
                            $("#selectBranch").hide();

                        // after html renders, update various required scripts
                        $(".ui.dropdown")
                            .dropdown();

                        $(".ui.checkbox")
                            .checkbox();

                        AuditTrails();

                        ManageUserRequests();

                        ManageMyAccount();

                        Amu();

                        $(this).fadeIn();

                        // Branch selection
                        var code1, code2;
                        $("#branchNameCmb")
                            .dropdown({
                                onChange: function (value, text, $selectedItem) {
                                    $(this).find(".text").css("color", "teal");

                                    // Set partner dropdown
                                    selectedBranch = code1 = $selectedItem.data("ip");
                                    $(".searchbranchcode").each(function () {
                                        $(this).val(code1);
                                    });
                                    if (code2 != code1)
                                        $("#branchCodeCmb").dropdown("set selected", $selectedItem.data("code"));

                                    // clear focus
                                    $("input").blur();
                                }
                            })
                        ;

                        $("#branchCodeCmb")
                            .dropdown({
                                onChange: function(value, text, $selectedItem) {
                                    $(this).find(".text").css("color", "teal");

                                    // Set partner dropdown
                                    selectedBranch = code2 = $selectedItem.data("ip");
                                    $(".searchbranchcode").each(function () {
                                        $(this).val(code2);
                                    });

                                    if (code2 != code1)
                                        $("#branchNameCmb").dropdown("set selected", $selectedItem.data("name"));

                                    // clear focus
                                    $("input").blur();
                                }
                            });

                        if (selectedBranch)
                            $(".searchbranchcode").each(function() {
                                $(this).val(selectedBranch);
                        });
                    });
                    
                    // update url
                    var loc = location.protocol + "//" + location.hostname + (location.port ? ":" + location.port : "");
                    history.replaceState("", "PassFix", loc + "/Tools?section=" + controller + "&option=" + action);
                }
            });
        });

       
        // if page is refreshed
        var controller = getURLParameter("section");
        var action = getURLParameter("option");

        if (controller && action) {
            // close all opened accordions
            $(".accordion .title").removeClass("active");

            // activate the correct menu options
            $("#" + controller).click();

            var item = $('a[data-option="' + action + '"]');
            item.click();
            item.closest(".content").addClass("active").siblings(".title").addClass("active");
        };

        // turn off autocomplete
        $(document).on("focus", ":input", function () {
            $(this).attr("autocomplete", "off");
        });
    });

function getURLParameter(name) {
    return decodeURIComponent(
        (new RegExp("[?|&]" + name + "=" + "([^&;]+?)(&|#|;|$)").exec(location.search) || [null, ""])[1].replace(/\+/g, "%20")) || null;
}
