﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using PassFix.Data;
using PassFix.Enums;
using PassFix.Extensions;
using PassFix.Models;
using PassFix.Models.Central;
using PassFix.Repositories;
using PassFix.ViewModels;
using PassFixData.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace PassFix.Services
{
    public class AuthService : IAuthRepository
    {
        private static readonly AuthDbContext Context = new AuthDbContextFactory().Create();
        private static readonly UserManager<ApplicationUser> UserManager =
            new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(Context));

        public async Task<int> GetNumberOfUsers()
        {
            return await Context.Users.CountAsync();
        }

        public async Task<List<State>> GetStates()
        {
            return await Context.ListStates.ToListAsync();
        }

        public async Task<List<Title>> GetTitles()
        {
            return await Context.ListTitles.ToListAsync();
        }

        public async Task<List<ApplicationUser>> GetUsers()
        {
            return await Context.Users.ToListAsync();
        }

        public async Task<List<ApplicationUser>> GetDisabledUsers()
        {
            return await Context.Users.Where(x => x.LockoutEnabled).ToListAsync();
        }

        public async Task<UserClaim> GetClaim(int id)
        {
            return await Context.ListUserClaims.SingleAsync(x => x.Id == id);
        }

        public async Task<List<UserClaim>> GetClaimsList()
        {
            return await Context.ListUserClaims.ToListAsync();
        }

        public async Task<ClaimsSubclass> GetSubClass(int id)
        {
            return await Context.ListUserClaimsSubclasses.Include("Claims").SingleAsync(x => x.Id == id);
        }

        public async Task<List<ClaimsSubclass>> GetSubClasses()
        {
            return await Context.ListUserClaimsSubclasses.Include("Claims").ToListAsync();
        }

        public async Task<ViewErrors> GetErrorLogs(int perpage, int page, string orderby, int reverse)
        {
            List<ErrorLog> result;

            if (reverse == 1)
            {
                if (orderby == "User")
                    result = await Context.ErrorLogs.Include("ApplicationUser").OrderByDescending(x => x.ApplicationUser.FirstName)
                        .Skip(page * perpage)
                        .Take(perpage).ToListAsync();

                else if (orderby == "Type")
                    result = await Context.ErrorLogs.Include("ApplicationUser").OrderByDescending(x => x.Type)
                        .Skip(page * perpage)
                        .Take(perpage).ToListAsync();
                else 
                    result = await Context.ErrorLogs.Include("ApplicationUser").OrderByDescending(x => x.DateCreated)
                        .Skip(page * perpage)
                        .Take(perpage).ToListAsync();
            }
            else
            {
                if (orderby == "User")
                    result = await Context.ErrorLogs.Include("ApplicationUser").OrderBy(x => x.ApplicationUser.FirstName)
                        .Skip(page * perpage)
                        .Take(perpage).ToListAsync();

                else if (orderby == "Type")
                    result = await Context.ErrorLogs.Include("ApplicationUser").OrderBy(x => x.Type)
                        .Skip(page * perpage)
                        .Take(perpage).ToListAsync();
                else 
                    result = await Context.ErrorLogs.Include("ApplicationUser").OrderBy(x => x.DateCreated)
                        .Skip(page * perpage)
                        .Take(perpage).ToListAsync();
            }

            var total = await Context.ErrorLogs.CountAsync();
            return new ViewErrors
            {
                ErrorLogs = result,
                CurrentPage = 1,
                NumberOfPages = total / perpage,
                NumberPerPage = perpage,
                Total = total
            };
        }

        public async Task<ErrorLog> GetErrorLog(int id)
        {
            return await Context.ErrorLogs.SingleAsync(x => x.Id == id);
        }

        public async Task<ViewAudits> GetAuditTrails(int perpage, int page, string orderby, int reverse)
        {
            List<Audit> result;

            if (reverse == 1)
            {
                if (orderby == "FirstName")
                    result = await Context.Audits.Include("User").Include("Event").OrderByDescending(x => x.User.FirstName)
                        .Skip(page * perpage)
                        .Take(perpage).ToListAsync();

                else if (orderby == "EventName")
                    result = await Context.Audits.Include("User").Include("Event").OrderByDescending(x => x.Event.Name)
                        .Skip(page * perpage)
                        .Take(perpage).ToListAsync();
                else if (orderby == "TimeStamp")
                    result = await Context.Audits.Include("User").Include("Event").OrderByDescending(x => x.TimeStamp)
                        .Skip(page * perpage)
                        .Take(perpage).ToListAsync();
                else
                    result = await Context.Audits.Include("User").Include("Event").OrderByDescending(x => x.Event.Description)
                        .Skip(page * perpage)
                        .Take(perpage).ToListAsync();
            }
            else
            {
                if (orderby == "FirstName")
                    result = await Context.Audits.Include("User").Include("Event").OrderBy(x => x.User.FirstName)
                        .Skip(page * perpage)
                        .Take(perpage).ToListAsync();

                else if (orderby == "EventName")
                    result = await Context.Audits.Include("User").Include("Event").OrderBy(x => x.Event.Name)
                        .Skip(page * perpage)
                        .Take(perpage).ToListAsync();
                else if (orderby == "TimeStamp")
                    result = await Context.Audits.Include("User").Include("Event").OrderBy(x => x.TimeStamp)
                        .Skip(page * perpage)
                        .Take(perpage).ToListAsync();
                else
                    result = await Context.Audits.Include("User").Include("Event").OrderBy(x => x.Event.Description)
                        .Skip(page * perpage)
                        .Take(perpage).ToListAsync();
            }

            var total = await Context.Audits.CountAsync();
            return new ViewAudits
            {
                Audits = result,
                CurrentPage = 1,
                NumberOfPages = total / perpage,
                NumberPerPage = perpage,
                Total = total
            };
        }

        public async Task<ViewAudits> SearchAuditTrails(int perpage, string userid, string action, string start, string end, string formno, string docno)
        {
            var query = Context.Audits.Include("User").Include("Event").AsQueryable();

            if (!string.IsNullOrEmpty(userid))
            {
                query = query.Where(x => x.UserId == userid);
            }

            if (!string.IsNullOrEmpty(action))
            {
                query = query.Where(x => x.Event.Name == action);
            }

            if (!string.IsNullOrEmpty(formno))
            {
                query = query.Where(x => x.FormNo == formno);
            }

            if (!string.IsNullOrEmpty(docno))
            {
                query = query.Where(x => x.DocNo == docno);
            }

            if (!string.IsNullOrEmpty(start))
            {
                var date = DateTime.Parse(start);
                query = query.Where(x => x.TimeStamp >= date);
            }

            if (!string.IsNullOrEmpty(end))
            {
                var date = DateTime.Parse(end).AddDays(1);
                query = query.Where(x => x.TimeStamp < date);
            }

            var total = await query.CountAsync();
            return new ViewAudits
            {
                Audits = await query.OrderByDescending(x => x.TimeStamp).Take(perpage).ToListAsync(),
                CurrentPage = 1,
                NumberOfPages = total / perpage,
                NumberPerPage = perpage,
                Total = total
            };
        }

        public async Task<int> AddClaimsToUser(string userId, string claims, string operatorId)
        {
            var user = await UserManager.FindByIdAsync(userId);
            var claimslist = new List<Claim>();
            var commavalues = new List<string>();

            foreach (string claim in claims.Split(','))
            {
                var type = claim.Split('/')[0].Trim();
                var value = claim.Split('/')[1].Trim();
                commavalues.Add(value);

                claimslist.Add(new Claim(type, value));
            }

            foreach (var claim in claimslist)
                await UserManager.AddClaimAsync(user.Id, claim);
            
            // add audit - divide into parts as max description is 150 chars
            var parts = commavalues.ChunkBy(5);

            foreach (var part in parts)
            {
                Context.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.PrivilegeAssigned,
                    Description = EnumAuditEvents.PrivilegeAssigned.GetEnumDescription() + $" - { string.Join(", ", part)}",
                    TimeStamp = DateTime.Now,
                    UserId = operatorId
                });
            }
            
            return await Context.SaveChangesAsync();
        }

        public async Task<int> RemoveClaimsFromUser(string userId, string claims, string operatorId)
        {
            var user = await UserManager.FindByIdAsync(userId);
            var claimslist = new List<Claim>();
            var commavalues = new List<string>();

            foreach (string claim in claims.Split(','))
            {
                var type = claim.Split('/')[0].Trim();
                var value = claim.Split('/')[1].Trim();
                commavalues.Add(value);

                claimslist.Add(new Claim(type, value));
            }

            foreach (var claim in claimslist)
                await UserManager.RemoveClaimAsync(user.Id, claim);

            // add audit - divide into parts as max description is 150 chars
            var parts = commavalues.ChunkBy(5);

            foreach (var part in parts)
            {
                Context.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.PrivilegeRevoked,
                    Description = EnumAuditEvents.PrivilegeAssigned.GetEnumDescription() + $" - { string.Join(", ", part)}",
                    TimeStamp = DateTime.Now,
                    UserId = operatorId
                });
            }

            return await Context.SaveChangesAsync();
        }

        public async Task<int> AddClaimsToSubclass(int id, string claims, string operatorId)
        {
            var claimslist = new List<UserClaim>();
            var commavalues = new List<string>();

            foreach (string c in claims.Split(','))
            {
                var type = c.Split('/')[0].Trim();
                var value = c.Split('/')[1].Trim();
                commavalues.Add(value);

                claimslist.Add(new UserClaim
                {
                    Controller = type,
                    Action = value
                });
            }
            
            var subclass = await Context.ListUserClaimsSubclasses.SingleAsync(x => x.Id == id);
            foreach (var c in claimslist)
            {
                var action = await Context.ListUserClaims.Include("Subclass").SingleAsync(x => x.Action == c.Action && x.Controller == c.Controller);
                action.Subclass = subclass;
            }

            // add audit - divide into parts as max description is 150 chars
            var parts = commavalues.ChunkBy(5);

            foreach (var part in parts)
            {
                Context.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.PrivilegeAssigned,
                    Description = EnumAuditEvents.PrivilegeAssigned.GetEnumDescription() + $" - { string.Join(", ", part)}",
                    TimeStamp = DateTime.Now,
                    UserId = operatorId
                });
            }

            return await Context.SaveChangesAsync();
        }

        public async Task<int> RemoveClaimsFromSubclass(int id, string claims, string operatorId)
        {
            var claimslist = new List<UserClaim>();
            var commavalues = new List<string>();

            foreach (string c in claims.Split(','))
            {
                var type = c.Split('/')[0].Trim();
                var value = c.Split('/')[1].Trim();
                commavalues.Add(value);

                claimslist.Add(new UserClaim
                {
                    Controller = type,
                    Action = value
                });
            }

            var subclass = await Context.ListUserClaimsSubclasses.SingleAsync(x => x.Id == id);
            for (int i = 0; i < claimslist.Count; i++)
            {
                var removeme =
                    subclass.Claims.FirstOrDefault(x => x.Action == claimslist[i].Action && x.Controller == claimslist[i].Controller);

                if (removeme != null)
                    subclass.Claims.Remove(removeme);
            }
            
            // add audit - divide into parts as max description is 150 chars
            var parts = commavalues.ChunkBy(5);

            foreach (var part in parts)
            {
                Context.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.PrivilegeRevoked,
                    Description = EnumAuditEvents.PrivilegeAssigned.GetEnumDescription() + $" - { string.Join(", ", part)}",
                    TimeStamp = DateTime.Now,
                    UserId = operatorId
                });
            }

            return await Context.SaveChangesAsync();
        }

        public async Task<int> UpdateUserDetails(ApplicationUser user)
        {
            var appuser = await Context.Users.SingleAsync(x => x.Id == user.Id);

            // if its a different username, confirm it is unique
            if (!string.Equals(appuser.UserName, user.UserName, StringComparison.CurrentCultureIgnoreCase))
            {
                var exists = await UserManager.FindByNameAsync(user.UserName.ToLower());

                if (exists != null)
                    return -1;
            }

            appuser.UserName = user.UserName;
            appuser.FirstName = user.FirstName;
            appuser.Surname = user.Surname;
            appuser.PhoneNumber = user.PhoneNumber;

            // add audit
            Context.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.PersonalDetailsUpdated,
                Description = EnumAuditEvents.PersonalDetailsUpdated.GetEnumDescription(),
                TimeStamp = DateTime.Now,
                UserId = user.Id
            });

            await Context.SaveChangesAsync();

            return 1;
        }

        public async Task<int> UpdateUserPassword(ApplicationUser user)
        {
            var appuser = await Context.Users.SingleAsync(x => x.Id == user.Id);
            var result = await UserManager.ChangePasswordAsync(appuser.Id, user.CurrentPassword, user.NewPassword);

            // add audit
            Context.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.PasswordChanged,
                Description = EnumAuditEvents.PasswordChanged.GetEnumDescription(),
                TimeStamp = DateTime.Now,
                UserId = user.Id
            });

            await Context.SaveChangesAsync();

            if (result.Succeeded)
                return 1;
            
            return 0;
        }

        public async Task<int> SetUserLockout(string userId, string operatorId, bool lockout)
        {
            var user = await UserManager.FindByIdAsync(userId);
            user.LockoutEnabled = lockout;

            // add audit
            Context.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.DisableUser,
                Description = EnumAuditEvents.DisableUser.GetEnumDescription(),
                TimeStamp = DateTime.Now,
                UserId = operatorId
            });

            await Context.SaveChangesAsync();

            return 1;
        }

        public async Task<UserRequest> GetUserRequest(int id)
        {
            return await Context.UserRequests.SingleAsync(x => x.Id == id);
        }

        public async Task<List<UserRequest>> GetUserRequests()
        {
            return await Context.UserRequests.Where(x => x.Approved == null && x.IsActive).ToListAsync();
        }

        public async Task<int> UserLoginFailed(string userId)
        {
            Context.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.LoginFailed,
                Description = EnumAuditEvents.LoginFailed.GetEnumDescription(),
                TimeStamp = DateTime.Now,
                UserId = userId
            });

            await Context.SaveChangesAsync();

            return 1;
        }

        public async Task<int> UserLoggedIn(string userId)
        {
            Context.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.LoginSuccess,
                Description = EnumAuditEvents.LoginSuccess.GetEnumDescription(),
                TimeStamp = DateTime.Now,
                UserId = userId
            });

            await Context.SaveChangesAsync();

            return 1;
        }

        public async Task<int> UserLoggedOut(string userId)
        {
            Context.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.LogOut,
                Description = EnumAuditEvents.LogOut.GetEnumDescription(),
                TimeStamp = DateTime.Now,
                UserId = userId
            });

            await Context.SaveChangesAsync();

            return 1;
        }

        public async Task<bool> CheckEmailRegistered(string email)
        {
            // Check if there is a user with this email
            bool exists = await Context.Users.AnyAsync(x => x.Email.ToLower() == email.ToLower());

            if (exists)
                return true;
            
            // Check if there is a request with this email
            return await Context.UserRequests.AnyAsync(x => x.EmailAddress.ToLower() == email.ToLower());
        }

        public async Task<int> AddUserRequest(UserRequest request)
        {
            Context.UserRequests.Add(request);

            // Add audit
            Context.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.RequestAdded,
                Description = EnumAuditEvents.RequestAdded.GetEnumDescription(),
                TimeStamp = DateTime.Now
            });

            await Context.SaveChangesAsync();

            return 1;
        }

        public async Task<int> ApproveUserRequest(string userId, List<int> requestId, string tempPassword)
        {
            foreach (var id in requestId)
            {
                var request = await Context.UserRequests.FirstAsync(x => x.Id == id);

                // create a new user from request
                var user = new ApplicationUser
                {
                    FirstName = request.FirstName,
                    Surname = request.Surname,
                    PhoneNumber = request.PhoneNumber,
                    Email = request.EmailAddress,
                    UserName = request.EmailAddress.Replace("@irissmart.com", "").ToLower(),
                    DateCreated = DateTime.Now,
                    SecurityStamp = Guid.NewGuid().ToString("D")
                };

                if (!Context.Users.Any(u => u.Email == user.Email))
                {
                    // Add audit
                    Context.Audits.Add(new Audit
                    {
                        EventId = (int)EnumAuditEvents.RequestApproved,
                        Description = EnumAuditEvents.RequestApproved.GetEnumDescription(),
                        TimeStamp = DateTime.Now,
                        UserId = userId
                    });

                    var password = new PasswordHasher();
                    var hashed = password.HashPassword(tempPassword);
                    user.PasswordHash = hashed;

                    var userStore = new UserStore<ApplicationUser>(Context);
                    userStore.CreateAsync(user).Wait();

                    // update the request as well
                    request.UserId = user.Id;
                    request.Approved = true;

                    await Context.SaveChangesAsync();
                }
            }

            return 1;
        }

        public async Task<int> RejectUserRequest(string userId, List<int> requestId)
        {
            foreach (var id in requestId)
            {
                var request = await Context.UserRequests.FirstAsync(x => x.Id == id);

                // Add audit
                Context.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.RequestDenied,
                    Description = EnumAuditEvents.RequestApproved.GetEnumDescription(),
                    TimeStamp = DateTime.Now,
                    UserId = userId
                });

                // update the request as well
                request.Approved = false;

                await Context.SaveChangesAsync();
            }

            return 1;
        }

        public async Task<int> SaveError(Exception ex, string url, string userId)
        {
            Context.ErrorLogs.Add(new ErrorLog
            {
                UserId = userId,
                DateCreated = DateTime.Now,
                InnerException = ex.ToString(),
                Message = ex.Message,
                Type = ex.GetType().ToString(),
                Url = url
            });

            return await Context.SaveChangesAsync();
        }
    }
}
