﻿using Newtonsoft.Json;
using PassFix.Data;
using PassFix.Enums;
using PassFix.Extensions;
using PassFix.Helpers;
using PassFix.Models.Central;
using PassFix.Models.NGRICAO;
using PassFix.Repositories;
using PassFix.ViewModels;
using PassFixData.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Xml;
using System.Xml.Linq;

namespace PassFix.Services
{
    public class BranchService: IBranchRepository
    {
        private static readonly AuthDbContext AuthContext = new AuthDbContextFactory().Create();
        private static NgricaoDbContext _context;
        private static NgricaoDbContextOld _contextOld;
        private readonly string _port = ConfigurationManager.AppSettings["port"];

        public async Task<List<Branch>> GetBranches()
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["idocserviceurl"];
                var contracturl = "WebInvestigatorService.svc/getbranches";
                client.BaseAddress = new Uri(serviceurl);
                var response = await client.GetAsync(contracturl);

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<List<Branch>>(response.Content.ReadAsStringAsync().Result);

                }
            }

            return null;
        }

        public async Task<List<ProcessingOffice>> GetPaymentOffices()
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["paymentserviceurl"];
                var contracturl = serviceurl + "/api/payment/getProcessingOffices";
                client.BaseAddress = new Uri(serviceurl);
                var response = await client.GetAsync(contracturl);

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<List<ProcessingOffice>>(response.Content.ReadAsStringAsync().Result);

                }
            }

            return null;
        }

        public async Task<List<ProcessingState>> GetPaymentStates()
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["paymentserviceurl"];
                var contracturl = serviceurl + "/api/payment/getProcessingStates";
                client.BaseAddress = new Uri(serviceurl);
                var response = await client.GetAsync(contracturl);

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<List<ProcessingState>>(response.Content.ReadAsStringAsync().Result);

                }
            }

            return null;
        }

        public async Task<List<AppReason>> GetAppReasons()
        {
            return await AuthContext.ListAppReasons.ToListAsync();
        }

        public async Task<Record> GetPassportFromCentralFormno(string formno, bool withbio, string userId)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["idocserviceurl"];
                var contracturl = $"WebInvestigatorService.svc/getpassportdetails/{formno}/{withbio}";
                client.BaseAddress = new Uri(serviceurl);
                var response = await client.GetAsync(contracturl);

                // add audit
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchCreateReissueRecord,
                    Description = EnumAuditEvents.SearchCreateReissueRecord.GetEnumDescription() + $" - Formno: {formno}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    DocNo = formno
                });

                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        var record = new Record
                        {
                            Passport = JsonConvert.DeserializeObject<LocPassport>(response.Content.ReadAsStringAsync()
                                .Result)
                        };
                        return record;
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }

            return null;
        }

        public async Task<Record> GetPassportFromCentralDocno(string docno, bool withbio, string userId)
        {
            // get details from central
            using (var client = new HttpClient())
            {
                //Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["idocserviceurl"];
                var contracturl = $"WebInvestigatorService.svc/getpassportdetailsdocno/{docno}/{withbio}";
                client.BaseAddress = new Uri(serviceurl);
                var response = await client.GetAsync(contracturl);

                // add audit
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchCreateReissueRecord,
                    Description = EnumAuditEvents.SearchCreateReissueRecord.GetEnumDescription() + $" - Docno: {docno}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    DocNo = docno
                });

                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        var record = new Record
                        {
                            Passport = JsonConvert.DeserializeObject<LocPassport>(response.Content.ReadAsStringAsync()
                                .Result)
                        };
                        return record;
                    }
                    catch (Exception ex)
                    {
                        return null;
                    }
                }
            }

            return null;
        }

        public async Task<Record> SearchEnrolementByFormNo(string formno, string branch, string userId, int eventId)
        {
            string name = branch.Split('|')[1];
            _context = GetConnectionString(branch);

            if (!CheckConnection())
                return new Record
                {
                    Error = "Could not connect to database"
                };

            var enroleProfile = await _context.LocEnrolProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderMainpProfile = await _context.LocDocHolderMainProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderCustomProfile = await _context.LocDocHolderCustomProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var persoProfile = await _context.LocPersoProfile.SingleOrDefaultAsync(x => x.Formno == formno);

            if (enroleProfile == null || docHolderMainpProfile == null || docHolderCustomProfile == null)
            {
                return null;
            }

            // if there is a docprofile, get the passport number
            string passportNo = string.Empty;
            string doctype = string.Empty;
            string docstagecode = string.Empty;
            var docprofiles = await _context.LocDocProfile.Where(x => x.Formno == formno).ToListAsync();

            if (docprofiles != null && docprofiles.Count != 0)
            {
                passportNo = docprofiles[0].Docno;
                doctype = docprofiles[0].Doctype;
                docstagecode = docprofiles[0].Stagecode;
            }

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = eventId,
                Description = ((EnumAuditEvents)eventId).GetEnumDescription() + $" - FormNo: {formno}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = formno
            });

            await AuthContext.SaveChangesAsync();
            
            return new Record
            {
                FirstName = docHolderMainpProfile.Firstname,
                Surname = docHolderMainpProfile.Surname,
                MiddleName = docHolderCustomProfile.Othername1,
                FormNo = enroleProfile.Formno,
                StageCode = enroleProfile.Stagecode,
                BranchCode = branch,
                PassportNo = passportNo,
                PassportType = doctype,
                LocationId = enroleProfile.Enrollocationid,
                PersoLocationId = persoProfile?.Persolocationid,
                DocProfileStageCode = docstagecode,
                Remark = enroleProfile.Remarks,
                PersoProfileStageCode = persoProfile?.Stagecode,
                DocProfiles = docprofiles?.Count > 1 ? docprofiles : null
            };
        }

        public async Task<Record> SearchEnrolementByDocNo(string docno, string branch, string userId)
        {
            string name = branch.Split('|')[1];
            _context = GetConnectionString(branch);

            if (!CheckConnection())
                return new Record
                {
                    Error = "Could not connect to database"
                };

            // Confirm there is a docno
            var docprofile = await _context.LocDocProfile.SingleOrDefaultAsync(x => x.Docno == docno);

            if (docprofile == null)
                return null;

            var formno = docprofile.Formno;
            var enroleProfile = await _context.LocEnrolProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderMainpProfile = await _context.LocDocHolderMainProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderCustomProfile = await _context.LocDocHolderCustomProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var persoProfile = await _context.LocPersoProfile.SingleOrDefaultAsync(x => x.Formno == formno);

            if (enroleProfile == null || docHolderMainpProfile == null || docHolderCustomProfile == null)
            {
                return null;
            }

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.SearchEnrolProfile,
                Description = EnumAuditEvents.SearchEnrolProfile.GetEnumDescription() + $" - FormNo: {formno}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = formno
            });

            await AuthContext.SaveChangesAsync();

            return new Record
            {
                FirstName = docHolderMainpProfile.Firstname,
                Surname = docHolderMainpProfile.Surname,
                MiddleName = docHolderCustomProfile.Othername1,
                FormNo = enroleProfile.Formno,
                StageCode = enroleProfile.Stagecode,
                BranchCode = branch,
                PassportNo = docprofile.Docno,
                PassportType = docprofile.Doctype,
                LocationId = enroleProfile.Enrollocationid,
                PersoLocationId = persoProfile?.Persolocationid,
                DocProfileStageCode = docprofile?.Stagecode,
                PersoProfileStageCode = persoProfile?.Stagecode
            };
        }

        public async Task<Record> SearchQueryBookletInfo(string docno, string branch, string userId)
        {
            string name = branch.Split('|')[1];
            _context = GetConnectionString(branch);

            if (!CheckConnection())
                return new Record
                {
                    Error = "Could not connect to database"
                };

            // Confirm there is a docno
            Record result;

            try
            {
                var docinventory = await _context.LocDocInventory.SingleOrDefaultAsync(x => x.Docno == docno);

                if (docinventory == null)
                    return null;

                result = new Record
                {
                    PassportNo = docinventory.Docno,
                    BoxSn = docinventory.Boxsn,
                    StageCode = docinventory.Stagecode,
                    BranchCode = branch
                };
            }
            catch (Exception)
            {
                // Old systems will throw error because table does not have DocType column
                _contextOld = GetConnectionStringOld(branch);
                var docinventory = await _contextOld.LocDocInventoryOld.SingleOrDefaultAsync(x => x.Docno == docno);

                if (docinventory == null)
                    return null;

                result = new Record
                {
                    PassportNo = docinventory.Docno,
                    BoxSn = docinventory.Boxsn,
                    StageCode = docinventory.Stagecode,
                    BranchCode = branch
                };
            }

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.SearchEnrolProfile,
                Description = EnumAuditEvents.SearchEnrolProfile.GetEnumDescription() + $" - DocNo: {docno}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                DocNo = docno
            });

            await AuthContext.SaveChangesAsync();

            return result;
        }
        
        public async Task<Payment> SearchPaymentQuery(string aid, string refid, string userId)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["paymentserviceurl"];
                var contracturl = serviceurl + $"/api/payment/GetPayment?refid={refid}&aid={aid}";
                client.BaseAddress = new Uri(serviceurl);
                var response = await client.GetAsync(contracturl);

                if (response.IsSuccessStatusCode)
                {
                    // add audit
                    AuthContext.Audits.Add(new Audit
                    {
                        EventId = (int)EnumAuditEvents.SearchPaymentQuery,
                        Description = EnumAuditEvents.SearchPaymentQuery.GetEnumDescription() + $" - Aid: {aid}, RefId: {refid}",
                        TimeStamp = DateTime.Now,
                        UserId = userId
                    });

                    await AuthContext.SaveChangesAsync();

                    return JsonConvert.DeserializeObject<Payment>(response.Content.ReadAsStringAsync().Result);
                }
            }

            return null;
        }

        public async Task<int> UpdatePaymentQuery(Payment payment, string userId)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["paymentserviceurl"];
                var contracturl = serviceurl + (payment.IsAdmin ? "/api/payment/EditPaymentAdmin" : "EditPayment");
                client.BaseAddress = new Uri(serviceurl);

                var response = client.PostAsync(contracturl, new StringContent(
                    new JavaScriptSerializer().Serialize(payment), Encoding.UTF8, "application/json")).Result;

                if (response.IsSuccessStatusCode)
                {
                    if (payment.Firstname?.ToUpper() != payment.OldFirstname?.ToUpper())
                    {
                        AuthContext.Audits.Add(new Audit
                        {
                            EventId = (int)EnumAuditEvents.UpdatePayment,
                            Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Firstname from \"{payment.OldFirstname}\" to \"{payment.Firstname}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });
                    }

                    if (payment.Lastname?.ToUpper() != payment.OldSurname?.ToUpper())
                    {
                        AuthContext.Audits.Add(new Audit
                        {
                            EventId = (int)EnumAuditEvents.UpdatePayment,
                            Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Surname from \"{payment.OldSurname}\" to \"{payment.Lastname}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });
                    }

                    if (payment.Dob != payment.OldDob)
                    {
                        AuthContext.Audits.Add(new Audit
                        {
                            EventId = (int)EnumAuditEvents.UpdatePayment,
                            Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Date of birth from \"{payment.OldDob}\" to \"{payment.Dob}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });
                    }
                    
                    if (payment.Expirydate != payment.OldExpiryDate)
                    {
                        AuthContext.Audits.Add(new Audit
                        {
                            EventId = (int)EnumAuditEvents.UpdatePayment,
                            Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Expiry date from \"{payment.OldExpiryDate}\" to \"{payment.Expirydate}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });
                    }

                    if (payment.OldSex?.ToUpper() != payment.Sex?.ToUpper())
                    {
                        AuthContext.Audits.Add(new Audit
                        {
                            EventId = (int)EnumAuditEvents.UpdatePayment,
                            Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Gender from \"{payment.OldSex}\" to \"{payment.Sex}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });
                    }

                    //if (payment.OldProcessingcountry?.ToUpper() != payment.Processingcountry?.ToUpper())
                    //{
                    //    AuthContext.Audits.Add(new Audit
                    //    {
                    //        EventId = (int)EnumAuditEvents.UpdatePayment,
                    //        Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Processing country from \"{payment.OldProcessingcountry}\" to \"{payment.Processingcountry}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                    //        TimeStamp = DateTime.Now,
                    //        UserId = userId
                    //    });
                    //}

                    if (payment.OldProcessingoffice?.ToUpper() != payment.Processingoffice?.ToUpper())
                    {
                        AuthContext.Audits.Add(new Audit
                        {
                            EventId = (int)EnumAuditEvents.UpdatePayment,
                            Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Processing office from \"{payment.OldProcessingoffice}\" to \"{payment.Processingoffice}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });
                    }

                    if (payment.OldProcessingstate?.ToUpper() != payment.Processingstate?.ToUpper())
                    {
                        AuthContext.Audits.Add(new Audit
                        {
                            EventId = (int)EnumAuditEvents.UpdatePayment,
                            Description = EnumAuditEvents.UpdatePayment.GetEnumDescription() + $" - Changed Processing state from \"{payment.OldProcessingstate}\" to \"{payment.Processingstate}\", Aid: {payment.Aid}, RefId: {payment.RefId}",
                            TimeStamp = DateTime.Now,
                            UserId = userId
                        });
                    }

                    await AuthContext.SaveChangesAsync();

                    return 1;
                }
            }

            return 0;
        }

        public async Task<int> ResetPayment(int aid, int refid, string userId)
        {
            using (var client = new HttpClient())
            {
                //Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["paymentserviceurl"];
                var contracturl = serviceurl + $"/api/payment/ResetPayment?refid={refid}&aid={aid}";
                client.BaseAddress = new Uri(serviceurl);
                var response = await client.GetAsync(contracturl);

                if (response.IsSuccessStatusCode)
                {
                    // add audit
                    AuthContext.Audits.Add(new Audit
                    {
                        EventId = (int)EnumAuditEvents.ResetPayment,
                        Description = EnumAuditEvents.ResetPayment.GetEnumDescription() + $" - Aid: {aid}, RefId: {refid}",
                        TimeStamp = DateTime.Now,
                        UserId = userId
                    });

                    await AuthContext.SaveChangesAsync();

                    return 1;
                }

                return 0;
            }
        }

        public async Task<DetailedRecord> SearchEnrolementByFormNoDetailed(string formno, string branch, string userId)
        {
            string name = branch.Split('|')[1];
            _context = GetConnectionString(branch);

            if (!CheckConnection())
                return new DetailedRecord
                {
                    Error = "Could not connect to database"
                };

            var enroleProfile = await _context.LocEnrolProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderMainpProfile = await _context.LocDocHolderMainProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderCustomProfile = await _context.LocDocHolderCustomProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var paymentHistory = await _context.LocPaymentHistory.SingleOrDefaultAsync(x => x.Formno == formno);

            if (enroleProfile == null || docHolderMainpProfile == null || docHolderCustomProfile == null)
            {
                return null;
            }

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.SearchEnrolProfile,
                Description = EnumAuditEvents.SearchEnrolProfile.GetEnumDescription() + $" - FormNo: {formno}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = formno
            });

            await AuthContext.SaveChangesAsync();

            return new DetailedRecord
            {
                FirstName = docHolderMainpProfile.Firstname,
                Surname = docHolderMainpProfile.Surname,
                MiddleName = docHolderCustomProfile.Othername1,
                FormNo = enroleProfile.Formno,
                StageCode = enroleProfile.Stagecode,
                BranchCode = branch,
                ApplicationId = paymentHistory.Appid,
                ReferenceId = paymentHistory.Refno,
                Appreason = enroleProfile.Appreason,
                DateOfBirth = docHolderMainpProfile.Birthdate,
                DocPage = enroleProfile.Docpage,
                DocType = enroleProfile.Doctype,
                Remarks = enroleProfile.Remarks,
                PersonalNo = docHolderMainpProfile.Personalno,
                Nofinger = enroleProfile.Nofinger == 1,
                Title = docHolderCustomProfile.Title,
                OriginState = docHolderCustomProfile.Originstate,
                BirthState = docHolderMainpProfile.Birthstate,
                Sex = docHolderMainpProfile.Sex,
                BirthTown = docHolderMainpProfile.Birthtown
            };
        }

        public async Task<List<DetailedRecord>> SearchEnrolementForAnalysis(List<string> formnos, string branch, string userId)
        {
            string name = branch.Split('|')[1];
            _context = GetConnectionString(branch);

            if (!CheckConnection())
                return null;

            var result = new List<DetailedRecord>();
            var appreasons = await GetAppReasons();

            foreach (string form in formnos)
            {
                var formno = form;
                Record rec;
                var isPassport = char.IsLetter(formno[0]);

                // pull record from central first. Check if its a passport number or formno by first char
                if (isPassport)
                    rec = await GetPassportFromCentralDocno(formno, true, userId);
                else
                    rec = await GetPassportFromCentralFormno(formno, true, userId);

                LocDocHolderMainProfile docHolderMainpProfile;
                LocDocHolderCustomProfile docHolderCustomProfile;
                LocPaymentHistory paymentHistory;
                LocEnrolProfile enroleProfile;
                bool local = false;

                // If record is EM5000, use central values
                if (rec != null && rec.Passport.LocEnrolProfile.Stagecode == "EM5000")
                {
                    docHolderMainpProfile = rec.Passport.LocDocHolderMainProfile;
                    docHolderCustomProfile = rec.Passport.LocDocHolderCustomProfile;
                    paymentHistory = rec.Passport.LocPaymentHistory;
                    enroleProfile = rec.Passport.LocEnrolProfile;

                    // Set pages
                    enroleProfile.Docpage = string.IsNullOrEmpty(enroleProfile.Docpage) || enroleProfile.Docpage == "0"
                        ? "32 page"
                        : "64 page";
                }
                else
                {
                    // if its passport number, get formno
                    if (isPassport)
                    {
                        var docprofile = await _context.LocDocProfile.FirstOrDefaultAsync(x => x.Docno == form && x.Stagecode != EnumStageCodes.PM2001.ToString());

                        if (docprofile == null)
                            return result;

                        formno = docprofile.Formno;
                    }

                    docHolderMainpProfile = await _context.LocDocHolderMainProfile.SingleOrDefaultAsync(x => x.Formno == formno);
                    docHolderCustomProfile = await _context.LocDocHolderCustomProfile.SingleOrDefaultAsync(x => x.Formno == formno);
                    paymentHistory = await _context.LocPaymentHistory.SingleOrDefaultAsync(x => x.Formno == formno);
                    enroleProfile = await _context.LocEnrolProfile.SingleOrDefaultAsync(x => x.Formno == formno);
                    local = true;
                }

                if (enroleProfile == null || docHolderMainpProfile == null || docHolderCustomProfile == null)
                {
                    return result;
                }
                
                var record = new DetailedRecord
                {
                    FirstName = docHolderMainpProfile.Firstname,
                    Surname = docHolderMainpProfile.Surname,
                    MiddleName = docHolderCustomProfile.Othername1,
                    FormNo = enroleProfile.Formno,
                    StageCode = enroleProfile.Stagecode,
                    BranchCode = branch,
                    ApplicationId = paymentHistory.Appid,
                    ReferenceId = paymentHistory.Refno,
                    Appreason = enroleProfile.Appreason,
                    AppreasonDescr = appreasons.FirstOrDefault(x => x.Id == enroleProfile.Appreason)?.Name,
                    DateOfBirth = docHolderMainpProfile.Birthdate,
                    DocPage = enroleProfile.Docpage,
                    DocType = enroleProfile.Doctype,
                    Remarks = enroleProfile.Remarks,
                    PersonalNo = docHolderMainpProfile.Personalno,
                    Nofinger = enroleProfile.Nofinger == 1,
                    Title = docHolderCustomProfile.Title,
                    OriginState = docHolderCustomProfile.Originstate,
                    BirthState = docHolderMainpProfile.Birthstate,
                    Sex = docHolderMainpProfile.Sex,
                    BirthTown = docHolderMainpProfile.Birthtown,
                    FromBranch = local
                };

                if (local)
                {
                    try
                    {
                        var bioProfile = await _context.LocDocHolderBioProfile.SingleOrDefaultAsync(x => x.Formno == formno);
                        AddBiometrics(record, bioProfile);
                    }
                    catch (Exception)
                    {
                        // Old systems will throw error because table does not have DocType column
                        _contextOld = GetConnectionStringOld(branch);
                        var bioProfile = await _contextOld.LocDocHolderBioProfile.SingleOrDefaultAsync(x => x.Formno == formno);
                        AddBiometrics(record, bioProfile);
                    }
                }
                else
                {
                    AddBiometrics(record, rec.Passport.LocDocHolderBioProfile);
                }
                
                result.Add(record);

                // add audit
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchEnrolProfile,
                    Description = EnumAuditEvents.SearchEnrolProfile.GetEnumDescription() + $" - FormNo: {formno}, Branch: {name}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = formno
                });

                await AuthContext.SaveChangesAsync();
            }

            return result;
        }

        private void AddBiometrics(DetailedRecord record, LocDocHolderBioProfile bioProfile)
        {
            record.Fingerprints = new List<Fingerprint>();

            record.FaceCurrent = bioProfile?.Faceimage;
            record.SignatureCurrent = bioProfile?.Signimage;

            // Fingerprints
            if (bioProfile?.Finger1Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger1Code,
                    Finger = bioProfile.Finger1Image
                });
            }

            if (bioProfile?.Finger2Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger2Code,
                    Finger = bioProfile.Finger2Image
                });
            }

            if (bioProfile?.Finger3Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger3Code,
                    Finger = bioProfile.Finger3Image
                });
            }

            if (bioProfile?.Finger4Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger4Code,
                    Finger = bioProfile.Finger4Image
                });
            }

            if (bioProfile?.Finger5Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger5Code,
                    Finger = bioProfile.Finger5Image
                });
            }

            if (bioProfile?.Finger6Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger6Code,
                    Finger = bioProfile.Finger6Image
                });
            }

            if (bioProfile?.Finger7Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger7Code,
                    Finger = bioProfile.Finger7Image
                });
            }

            if (bioProfile?.Finger8Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger8Code,
                    Finger = bioProfile.Finger8Image
                });
            }

            if (bioProfile?.Finger9Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger9Code,
                    Finger = bioProfile.Finger9Image
                });
            }

            if (bioProfile?.Finger10Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger10Code,
                    Finger = bioProfile.Finger10Image
                });
            }
        }

        private void AddBiometrics(DetailedRecord record, LocDocHolderBioProfileOld bioProfile)
        {
            record.Fingerprints = new List<Fingerprint>();

            record.FaceCurrent = bioProfile?.Faceimage;
            record.SignatureCurrent = bioProfile?.Signimage;

            // Fingerprints
            if (bioProfile?.Finger1Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger1Code,
                    Finger = bioProfile.Finger1Image
                });
            }

            if (bioProfile?.Finger2Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger2Code,
                    Finger = bioProfile.Finger2Image
                });
            }

            if (bioProfile?.Finger3Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger3Code,
                    Finger = bioProfile.Finger3Image
                });
            }

            if (bioProfile?.Finger4Image != null)
            {
                record.Fingerprints.Add(new Fingerprint
                {
                    FingerprintName = bioProfile.Finger4Code,
                    Finger = bioProfile.Finger4Image
                });
            }
        }

        public async Task<Record> SearchForApprovalRecord(string formno, string branch, string userId)
        {
            string name = branch.Split('|')[1];
            _context = GetConnectionString(branch);

            if (!CheckConnection())
                return new Record
                {
                    Error = "Could not connect to database"
                };

            var enroleProfile = await _context.LocEnrolProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderMainpProfile = await _context.LocDocHolderMainProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderCustomProfile = await _context.LocDocHolderCustomProfile.SingleOrDefaultAsync(x => x.Formno == formno);

            if (enroleProfile == null || docHolderMainpProfile == null || docHolderCustomProfile == null)
            {
                return null;
            }

            var result = new Record
            {
                FirstName = docHolderMainpProfile.Firstname,
                Surname = docHolderMainpProfile.Surname,
                MiddleName = docHolderCustomProfile.Othername1,
                FormNo = enroleProfile.Formno,
                StageCode = enroleProfile.Stagecode,
                BranchCode = branch,
                LocationId = enroleProfile.Enrollocationid
            };

            // if docprofile or persoprofile exist, return error
            var hasDocprofile = await _context.LocDocProfile.AnyAsync(x => x.Formno == formno);
            var hasPerso = await _context.LocPersoProfile.AnyAsync(x => x.Formno == formno);

            if (hasDocprofile)
                result.Error = "Cannot alter this record as it has a DocProfile";

            if (hasPerso)
            {
                if (hasDocprofile)
                    result.Error += " and a PersoProfile";
                else
                    result.Error = "Cannot alter this record as it has a PersoProfile";
            }
            
            if (!string.IsNullOrEmpty(result.Error))
                return result;

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.SearchEnrolProfile,
                Description = EnumAuditEvents.SearchEnrolProfile.GetEnumDescription() + $" - FormNo: {formno}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = formno
            });

            await AuthContext.SaveChangesAsync();

            return result;
        }

        public async Task<Record> SearchEditBioRecord(string formno, string secondformno, string branch, string userId)
        {
            string name = branch.Split('|')[1];
            _context = GetConnectionString(branch);

            if (!CheckConnection())
                return new Record
                {
                    Error = "Could not connect to database"
                };

            var bioProfile1 = await _context.LocDocHolderBioProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var bioProfile2 = await _context.LocDocHolderBioProfile.SingleOrDefaultAsync(x => x.Formno == secondformno);
            
            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.SearchEditBioRecord,
                Description = EnumAuditEvents.SearchEditBioRecord.GetEnumDescription() + $" - FormNoOld: {formno}, FormNoNew: {secondformno}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = formno
            });

            await AuthContext.SaveChangesAsync();

            return new Record
            {
                FaceCurrent = bioProfile1?.Faceimage,
                FaceReplace = bioProfile2?.Faceimage,
                SignatureCurrent = bioProfile1?.Signimage,
                SignatureReplace = bioProfile2?.Signimage,
                ShowRevert = false
            };
        }

        public async Task<Record> SearchReverseEditBioRecord(string formno, string branch, string userId)
        {
            string name = branch.Split('|')[1];
            _context = GetConnectionString(branch);

            if (!CheckConnection())
                return new Record
                {
                    Error = "Could not connect to database"
                };

            var bioProfile2 = await _context.LocDocHolderBioProfile.SingleOrDefaultAsync(x => x.Formno == formno);

            // check if any record to revert to
            var revert = await AuthContext.EditBioRecordStorage.OrderByDescending(x => x.DateChanged).FirstOrDefaultAsync(x => x.FormNo == formno);

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.SearchReverseEditBioRecord,
                Description = EnumAuditEvents.SearchReverseEditBioRecord.GetEnumDescription() + $" - FormNoOld: {formno}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = formno
            });

            await AuthContext.SaveChangesAsync();

            if (revert == null)
                return null;

            var same = bioProfile2.Faceimage.SequenceEqual(revert.Face) && bioProfile2.Signimage.SequenceEqual(revert.Signature);
            if (same)
                return null;

            return new Record
            {
                FaceCurrent = revert.Face,
                FaceReplace = bioProfile2?.Faceimage,
                SignatureCurrent = revert.Signature,
                SignatureReplace = bioProfile2?.Signimage,
                ShowRevert = true
            };
        }

        public async Task<Record> SearchManageUser(string username, string branch, string userId)
        {
            string name = branch.Split('|')[1];
            _context = GetConnectionString(branch);

            if (!CheckConnection())
                return new Record
                {
                    Error = "Could not connect to database"
                };

            var user = await _context.User.SingleOrDefaultAsync(x => x.LoginName == username);

            if (user == null)
            {
                return null;
            }

            var result = new Record
            {
                LocUser = user,
                BranchCode = branch
            };

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.SearchBranchUser,
                Description = EnumAuditEvents.SearchBranchUser.GetEnumDescription() + $" - Login Name: {username}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId
            });

            await AuthContext.SaveChangesAsync();

            return result;
        }

        public async Task<Record> SearchModfyEnrolment(string formno, string password, string searchbranchcode, string userId)
        {
            string ip = searchbranchcode.Split('|')[0];
            string name = searchbranchcode.Split('|')[1];
            string path = $"\\\\{ip}\\c$\\inetpub\\wwwroot\\web\\Enrolment\\tempFile";

            if (!CheckConnection())
                return new Record
                {
                    Error = "Could not connect to database"
                };

            // Get file from server
            using (new NetworkConnection(path, new NetworkCredential("administrator", password)))
            {
                var dInfo = new DirectoryInfo(path);
                var dSecurity = dInfo.GetAccessControl();
                dSecurity.AddAccessRule(new FileSystemAccessRule(
                    new SecurityIdentifier(WellKnownSidType.WorldSid, null),
                    FileSystemRights.FullControl,
                    InheritanceFlags.ObjectInherit | InheritanceFlags.ContainerInherit,
                    PropagationFlags.NoPropagateInherit, AccessControlType.Allow)
                    );
                dInfo.SetAccessControl(dSecurity);

                // create folder of edited records if it doesn't exist
                Directory.CreateDirectory(Path.Combine(path, "Original Records"));

                var file = File.ReadAllBytes(Path.Combine(path, formno + ".xml"));

                // convert to xml
                var doc = new XmlDocument();
                string xml = Encoding.UTF8.GetString(file);

                // sometimes the file has a useless char at start that causes error
                string byteOrderMarkUtf8 = Encoding.UTF8.GetString(Encoding.UTF8.GetPreamble());
                if (xml.StartsWith(byteOrderMarkUtf8))
                {
                    xml = xml.Remove(0, byteOrderMarkUtf8.Length);
                }

                doc.LoadXml(xml);

                var record = new Record
                {
                    BranchCode = searchbranchcode,
                    FormNo = formno,
                    Xml = doc.OuterXml,
                    Password = password,
                    UpdateUrl = "UpdateModifyEnrolmentFile"
                };

                // add audit
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchXmlRecord,
                    Description = EnumAuditEvents.SearchXmlRecord.GetEnumDescription() + $" - Formno: {formno}, Branch: {name}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = formno
                });

                await AuthContext.SaveChangesAsync();

                return record;
            }
        }

        public async Task<Record> SearchCreateReissueRecord(string docno, string branchcode, bool getbio, string userId)
        {
            return await GetPassportFromCentralDocno(docno, getbio, userId);
        }

        public async Task<Record> SearchBindReissueRecord(string docno, string formno, string branchcode, bool getbio, string userId)
        {
            // get details from central
            using (var client = new HttpClient())
            {
                //Passing service base url  
                var serviceurl = ConfigurationManager.AppSettings["idocserviceurl"];
                var contracturl = $"WebInvestigatorService.svc/getpassportdetailsdocno/{docno}/{getbio}";
                client.BaseAddress = new Uri(serviceurl);
                var response = await client.GetAsync(contracturl);

                // add audit
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.SearchBindReissueRecord,
                    Description = EnumAuditEvents.SearchBindReissueRecord.GetEnumDescription() + $" - DocNo: {docno}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = formno,
                    DocNo = docno
                });

                if (response.IsSuccessStatusCode)
                {
                    try
                    {
                        var record = new Record
                        {
                            Passport = JsonConvert.DeserializeObject<LocPassport>(response.Content.ReadAsStringAsync()
                                .Result)
                        };

                        // get new biometrics
                        var bioProfile = await _context.LocDocHolderBioProfile.SingleOrDefaultAsync(x => x.Formno == formno);

                        if (bioProfile == null)
                            return new Record
                            {
                                Error = "Could not find a record matching form number entered"
                            };

                        record.FaceCurrent = bioProfile.Faceimage;
                        record.SignatureCurrent = bioProfile.Signimage;

                        return record;
                    }
                    catch (Exception ex)
                    {
                        return new Record
                        {
                            Error = "Could not find a record matching passport number entered"
                        };
                    }
                }
            }

            return null;
        }

        public async Task<Record> SearchFixWrongPassportIssued(string formno, string branch, string userId)
        {
            string name = branch.Split('|')[1];
            _context = GetConnectionString(branch);

            if (!CheckConnection())
                return new Record
                {
                    Error = "Could not connect to database"
                };

            var enroleProfile = await _context.LocEnrolProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderMainpProfile = await _context.LocDocHolderMainProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var docHolderCustomProfile = await _context.LocDocHolderCustomProfile.SingleOrDefaultAsync(x => x.Formno == formno);
            var persoProfile = await _context.LocPersoProfile.SingleOrDefaultAsync(x => x.Formno == formno);

            if (enroleProfile == null || docHolderMainpProfile == null || docHolderCustomProfile == null)
            {
                return null;
            }

            // if there is a docprofile, get the passport number
            string doctype = string.Empty;
            string docstagecode = string.Empty;
            var docprofiles = await _context.LocDocProfile.Where(x => x.Formno == formno).ToListAsync();

            // There must be 2 docprofiles
            if (docprofiles.Count != 2)
            {
                return new Record
                {
                    Error = "Only one docprofile found. This process requires at least 2."
                };
            }

            string stagecode1 = "";
            string stagecode2 = "";
            string booklet1 = "";
            string booklet2 = "";

            foreach (var profile in docprofiles)
            {
                // check if this passport has a perso profile
                var perso = await _context.LocPersoProfile.FirstOrDefaultAsync(y => y.Docno == profile.Docno);

                if (perso == null)
                {
                    booklet2 = profile.Docno;
                    stagecode2 = profile.Stagecode;
                }
                else
                {
                    stagecode1 = profile.Stagecode;
                    booklet1 = profile.Docno;
                }
            }

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.SearchFixWrongPassportIssued,
                Description = EnumAuditEvents.SearchFixWrongPassportIssued.GetEnumDescription() + $" - FormNo: {formno}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = formno
            });

            await AuthContext.SaveChangesAsync();

            return new Record
            {
                FirstName = docHolderMainpProfile.Firstname,
                Surname = docHolderMainpProfile.Surname,
                MiddleName = docHolderCustomProfile.Othername1,
                FormNo = enroleProfile.Formno,
                StageCode = enroleProfile.Stagecode,
                BranchCode = branch,
                PassportNo = booklet2,
                PassportType = doctype,
                DocNoTwo = booklet1,
                DocTwoStageCode = stagecode1,
                DocStageCode2 = stagecode2,
                PersoLocationId = persoProfile.Persolocationid,
                DocProfileStageCode = docstagecode,
                PersoProfileStageCode = persoProfile?.Stagecode,
                DocProfiles = docprofiles?.Count > 1 ? docprofiles : null
            };
        }

        public async Task<int> UpdateAfisRecord(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            var enroleProfile = await _context.LocEnrolProfile.SingleAsync(x => x.Formno == record.FormNo);
            if (enroleProfile.Stagecode == "EM2001")
            {
                enroleProfile.Stagecode = "EM1500";
                enroleProfile.Remarks = string.Empty;
            }
               

            var result = await _context.SaveChangesAsync();

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateAfisRecord,
                Description = EnumAuditEvents.UpdateAfisRecord.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            await AuthContext.SaveChangesAsync();

            return result;
        }

        public async Task<int> UpdateResetStagecode(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            var enroleProfile = await _context.LocEnrolProfile.SingleAsync(x => x.Formno == record.FormNo);
            if (enroleProfile.Stagecode == "EM0801")
            {
                enroleProfile.Stagecode = "EM1000";
            }
            
            var result = await _context.SaveChangesAsync();

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateResetStagecode,
                Description = EnumAuditEvents.UpdateResetStagecode.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            await AuthContext.SaveChangesAsync();

            return result;
        }

        public async Task<int> UpdateManuallyFail(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            var enroleProfile = await _context.LocEnrolProfile.SingleAsync(x => x.Formno == record.FormNo);
            if (enroleProfile.Stagecode == "EM2000" || enroleProfile.Stagecode == "EM4000")
            {
                enroleProfile.Stagecode = "EM2001";
                enroleProfile.Remarks = record.Remark;
            }

            var result = await _context.SaveChangesAsync();

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateManuallyFail,
                Description = EnumAuditEvents.UpdateManuallyFail.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            await AuthContext.SaveChangesAsync();

            return result;
        }

        public async Task<int> UpdateBackToApproval(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            var enroleProfile = await _context.LocEnrolProfile.SingleAsync(x => x.Formno == record.FormNo);

            // change the stage code
            if (enroleProfile.Stagecode == "EM4000")
                enroleProfile.Stagecode = "EM2000";

            // also set enrol location id
            enroleProfile.Enrollocationid = record.LocationId;

            var result = await _context.SaveChangesAsync();

            // add audit for enrol change
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateBackToApproval,
                Description = EnumAuditEvents.UpdateBackToApproval.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });
            
            await AuthContext.SaveChangesAsync();

            return result;
        }

        public async Task<int> UpdateBackToPaymentCheck(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            var enroleProfile = await _context.LocEnrolProfile.SingleAsync(x => x.Formno == record.FormNo);

            // change the stage code
            if (enroleProfile.Stagecode == "EM0801")
                enroleProfile.Stagecode = "EM0800";
            
            var result = await _context.SaveChangesAsync();

            // add audit for enrol change
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateBackToPaymentCheck,
                Description = EnumAuditEvents.UpdateBackToPaymentCheck.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            await AuthContext.SaveChangesAsync();

            return result;
        }

        public async Task<int> UpdateBackToPerso(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            var docProfile = await _context.LocDocProfile.SingleAsync(x => x.Formno == record.FormNo);
            var persoProfile = await _context.LocPersoProfile.SingleAsync(x => x.Formno == record.FormNo);

            // change the stage code
            if (docProfile.Stagecode == "PM2000")
                docProfile.Stagecode = "PM2001";

            if (persoProfile.Stagecode == "PM2000")
                persoProfile.Stagecode = "PM2001";

            persoProfile.Persolocationid = record.PersoLocationId;

            await _context.SaveChangesAsync();

            // add audit for enrol change
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateBackToPerso,
                Description = EnumAuditEvents.UpdateBackToPerso.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            await AuthContext.SaveChangesAsync();

            return 1;
        }

        public async Task<int> UpdateFixWrongPassportIssued(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            var docProfiles = await _context.LocDocProfile.Where(x => x.Formno == record.FormNo).ToListAsync();
            var enroleProfile = await _context.LocEnrolProfile.SingleOrDefaultAsync(x => x.Formno == record.FormNo);

            // There must be 2 docprofiles
            if (docProfiles.Count != 2)
            {
                return 3;
            }

            string booklet1 = "";
            string booklet2 = "";

            // change the stage code
            foreach (var profile in docProfiles)
            {
                // check if this passport has a perso profile
                var perso = await _context.LocPersoProfile.FirstOrDefaultAsync(y => y.Docno == profile.Docno);

                if (perso == null)
                {
                    profile.Stagecode = "PM2001";
                    booklet2 = profile.Docno;
                }
                else
                {
                    profile.Stagecode = "EM4200";
                    booklet1 = profile.Docno;
                }
            }

            enroleProfile.Stagecode = "EM4200";

            await _context.SaveChangesAsync();

            // add audit for enrol change
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateFixWrongPassportIssued,
                Description = EnumAuditEvents.UpdateFixWrongPassportIssued.GetEnumDescription() + $" - Booklet1: {booklet1} to EM4200, Booklet2: {booklet2} to PM2001. Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            await AuthContext.SaveChangesAsync();

            return 1;
        }   

        public async Task<int> UpdateResendJobForPrinting(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            var docProfile = await _context.LocDocProfile.SingleAsync(x => x.Formno == record.FormNo);
            var persoProfile = await _context.LocPersoProfile.SingleAsync(x => x.Formno == record.FormNo);

            // change the stage code
            if (docProfile.Stagecode == "PM1000")
                docProfile.Stagecode = "PM0500";

            if (persoProfile.Stagecode == "PM1000")
                persoProfile.Stagecode = "PM0500";

            await _context.SaveChangesAsync();

            // add audit for enrol change
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateResendJobForPrinting,
                Description = EnumAuditEvents.UpdateResendJobForPrinting.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            await AuthContext.SaveChangesAsync();

            return 1;
        }

        public async Task<int> UpdateIssuePassport(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            var docProfile = await _context.LocDocProfile.SingleAsync(x => x.Formno == record.FormNo);
            var persoProfile = await _context.LocPersoProfile.SingleAsync(x => x.Formno == record.FormNo);
            var enrolProfile = await _context.LocEnrolProfile.SingleAsync(x => x.Formno == record.FormNo);

            // change the stage code
            if (persoProfile.Stagecode == "PM2000")
            {
                if (enrolProfile.Stagecode == "EM4000")
                    enrolProfile.Stagecode = "EM4200";

                if (docProfile.Stagecode == "PM2000")
                    docProfile.Stagecode = "EM4200";
            }
           
            await _context.SaveChangesAsync();

            // add audit for enrol change
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateResendJobForPrinting,
                Description = EnumAuditEvents.UpdateResendJobForPrinting.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            await AuthContext.SaveChangesAsync();

            return 1;
        }

        public async Task<int> UpdateEditBioRecord(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            var bioProfile1 = await _context.LocDocHolderBioProfile.SingleOrDefaultAsync(x => x.Formno == record.FormNo);
            var bioProfile2 = await _context.LocDocHolderBioProfile.SingleOrDefaultAsync(x => x.Formno == record.SecondFormNo);

            // save the current one
            AuthContext.EditBioRecordStorage.Add(new EditBioRecordStore
            {
                FormNo = record.FormNo,
                UpdateWithFormNo = record.SecondFormNo,
                Face = bioProfile1.Faceimage,
                Signature = bioProfile1.Signimage,
                DateChanged = DateTime.Now,
                UserId = userId
            });

            await AuthContext.SaveChangesAsync();

            // overwrite the images
            bioProfile1.Faceimage = bioProfile2.Faceimage;
            bioProfile1.Signimage = bioProfile2.Signimage;

            await _context.SaveChangesAsync();

            // add audit for enrol change
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateEditBioRecord,
                Description = EnumAuditEvents.UpdateEditBioRecord.GetEnumDescription() + $" - FormNoOld: {record.FormNo}, FormNoNew: {record.SecondFormNo}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            await AuthContext.SaveChangesAsync();

            return 1;
        }

        public async Task<int> UpdateManageUser(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            var user = await _context.User.SingleOrDefaultAsync(x => x.LoginName.ToLower() == record.Username);

            string disabled = string.Empty;
            bool isdisabled = record.Disabled == 1;
            if (user.Disabled != isdisabled)
                disabled = "Disabled: " + isdisabled;

            string extendby = string.Empty;
            if (record.ExtendBy != 0)
                extendby = record.ExtendBy + " months";

            user.Disabled = isdisabled;
            user.PasswordExpires = record.GetNewUserPasswordExpiry(user.PasswordExpires, record.ExtendBy);

            await _context.SaveChangesAsync();

            // add audit for enrol change
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateBranchUser,
                Description = EnumAuditEvents.UpdateBranchUser.GetEnumDescription() + $" - Branch: {name}, {disabled} {extendby}",
                TimeStamp = DateTime.Now,
                UserId = userId
            });

            await AuthContext.SaveChangesAsync();

            return 1;
        }

        public async Task<int> UpdateModifyEnrolmentFile(Record record, string userId)
        {
            _context = GetConnectionString(record.BranchCode);

            // save xml back to server
            string ip = record.BranchCode.Split('|')[0];
            string name = record.BranchCode.Split('|')[1];
            string path = $"\\\\{ip}\\c$\\inetpub\\wwwroot\\web\\Enrolment\\tempFile";

            using (new NetworkConnection(path, new NetworkCredential("administrator", record.Password)))
            {
                string backupFolder = Path.Combine(path, "Original Records");
                string current = Path.Combine(path, record.FormNo + ".xml");
                string backup = Path.Combine(backupFolder, record.FormNo + "_" + DateTime.Now.ToString("yyyyMMddhhmmss") + ".xml");

                // Save backup of current file
                File.Copy(current, backup);

                // Overwrite current
                var xml = new XmlDocument();

                // remove space:"preserve" tag
                var doc = XDocument.Parse(record.Xml); 

                bool PreserveAttrFunc(XAttribute atr) => atr.Name.LocalName == "space" && atr.Value == "preserve";
                var descendants = doc.Descendants().Where(kp => kp.HasAttributes && kp.Attributes().Any(PreserveAttrFunc));

                foreach (var descendant in descendants)
                {
                    descendant.Attributes().FirstOrDefault(PreserveAttrFunc)?.Remove();
                }

                doc.AddFirst(xml.CreateXmlDeclaration("1.0", "UTF-8", null));
                doc.Save(current);
            }

            // add audit for change
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateXmlFile,
                Description = EnumAuditEvents.UpdateXmlFile.GetEnumDescription() + $" - Branch: {name}, Formno: {record.FormNo}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            await AuthContext.SaveChangesAsync();

            return 1;
        }

        public async Task<int> UpdateReverseEditBioRecord(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            var bioProfile = await _context.LocDocHolderBioProfile.SingleOrDefaultAsync(x => x.Formno == record.FormNo);

            // get the previous one
            var previous = await AuthContext.EditBioRecordStorage.FirstOrDefaultAsync(x => x.FormNo == record.FormNo);

            if (previous == null)
                return 0;

            // overwrite image with old
            bioProfile.Faceimage = previous.Face;
            bioProfile.Signimage = previous.Signature;

            await _context.SaveChangesAsync();

            // add audit for enrol change
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateEditBioRecordRevert,
                Description = EnumAuditEvents.UpdateEditBioRecordRevert.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            await AuthContext.SaveChangesAsync();

            return 1;
        }

        public async Task<int> UpdateSendToIssuance(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            var docProfile = await _context.LocDocProfile.SingleAsync(x => x.Formno == record.FormNo);
            var persoProfile = await _context.LocPersoProfile.SingleAsync(x => x.Formno == record.FormNo);

            // change the stage code
            if (persoProfile.Stagecode == "PM2001" || persoProfile.Stagecode == "PM1000")
            {
                persoProfile.Stagecode = "PM2000";
            }

            if (docProfile.Stagecode == "PM2001" || docProfile.Stagecode == "PM1000")
            {
                docProfile.Stagecode = "PM2000";
            }

            await _context.SaveChangesAsync();

            // add audit for enrol change
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateSendToIssuance,
                Description = EnumAuditEvents.UpdateSendToIssuance.GetEnumDescription() + $" - FormNo: {record.FormNo}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            await AuthContext.SaveChangesAsync();

            return 1;
        }

        public async Task<int> UpdateOverideBookletSequence(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];

            try
            {
                _context = GetConnectionString(record.BranchCode);
                var docinventory = await _context.LocDocInventory.SingleOrDefaultAsync(x => x.Docno == record.PassportNo);

                if (docinventory.Stagecode == "IM2000")
                    docinventory.Stagecode = "IM3000";

                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                // Old systems will throw error because table does not have DocType column
                _contextOld = GetConnectionStringOld(record.BranchCode);
                var docinventory = await _contextOld.LocDocInventoryOld.SingleOrDefaultAsync(x => x.Docno == record.PassportNo);

                if (docinventory.Stagecode == "IM2000")
                    docinventory.Stagecode = "IM3000";

                await _context.SaveChangesAsync();
            }

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateOverideBookletSequence,
                Description = EnumAuditEvents.UpdateOverideBookletSequence.GetEnumDescription() + $" - DocNo: {record.PassportNo}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                DocNo = record.DocNo
            });

            await AuthContext.SaveChangesAsync();

            return 1;
        }

        public async Task<Record> UpdateCreateReissueRecord(Record rec, string userId)
        {
            string name = rec.BranchCode.Split('|')[1];
            _context = GetConnectionString(rec.BranchCode);
            List<Branch> branches;

            if (!bool.Parse(ConfigurationManager.AppSettings["showFullBranchList"]))
                branches = Helper.GetTempBranchList();
            else
            {
                // get branch info
                branches = await GetBranches();
            }

            var branch = branches.SingleOrDefault(x => x.BranchName == name);

            if (branch == null)
                return null;

            // get record
            var record = await SearchCreateReissueRecord(rec.DocNo, rec.BranchCode, true, userId);

            // Create new tables
            var formno = await _context.FormCache.OrderBy(x => x.FormNo).FirstOrDefaultAsync();
            _context.FormCache.Remove(formno);
            await _context.SaveChangesAsync();

            var newformno = branch.BranchCode + formno.FormNo.ToString("000000000");

            var newenrolprofile = Helper.DeepCopy(record.Passport.LocEnrolProfile);
            newenrolprofile.Formno = newformno;
            newenrolprofile.Docpage = rec.DocPages;
            newenrolprofile.Appreason = rec.AppReason;
            _context.LocEnrolProfile.Add(newenrolprofile);

            var newmainprofile = Helper.DeepCopy(record.Passport.LocDocHolderMainProfile);
            newmainprofile.Formno = newformno;
            _context.LocDocHolderMainProfile.Add(newmainprofile);

            var newcustomprofile = Helper.DeepCopy(record.Passport.LocDocHolderCustomProfile);
            newcustomprofile.Formno = newformno;
            _context.LocDocHolderCustomProfile.Add(newcustomprofile);

            var newbioprofile = Helper.DeepCopy(record.Passport.LocDocHolderBioProfile);
            newbioprofile.Formno = newformno;
            _context.LocDocHolderBioProfile.Add(newbioprofile);
            await _context.SaveChangesAsync();

            var newpaymenthistory = Helper.DeepCopy(record.Passport.LocPaymentHistory);
            newpaymenthistory.Formno = newformno;
            newpaymenthistory.Refno = rec.RefId;
            newpaymenthistory.Appid = rec.AppId;
            _context.LocPaymentHistory.Add(newpaymenthistory);
            await _context.SaveChangesAsync();

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateCreateReissueRecord,
                Description = EnumAuditEvents.UpdateCreateReissueRecord.GetEnumDescription() + $" - OldFormNo: {rec.FormNo}, NewFormNo: {newformno}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = record.FormNo
            });

            await AuthContext.SaveChangesAsync();

            await _context.SaveChangesAsync();

            return new Record
            {
                Passport = new LocPassport
                {
                    LocEnrolProfile = newenrolprofile,
                    LocDocHolderMainProfile = newmainprofile,
                    LocDocHolderCustomProfile = newcustomprofile,
                    LocDocHolderBioProfile = newbioprofile,
                    LocPaymentHistory = newpaymenthistory
                },
                FaceCurrent = newbioprofile.Faceimage,
                SignatureCurrent = newbioprofile.Signimage
            };
        }

        public async Task<Record> UpdateBindReissueRecord(Record rec, string userId)
        {
            string name = rec.BranchCode.Split('|')[1];
            _context = GetConnectionString(rec.BranchCode);
            List<Branch> branches;

            if (!bool.Parse(ConfigurationManager.AppSettings["showFullBranchList"]))
                branches = Helper.GetTempBranchList();
            else
            {
                // get branch info
                branches = await GetBranches();
            }

            var branch = branches.SingleOrDefault(x => x.BranchName == name);

            if (branch == null)
                return null;

            // get record
            var record = await SearchBindReissueRecord(rec.DocNo, rec.FormNo, rec.BranchCode, false, userId);

            // Create new tables
            var formno = await _context.FormCache.OrderBy(x => x.FormNo).FirstOrDefaultAsync();
            _context.FormCache.Remove(formno);
            await _context.SaveChangesAsync();

            var newformno = branch.BranchCode + formno.FormNo.ToString("000000000");

            var newenrolprofile = Helper.DeepCopy(record.Passport.LocEnrolProfile);
            newenrolprofile.Formno = newformno;
            newenrolprofile.Docpage = rec.DocPages;
            newenrolprofile.Appreason = rec.AppReason;
            _context.LocEnrolProfile.Add(newenrolprofile);

            var newmainprofile = Helper.DeepCopy(record.Passport.LocDocHolderMainProfile);
            newmainprofile.Formno = newformno;
            _context.LocDocHolderMainProfile.Add(newmainprofile);

            var newcustomprofile = Helper.DeepCopy(record.Passport.LocDocHolderCustomProfile);
            newcustomprofile.Formno = newformno;
            _context.LocDocHolderCustomProfile.Add(newcustomprofile);

            var newpaymenthistory = Helper.DeepCopy(record.Passport.LocPaymentHistory);
            newpaymenthistory.Formno = newformno;
            newpaymenthistory.Refno = rec.RefId;
            newpaymenthistory.Appid = rec.AppId;
            _context.LocPaymentHistory.Add(newpaymenthistory);

            // use bioprofile from formno
            var bioProfile = await _context.LocDocHolderBioProfile.SingleOrDefaultAsync(x => x.Formno == rec.FormNo);
            var newbioprofile = Helper.DeepCopy(bioProfile);
            newbioprofile.Formno = newformno;
            _context.LocDocHolderBioProfile.Add(newbioprofile);
            await _context.SaveChangesAsync();

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateCreateReissueRecord,
                Description = EnumAuditEvents.UpdateCreateReissueRecord.GetEnumDescription() + $" - OldFormNo: {rec.FormNo}, NewFormNo: {newformno}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                FormNo = rec.FormNo
            });

            await AuthContext.SaveChangesAsync();

            await _context.SaveChangesAsync();

            return new Record
            {
                Passport = new LocPassport
                {
                    LocEnrolProfile = newenrolprofile,
                    LocDocHolderMainProfile = newmainprofile,
                    LocDocHolderCustomProfile = newcustomprofile,
                    LocDocHolderBioProfile = newbioprofile,
                    LocPaymentHistory = newpaymenthistory
                },
                FaceCurrent = newbioprofile.Faceimage,
                SignatureCurrent = newbioprofile.Signimage
            };
        }

        public async Task<int> UpdateBookletToPersoQueue(Record record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            try
            {
                _context = GetConnectionString(record.BranchCode);
                var docinventory = await _context.LocDocInventory.SingleOrDefaultAsync(x => x.Docno == record.PassportNo);

                if (docinventory.Stagecode == "IM3000")
                    docinventory.Stagecode = "IM2000";

                await _context.SaveChangesAsync();
            }
            catch (Exception)
            {
                // Old systems will throw error because table does not have DocType column
                _contextOld = GetConnectionStringOld(record.BranchCode);
                var docinventory = await _contextOld.LocDocInventoryOld.SingleOrDefaultAsync(x => x.Docno == record.PassportNo);

                if (docinventory.Stagecode == "IM3000")
                    docinventory.Stagecode = "IM2000";

                await _context.SaveChangesAsync();
            }

            // add audit
            AuthContext.Audits.Add(new Audit
            {
                EventId = (int)EnumAuditEvents.UpdateBookletToPersoQueue,
                Description = EnumAuditEvents.UpdateBookletToPersoQueue.GetEnumDescription() + $" - DocNo: {record.PassportNo}, Branch: {name}",
                TimeStamp = DateTime.Now,
                UserId = userId,
                DocNo = record.DocNo
            });

            await AuthContext.SaveChangesAsync();

            return 1;
        }

        public async Task<List<LocalQuery>> SearchLocalQuery(string docno, string searchbranchcode, string gender, string firstname, string surname, 
            string formno, string userId)
        {
            string name = searchbranchcode.Split('|')[1];
            _contextOld = GetConnectionStringOld(searchbranchcode);

            if (!CheckConnectionOld())
                return null;

            var audit = new List<string>
            {
                "Branch: " + name
            };

            var query = from ep in _contextOld.LocEnrolProfile
                join dm in _contextOld.LocDocHolderMainProfile on ep.Formno equals dm.Formno into b
                from dma in b.DefaultIfEmpty()
                join dc in _contextOld.LocDocHolderCustomProfile on ep.Formno equals dc.Formno into c
                from dca in c.DefaultIfEmpty()
                join dp in _contextOld.LocDocProfile on ep.Formno equals dp.Formno into a
                from dpa in a.DefaultIfEmpty()
                join pp in _contextOld.LocPersoProfile on ep.Formno equals pp.Formno into d
                from ppa in d.DefaultIfEmpty()
                join bp in _contextOld.LocDocHolderBioProfile on ep.Formno equals bp.Formno into e
                from bpa in e.DefaultIfEmpty()
                select new LocalQuery
                {
                    EnrolProfile = ep,
                    DocProfile = dpa,
                    DocHolderMainProfile = dma,
                    DocHolderCustomProfile = dca,
                    PersoProfile = ppa,
                    BioProfile = bpa
                };

            if (!string.IsNullOrEmpty(formno))
            {
                audit.Add("FormNo: " + formno);
                query = query.Where(p => p.EnrolProfile.Formno == formno);
            }

            if (!string.IsNullOrEmpty(docno))
            {
                audit.Add("DocNo: " + docno);
                query = query.Where(p => p.DocProfile.Docno.ToLower() == docno.ToLower());
            }

            if (!string.IsNullOrEmpty(firstname))
            {
                audit.Add("Firstname: " + firstname);
                query = query.Where(p => p.DocHolderMainProfile.Firstname.ToLower() == firstname.ToLower());
            }


            if (!string.IsNullOrEmpty(surname))
            {
                audit.Add("Surname: " + surname);
                query = query.Where(p => p.DocHolderMainProfile.Surname.ToLower() == surname.ToLower());
            }

            if (!string.IsNullOrEmpty(gender))
            {
                audit.Add("Gender: " + gender);
                query = query.Where(p => p.DocHolderMainProfile.Sex.ToLower() == gender.ToLower());
            }

            try
            {
                var result = await query.ToListAsync();

                // add audit for enrol change
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.LocalQuery,
                    Description = EnumAuditEvents.LocalQuery.GetEnumDescription() + " where " + string.Join(", ", audit),
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = formno
                });

                await AuthContext.SaveChangesAsync();

                return result;

            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                return null;
            }
        }

        public async Task<int> UpdateEnrolmentRecord(DetailedRecord record, string userId)
        {
            string name = record.BranchCode.Split('|')[1];
            _context = GetConnectionString(record.BranchCode);

            var enroleProfile = await _context.LocEnrolProfile.SingleOrDefaultAsync(x => x.Formno == record.FormNo);
            var docHolderMainpProfile = await _context.LocDocHolderMainProfile.SingleOrDefaultAsync(x => x.Formno == record.FormNo);
            var docHolderCustomProfile = await _context.LocDocHolderCustomProfile.SingleOrDefaultAsync(x => x.Formno == record.FormNo);
            var paymentHistory = await _context.LocPaymentHistory.SingleOrDefaultAsync(x => x.Formno == record.FormNo);

            // update records
            if (record.FirstName?.ToUpper() != docHolderMainpProfile.Firstname?.ToUpper() && !(string.IsNullOrEmpty(docHolderMainpProfile.Firstname) && string.IsNullOrEmpty(record.FirstName)))
            {
                var prev = docHolderMainpProfile.Firstname;

                docHolderMainpProfile.Firstname = record.FirstName?.ToUpper();
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Firstname from \"{prev}\" to \"{record.FirstName}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            if (record.Surname.ToUpper() != docHolderMainpProfile.Surname?.ToUpper() && !(string.IsNullOrEmpty(docHolderMainpProfile.Surname) && string.IsNullOrEmpty(record.Surname)))
            {
                var prev = docHolderMainpProfile.Surname;                               

                docHolderMainpProfile.Surname = record.Surname?.ToUpper();
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Surname from \"{prev}\" to \"{record.Surname}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            if (record.DateOfBirth != docHolderMainpProfile.Birthdate)
            {
                var prev = docHolderMainpProfile.Birthdate;
                docHolderMainpProfile.Birthdate = record.DateOfBirth;
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Date of birth from \"{prev}\" to \"{record.DateOfBirth}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            if (record.Sex != docHolderMainpProfile.Sex && !(string.IsNullOrEmpty(docHolderMainpProfile.Sex) && string.IsNullOrEmpty(record.Sex)))
            {
                var prev = docHolderMainpProfile.Sex;
                docHolderMainpProfile.Sex = record.Sex;
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Gender from \"{prev}\" to \"{record.Sex}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            if (record.BirthTown.ToUpper() != docHolderMainpProfile.Birthtown?.ToUpper() && !(string.IsNullOrEmpty(docHolderMainpProfile.Birthtown) && string.IsNullOrEmpty(record.BirthTown)))
            {
                var prev = docHolderMainpProfile.Birthtown;

                docHolderMainpProfile.Birthtown = record.BirthTown?.ToUpper();
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Birth Town from \"{prev}\" to \"{record.BirthTown}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            if (record.BirthState.ToUpper() != docHolderMainpProfile.Birthstate?.ToUpper() && !(string.IsNullOrEmpty(docHolderMainpProfile.Birthstate) && string.IsNullOrEmpty(record.BirthState)))
            {
                var prev = docHolderMainpProfile.Birthstate;

                docHolderMainpProfile.Birthstate = record.BirthState;
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Birth state from \"{prev}\" to \"{record.BirthState}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }


            if (record.Title?.ToUpper() != docHolderCustomProfile.Title?.ToUpper() && !(string.IsNullOrEmpty(docHolderCustomProfile.Title) && string.IsNullOrEmpty(record.Title)))
            {
                var prev = docHolderCustomProfile.Title;
                
                docHolderCustomProfile.Title = record.Title.ToUpper();
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Title from \"{prev}\" to \"{record.Title}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            if (record.MiddleName?.ToUpper() != docHolderCustomProfile.Othername1?.ToUpper() && !(string.IsNullOrEmpty(docHolderCustomProfile.Othername1) && string.IsNullOrEmpty(record.MiddleName)))
            {
                var prev = docHolderCustomProfile.Othername1;

                docHolderCustomProfile.Othername1 = record.MiddleName?.ToUpper();
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Middlename from \"{prev}\" to \"{record.MiddleName}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            if (record.OriginState?.ToUpper() != docHolderCustomProfile.Originstate?.ToUpper() && !(string.IsNullOrEmpty(docHolderCustomProfile.Originstate) && string.IsNullOrEmpty(record.OriginState)))
            {
                var prev = docHolderCustomProfile.Originstate;
                docHolderCustomProfile.Originstate = record.OriginState?.ToUpper();
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Origin state from \"{prev}\" to \"{record.OriginState}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }
                      
            if (record.Appreason != enroleProfile.Appreason)
            {
                var prev = enroleProfile.Appreason;
                enroleProfile.Appreason = record.Appreason;
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Appreason from \"{prev}\" to \"{record.Appreason}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            if (record.DocPage != enroleProfile.Docpage && !(string.IsNullOrEmpty(enroleProfile.Docpage) && string.IsNullOrEmpty(record.DocPage)))
            {
                var prev = enroleProfile.Docpage;
                enroleProfile.Docpage = record.DocPage;
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed DocPage from \"{prev}\" to \"{record.DocPage}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            if (record.DocType != enroleProfile.Doctype && !(string.IsNullOrEmpty(enroleProfile.Doctype) && string.IsNullOrEmpty(record.DocType)))
            {
                var prev = enroleProfile.Doctype;
                enroleProfile.Doctype = record.DocType;
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed DocType from \"{prev}\" to \"{record.DocType}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            if (record.PersonalNo?.ToUpper() != docHolderMainpProfile.Personalno?.ToUpper() && !(string.IsNullOrEmpty(docHolderMainpProfile.Personalno) && string.IsNullOrEmpty(record.PersonalNo)))
            {
                var prev = docHolderMainpProfile.Personalno;

                docHolderMainpProfile.Personalno = record.PersonalNo?.ToUpper();
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed PersonalNo from \"{prev}\" to \"{record.PersonalNo?.ToUpper()}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            if (record.Remarks != enroleProfile.Remarks?.Trim() && !(string.IsNullOrEmpty(enroleProfile.Remarks) && string.IsNullOrEmpty(record.Remarks)))
            {
                var prev = enroleProfile.Remarks;
                enroleProfile.Remarks = record.Remarks;
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Remarks from \"{prev}\" to \"{record.Remarks}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            if (record.Nofinger != Convert.ToBoolean(enroleProfile.Nofinger))
            {
                var prev = enroleProfile.Nofinger;
                enroleProfile.Nofinger = record.Nofinger ? 1 : 0;
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed Nofinger from \"{prev}\" to \"{record.Nofinger}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }


            if (record.ApplicationId != paymentHistory.Appid)
            {
                var prev = paymentHistory.Appid;
                paymentHistory.Appid = record.ApplicationId;
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed ApplicationId from \"{prev}\" to \"{record.ApplicationId}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            if (record.ReferenceId != paymentHistory.Refno)
            {
                var prev = paymentHistory.Refno;
                paymentHistory.Refno = record.ReferenceId;
                AuthContext.Audits.Add(new Audit
                {
                    EventId = (int)EnumAuditEvents.UpdateRecordDetails,
                    Description = EnumAuditEvents.UpdateRecordDetails.GetEnumDescription() + $" - Changed ReferenceId from \"{prev}\" to \"{record.ReferenceId}\", Branch: {name}, FormNo: {record.FormNo}",
                    TimeStamp = DateTime.Now,
                    UserId = userId,
                    FormNo = record.FormNo
                });
            }

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    var type = eve.Entry.Entity.GetType().Name;
                    var state = eve.Entry.State;
                    string msg = "";

                    foreach (var ve in eve.ValidationErrors)
                    {
                        msg += ve.ErrorMessage + " | ";
                        Console.WriteLine("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                throw;
            } 

            await AuthContext.SaveChangesAsync();

            return 1;
        }


        // Mini Queries
        public async Task<LocPersoProfile> GetPersoProfile(string docNo)
        {
            return await _context.LocPersoProfile.SingleOrDefaultAsync(x => x.Docno == docNo);
        }


        // Operations
        private NgricaoDbContext GetConnectionString(string branch)
        {
            string ip = branch.Split('|')[0];

            // simple decryption of username/pw from config file. Basically i "salt" it with random characters which I remove in the code based on the "salt" value in config
            var encrypted = ConfigurationManager.AppSettings["branchConnectionString"];
            var decrypted = Helper.DecryptConfig(encrypted);

            string connString = $"Server={ ip }{ _port }; { decrypted }";

            if (bool.Parse(ConfigurationManager.AppSettings["useBranchDownstairs"]))
            {
                connString = ConfigurationManager.ConnectionStrings["TestServerIcao"].ConnectionString;
            }

            if (bool.Parse(ConfigurationManager.AppSettings["useLaptopAsBranch"]))
            {
                connString = ConfigurationManager.ConnectionStrings["LaptopNGRICAO"].ConnectionString;
            }

            var context = new NgricaoDbContextFactory { Conn = connString };
            return context.Create();
        }

        private NgricaoDbContextOld GetConnectionStringOld(string branch)
        {
            string ip = branch.Split('|')[0];

            // simple decryption of username/pw from config file. Basically i "salt" it with random characters which I remove in the code based on the "salt" value in config
            var encrypted = ConfigurationManager.AppSettings["branchConnectionString"];
            var decrypted = Helper.DecryptConfig(encrypted);

            string connString = $"Server={ ip }{ _port }; { decrypted }";

            if (bool.Parse(ConfigurationManager.AppSettings["useBranchDownstairs"]))
            {
                connString = ConfigurationManager.ConnectionStrings["TestServerIcao"].ConnectionString;
            }

            if (bool.Parse(ConfigurationManager.AppSettings["useLaptopAsBranch"]))
            {
                connString = ConfigurationManager.ConnectionStrings["LaptopNGRICAO"].ConnectionString;
            }

            var contextOld = new NgricaoDbContextOldFactory { Conn = connString };
            return contextOld.Create();
        }

        private bool CheckConnection()
        {
            try
            {
                _context.Database.Connection.Open();
                _context.Database.Connection.Close();
            }
            catch (SqlException ex)
            {
                string err = ex.Message;
                return false;
            }
            return true;
        }

        private bool CheckConnectionOld()
        {
            try
            {
                _contextOld.Database.Connection.Open();
                _contextOld.Database.Connection.Close();
            }
            catch (SqlException ex)
            {
                string err = ex.Message;
                return false;
            }
            return true;
        }
    }
}
