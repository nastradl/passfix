﻿using System.Threading.Tasks;
using PassFix.Repositories;

namespace PassFix.Services
{
    public class EmailSender : IEmailSender
    {
        public Task SendEmailAsync(string email, string subject, string message)
        {
            return Task.CompletedTask;
        }
    }
}
