﻿using PassFix.Data;
using PassFix.Enums;
using PassFix.Models.NGRICAO;
using PassFix.Repositories;
using System.Data.Entity;
using System.Threading.Tasks;

namespace PassFix.Services
{
    public class EnroleProfileService : IEnrolProfileService
    {
        private readonly NgricaoDbContext _context;

        public EnroleProfileService(NgricaoDbContext ctx){
             _context = ctx;
        }
        
        public async Task<LocEnrolProfile> GetEnrolProfile(string formNo)
        {
            return await _context.LocEnrolProfile.FirstAsync(x => x.Formno == formNo);
        }

        public async Task<int> UpdateEnrolProfile(LocEnrolProfile enrolProfile)
        {
            _context.LocEnrolProfile.Attach(enrolProfile);
            _context.Entry(enrolProfile).State = EntityState.Modified;
           
            return await _context.SaveChangesAsync();
        }

        public Task<int> ChangeStageCode(EnumStageCodes from, EnumStageCodes to)
        {
            throw new System.NotImplementedException();
        }
    }
}
