﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PassFix.Startup))]
namespace PassFix
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
