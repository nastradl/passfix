﻿using PassFix.Models.Central;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace PassFix.ViewModels
{
    public class DetailedRecord
    {
        public DetailedRecord()
        {
            TestInfo = new List<TestInfo>();
        }

        public string FormNo { get; set; }

        public string Surname { get; set; }

        public string PageTitle { get; set; }

        public string FirstName { get; set; }

        public string Sex { get; set; }

        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? DateOfBirth { get; set; }

        public string PersonalNo { get; set; }

        public string Error { get; set; }

        public string BirthState { get; set; }

        public string BirthTown { get; set; }

        public string MiddleName { get; set; }

        public string Title { get; set; }

        public string OriginState { get; set; }

        public string DocType { get; set; }

        public string DocPage { get; set; }

        public int Appreason { get; set; }

        public string AppreasonDescr { get; set; }

        public bool FromBranch { get; set; }

        public string Remarks { get; set; }

        public bool Nofinger { get; set; }

        public string ReferenceId { get; set; }

        public string ApplicationId { get; set; }

        public string StageCode { get; set; }

        public string BranchCode { get; set; }

        public string UpdateUrl { get; set; }

        public string SearchUrl { get; set; }

        public bool ShowMultiMessage { get; set; }

        public List<State> States { get; set; }

        public List<Title> Titles { get; set; }

        public List<TestInfo> TestInfo { get; set; }

        public List<Fingerprint> Fingerprints { get; set; }

        public byte[] SignatureCurrent { get; set; }

        public byte[] FaceCurrent { get; set; }

        public string SignatureCurrentString => SignatureCurrent == null ? null : $"data:image/jpeg;base64,{Convert.ToBase64String(SignatureCurrent)}";

        public string FaceCurrentString => FaceCurrent == null ? null : $"data:image/jpeg;base64,{Convert.ToBase64String(FaceCurrent)}";
    }
}
