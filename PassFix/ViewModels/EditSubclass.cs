﻿using System.Collections.Generic;
using PassFix.Models.Central;

namespace PassFix.ViewModels
{
    public class EditSubclass
    {
        public int Id { get; set; }

        public string SubclassName { get; set; }

        public List<UserClaim> SelectedClaims { get; set; }

        public List<UserClaim> UnSelectedClaims { get; set; }
    }
}
