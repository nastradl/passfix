﻿using PassFix.Models.Central;
using System.Collections.Generic;

namespace PassFix.ViewModels
{
    public class EditSubclassWrap
    {
        public string SubclassId { get; set; }

        public List<ClaimsSubclass> Subclasses { get; set; }
    }
}
