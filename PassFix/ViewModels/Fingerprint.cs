﻿using System;
using System.Drawing;
using System.IO;
using Wsq2Bmp;

namespace PassFix.ViewModels
{
    public class Fingerprint
    {
        public string FingerprintName { get; set; }

        public byte[] Finger { get; set; }

        public string FingerprintString => Finger == null ? null : GetBitmapString(Finger);

        public static byte[] ImageToBytes(Bitmap img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        private string GetBitmapString(byte[] img)
        {
            var decoder = new WsqDecoder();
            var bitmap = decoder.Decode(img);
            var bitmaparray = ImageToBytes(bitmap);
            return $"data:image/gif;base64,{Convert.ToBase64String(bitmaparray)}";
        }
    }
}