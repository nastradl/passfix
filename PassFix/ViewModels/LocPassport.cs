﻿using PassFix.Models.NGRICAO;
using System.Collections.Generic;

namespace PassFix.ViewModels
{
    public class LocPassport
    {
        public LocEnrolProfile LocEnrolProfile { get; set; }

        public List<LocDocProfile> LocDocProfiles { get; set; }

        public LocDocHolderBioProfile LocDocHolderBioProfile { get; set; }

        public LocDocHolderCustomProfile LocDocHolderCustomProfile { get; set; }

        public LocDocHolderMainProfile LocDocHolderMainProfile { get; set; }

        public LocPaymentHistory LocPaymentHistory { get; set; }
    }
}
