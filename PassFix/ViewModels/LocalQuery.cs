﻿using System;
using PassFix.Models.NGRICAO;
using System.Collections.Generic;

namespace PassFix.ViewModels
{
    public class LocalQuery
    {
        public LocDocHolderMainProfile DocHolderMainProfile { get; set; }

        public LocDocHolderCustomProfile DocHolderCustomProfile { get; set; }

        public LocEnrolProfileOld EnrolProfile { get; set; }

        public LocDocProfile DocProfile { get; set; }

        public LocDocHolderBioProfileOld BioProfile { get; set; }

        public LocPersoProfile PersoProfile { get; set; }

        public List<TestInfo> TestInfo { get; set; }

        public string SignatureString => BioProfile?.Signimage == null ? null : $"data:image/jpeg;base64,{Convert.ToBase64String(BioProfile.Signimage)}";

        public string FaceString => BioProfile?.Faceimage == null ? null : $"data:image/jpeg;base64,{Convert.ToBase64String(BioProfile.Faceimage)}";
    }
}
