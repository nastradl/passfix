﻿using PassFix.Models.Central;
using System.Collections.Generic;
using System.Security.Claims;

namespace PassFix.ViewModels
{
    public class MenuModel
    {
        public string Username { get; set; }

        public List<Claim> Claims { get; set; }

        public List<Branch> Branches { get; set; }

        public List<ClaimsSubclass> SubClasses { get; set; }
    }
}
