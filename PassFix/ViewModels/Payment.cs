﻿using System.ComponentModel.DataAnnotations;

namespace PassFix.ViewModels
{
    public class Payment
    {
        public int Aid { get; set; }
        public int RefId { get; set; }

        public string SearchUrl { get; set; }

        public string UpdateUrl { get; set; }

        public string UpdateMsg { get; set; }   

        public bool IsAdmin { get; set; }

        public bool AllowEdit { get; set; }

        public bool AllowUpdate{ get; set; }

        public string Title { get; set; }

        [StringLength(30)]
        public string Firstname { get; set; }

        public string OldFirstname { get; set; }

        [StringLength(30)]
        public string Lastname { get; set; }

        public string OldSurname { get; set; }

        public string Dob { get; set; }

        public string OldDob { get; set; }

        [StringLength(50)]
        public string StateOfBirth { get; set; }

        [StringLength(1)]
        public string Sex { get; set; }

        public string OldSex { get; set; }

        [StringLength(50)]
        public string Processingcountry { get; set; }

        public string OldProcessingcountry { get; set; }

        public string ProcessingCountryName { get; set; }

        [StringLength(50)]
        public string Processingstate { get; set; }

        public string OldProcessingstate { get; set; }

        public string ProcessingStateName { get; set; }

        [StringLength(50)]
        public string Processingoffice { get; set; }

        public string OldProcessingoffice { get; set; }

        public string ProcessingOfficeName { get; set; }

        public string Mystring { get; set; }

        public string Paymentdate { get; set; }

        [StringLength(15)]
        public string Passporttype { get; set; }

        public string Expirydate { get; set; }
        
        public string OldExpiryDate { get; set; }

        [StringLength(12)]
        public string Formno { get; set; }

        [StringLength(45)]
        public string Activationno { get; set; }

        [StringLength(20)]
        public string Applicationstatus { get; set; }

        public string Enrolmentdate { get; set; }

        [StringLength(10)]
        public string Postingstate { get; set; }

        public int AmountNaira { get; set; }

        public string AmountDollar { get; set; }

        [StringLength(2)]
        public string Passportsize { get; set; }

        [StringLength(50)]
        public string Interventionoffice { get; set; }

        public int Interventionstatus { get; set; }
    }
}
