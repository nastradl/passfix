﻿namespace PassFix.ViewModels
{
    public class ProcessingCountry
    {
        public int CountryId { get; set; }

        public string CountryName { get; set; }

        public string CountryCode { get; set; }
    }
}