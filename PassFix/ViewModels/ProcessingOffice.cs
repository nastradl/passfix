﻿namespace PassFix.ViewModels
{
    public class ProcessingOffice
    {
        public int OfficeId { get; set; }

        public string OfficeName { get; set; }
    }
}