﻿using PassFix.Models.Central;
using PassFix.Models.NGRICAO;
using System;
using System.Collections.Generic;
using System.Globalization;

namespace PassFix.ViewModels
{
    public class Record
    {
        public Record()
        {
            TestInfo = new List<TestInfo>();
            LocUser = new User();
        }

        public string FormNo { get; set; }

        public string SecondFormNo { get; set; }

        public string DocNo { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string MiddleName { get; set; }

        public int AppReason { get; set; }

        public string DocPages { get; set; }

        public long LocationId { get; set; }

        public long? PersoLocationId { get; set; }

        public string BranchCode { get; set; }

        public string StageCode { get; set; }

        public string AppId { get; set; }

        public string RefId { get; set; }

        public string PassportNo { get; set; }

        public string DocNoTwo { get; set; }

        public string DocTwoStageCode { get; set; }

        public string DocStageCode2 { get; set; }

        public string PassportType { get; set; }

        public string BoxSn { get; set; }

        public string Remark { get; set; }

        public string Error { get; set; }

        public string Title { get; set; }

        public string UpdateUrl { get; set; }

        public string SearchUrl { get; set; }

        public string DocProfileStageCode { get; set; }

        public string PersoProfileStageCode { get; set; }

        public string Password { get; set; }

        public string Xml { get; set; }

        public bool ShowPassportNo { get; set; }

        public bool ShowRemark { get; set; }

        public bool ShowPassportType { get; set; }

        public bool DisableStagecode { get; set; }

        public bool ShowStageCode { get; set; } = true;

        public bool ShowPersoStageCode { get; set; }

        public bool ShowDocStageCode { get; set; }

        public bool ShowSearchByDocNo { get; set; } 

        public bool ShowLocation { get; set; }

        public bool ShowPersoLocation { get; set; }

        public bool ShowSecondFormNo { get; set; } = true;

        public bool ShowBoxSn { get; set; }

        public int Revert { get; set; }

        public bool ShowRevert { get; set; }

        public string Gender { get; set; }

        public string Username { get; set; }

        public byte[] SignatureCurrent { get; set; }

        public byte[] SignatureReplace { get; set; }

        public byte[] FaceCurrent { get; set; }

        public byte[] FaceReplace { get; set; }

        public string SignatureCurrentString => SignatureCurrent == null ? null : $"data:image/jpeg;base64,{Convert.ToBase64String(SignatureCurrent)}";

        public string FaceCurrentString => FaceCurrent == null ? null : $"data:image/jpeg;base64,{Convert.ToBase64String(FaceCurrent)}";

        public string SignatureReplaceString => SignatureReplace == null ? null : $"data:image/jpeg;base64,{Convert.ToBase64String(SignatureReplace)}";

        public string FaceReplaceString => FaceReplace == null ? null : $"data:image/jpeg;base64,{Convert.ToBase64String(FaceReplace)}";

        public bool ShowUpdateButton { get; set; } = true;

        public bool AllowEditLocation { get; set; } = false;

        public string UpdateMsg { get; set; } = "Update";

        public User LocUser { get; set; }

        public List<TestInfo> TestInfo { get; set; }

        public List<AppReason> AppReasons { get; set; }

        public LocPassport Passport { get; set; }

        public List<LocDocProfile> DocProfiles { get; set; }

        public int ExtendBy { get; set; }

        public int Disabled { get; set; }

        public string GetUserPasswordExpiry()
        {
            if (LocUser == null || LocUser.PasswordExpires == 0)
                return string.Empty;

            var date = DateTime.ParseExact(LocUser.PasswordExpires.ToString(), "yyyyMMddHHmmssfff", CultureInfo.CurrentCulture);
            return date.ToString("dd/MM/yyyy"); // + " at " + date.ToString("hh:mmtt").ToLower();
        }

        public long GetNewUserPasswordExpiry(long expires, int extendby)
        {
            var currdate = DateTime.ParseExact(expires.ToString(), "yyyyMMddHHmmssfff", CultureInfo.CurrentCulture);
            var newdate = currdate.AddMonths(extendby);
            return long.Parse(newdate.ToString("yyyyMMddHHmmssfff"));
        }
    }
}
