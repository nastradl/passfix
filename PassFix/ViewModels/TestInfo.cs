﻿namespace PassFix.ViewModels
{
    public class TestInfo
    {
        public string Name { get; set; }

        public string Value { get; set; }

        public bool Pass { get; set; }
    }
}
