﻿using PassFix.Models;
using PassFix.Models.Central;
using System.Collections.Generic;
using X.PagedList;

namespace PassFix.ViewModels
{
    public class ViewAudits
    {
        public List<Audit> Audits { get; set; }

        public int CurrentPage { get; set; }

        public int NumberOfPages { get; set; }

        public int NumberPerPage { get; set; }

        public int Reverse { get; set; }

        public string OrderBy { get; set; }
        
        public int Total { get; set; }

        public StaticPagedList<Audit> Page { get; set; }

        public List<ApplicationUser> UserList { get; set; }

        public List<string> ActionsList { get; set; }

        public ApplicationUser SearchUser { get; set; }

        public string SearchAction { get; set; }

        public string SearchStartDate { get; set; }

        public string SearchEndDate { get; set; }

        public string FormNo { get; set; }

        public string DocNo { get; set; }
    }
}
