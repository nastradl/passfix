﻿using PassFix.Models.Central;
using System.Collections.Generic;
using X.PagedList;

namespace PassFix.ViewModels
{
    public class ViewErrors
    {
        public List<ErrorLog> ErrorLogs { get; set; }

        public int CurrentPage { get; set; }

        public int NumberOfPages { get; set; }

        public int NumberPerPage { get; set; }

        public int Reverse { get; set; }

        public string OrderBy { get; set; }
        
        public int Total { get; set; }

        public StaticPagedList<ErrorLog> Page { get; set; }
    }
}
